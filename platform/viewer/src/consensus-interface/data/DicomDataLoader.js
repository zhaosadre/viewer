
import DICOMWeb from '../../../../core/src/DICOMWeb/'; //TODO: use dcmjs instead

import {api} from "dicomweb-client";
import dcmjs from 'dcmjs';


export default class DicomDataLoader {
  static instance = null;
  pacsURL = null;
  client = null;
  enaled = true;
  runningNb = 0;
  
  enable() {
    this.enaled = true;
  }
  
  async disable() {
    this.enaled = false;
    
    await this.waitUntilIsDisabled();
  }
  
  async waitUntilIsDisabled() {
    const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
    
    while(this.runningNb) {
      await sleep(50);
    }
    if(!this.enaled) // if was intentionnaly disable, wait 10 ms more to be sure return statement occured.
      sleep(10);
  }
  
  setPACSURL(pacsURL) {
    if(pacsURL==this.pacsURL)
      return;
      
    this.pacsURL = pacsURL;
    this.client = new api.DICOMwebClient({url: pacsURL});
  }
  
  static getInstance(){
    if (!DicomDataLoader.instance)
      DicomDataLoader.instance = new DicomDataLoader();
      
    return this.instance;
  }
  
  async getInstanceMetaData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID) {
    if (!this.enaled)
      return null;
      
    this.runningNb++;

    const options = {
      studyInstanceUID:StudyInstanceUID, 
      seriesInstanceUID:SeriesInstanceUID,
      sopInstanceUID:SOPInstanceUID
    };
  
    const data = await this.client.retrieveInstance(options);
  
    const dicomData = dcmjs.data.DicomMessage.readFile(data);
    
    this.runningNb--;
    return (dcmjs.data.DicomMetaDictionary.naturalizeDataset(dicomData.dict));
  }

  async getInstanceData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID, seriesMetaData) {
    if (!this.enaled)
      return null;
      
    this.runningNb++;
      
    const options = {
      studyInstanceUID:StudyInstanceUID, 
      seriesInstanceUID:SeriesInstanceUID,
      sopInstanceUID:SOPInstanceUID
    };
    
    if (!seriesMetaData)
      seriesMetaData = await this.getSeriesMetaData(url, StudyInstanceUID, SeriesInstanceUID); // We could retrieve them directly from client.retrieveInstance
    
    const data = await this.client.retrieveInstance(options);
    const data16 = new Int16Array(data);
    
    let newData = data16.slice(data16.length-seriesMetaData[0].Rows*seriesMetaData[0].Columns, data16.length);
    
    if (seriesMetaData[0].hasOwnProperty("RescaleIntercept") && seriesMetaData[0].RescaleSlope)
      newData = newData.map(elem => elem*seriesMetaData[0].RescaleSlope + seriesMetaData[0].RescaleIntercept);
    
    this.runningNb--;
    
    return newData;
  }

  async getInstancesDataFromSeriesMetaData(seriesMetaData, progressFunc) {
    this.runningNb++;
    
    let data = new Int16Array(seriesMetaData[0].Rows*seriesMetaData[0].Columns*seriesMetaData.length);
    
    // metadata already sorted by getSeriesMetaData
    let ind = 0;
    for (let i=0; i<seriesMetaData.length; i++) {
      if (!this.enaled) {
        this.runningNb--;
        
        return null;
      }
      
      console.log('Loading instance ' + (i+1) + '/' + seriesMetaData.length);
      
      let instanceMetada = seriesMetaData[i];
      let instanceData = await this.getInstanceData(instanceMetada.StudyInstanceUID, instanceMetada.SeriesInstanceUID, instanceMetada.SOPInstanceUID, seriesMetaData);
      data.set(instanceData, ind);
      ind = ind+instanceData.length;
      
      progressFunc((i+1)/seriesMetaData.length);
    }
    
    this.runningNb--;
    return data;
  }
  
  async getSEGData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID) {
    if (!this.enaled)
      return null;
    
    this.runningNb++;
    
    const res = await this.getInstanceMetaData(StudyInstanceUID, SeriesInstanceUID, SOPInstanceUID);
    
    this.runningNb--;
    return res;
  }

  async getSeriesData(StudyInstanceUID, SeriesInstanceUID, progressFunc) {
    if (!this.enaled)
      return {data: null, metaData:null};
    
    this.runningNb++;
    
    const seriesMetaData = await this.getSeriesMetaData(StudyInstanceUID, SeriesInstanceUID);
    const data = await this.getInstancesDataFromSeriesMetaData(seriesMetaData, progressFunc);
    
    this.runningNb--;
    return {data, metaData:seriesMetaData};
  }

  async getSeriesMetaData(StudyInstanceUID, SeriesInstanceUID) {
    if (!this.enaled)
      return null;
    
    this.runningNb++;
    
    const options = {
      studyInstanceUID:StudyInstanceUID, 
      seriesInstanceUID:SeriesInstanceUID
    };
    
    const seriesMetaData = await this.client.retrieveSeriesMetadata(options);
    let res = resultDataToStudies(seriesMetaData, 'getSeriesMetaData').slice();
    //res.sort((metadata1, metadata2) => metadata1.InstanceNumber > metadata2.InstanceNumber ? 1 : -1); // We have to return a real number and not a boolean in chrome!
    res.sort((metadata1, metadata2) => metadata1.SliceLocation < metadata2.SliceLocation ? 1 : -1); // We have to return a real number and not a boolean in chrome!
    
    this.runningNb--;
    return res;
  }
  
  async getStudies() {
    if (!this.enaled)
      return null;
      
    this.runningNb++;
    
    const resultData = await this.client.searchForStudies();
    const res = resultDataToStudies(resultData, 'getStudies');
    
    this.runningNb--;
    return res;
  }
  
  async getStudyMetaData(StudyInstanceUID) {
    if (!this.enaled)
      return null;
      
    this.runningNb++;
    
    const options = {studyInstanceUID:StudyInstanceUID};
    
    const metaData = await this.client.retrieveStudyMetadata(options);
    
    const res = resultDataToStudies(metaData, 'getStudyMetaData');
    
    this.runningNb--;
    return res;
  }
  
  sendSEGData(metaData) {
    console.log('Not implemented yet - Cannot send:');
    console.log(metaData);
  }
}

function resultDataToStudies(resultData, source) {
  const studies = [];

  if (!resultData || !resultData.length)
    return;

  resultData.forEach(study => {  
    let PixelSpacing = null;
    let ImagePositionPatient = null;
    let SliceThickness = null;
    let ImageOrientationPatient = null;
    let RescaleIntercept = null;
    let RescaleSlope = null;
    let InstanceCreationDate = null;
    let InstanceNumber = null;
    let SliceLocation = null;
    
    if (study['00280030'])
      PixelSpacing = study['00280030'].Value.slice();
    if (study['00200032'])
      ImagePositionPatient = study['00200032'].Value.slice();
    if (study['00200037'])
      ImageOrientationPatient = study['00200037'].Value.slice();
    if (study['00180050'])
      SliceThickness = study['00180050'].Value[0];
    if (study['00281052'])
      RescaleIntercept = study['00281052'].Value[0];
    if (study['00281053'])
      RescaleSlope = study['00281053'].Value[0];
    if (study['00080012'])
      InstanceCreationDate = DICOMWeb.getString(study['00080012']);
    if (study['00200013'])
      InstanceNumber = study['00200013'].Value[0];
    if (study['00201041'])
      SliceLocation = study['00201041'].Value[0];
        
    studies.push({
      StudyInstanceUID: DICOMWeb.getString(study['0020000D']),
      // 00080005 = SpecificCharacterSet
      StudyDate: DICOMWeb.getString(study['00080020']),
      StudyTime: DICOMWeb.getString(study['00080030']),
      AccessionNumber: DICOMWeb.getString(study['00080050']),
      referringPhysicianName: DICOMWeb.getString(study['00080090']),
      // 00081190 = URL
      PatientName: DICOMWeb.getName(study['00100010']),
      PatientID: DICOMWeb.getString(study['00100020'])? DICOMWeb.getString(study['00100020']) : "Null",
      PatientBirthdate: DICOMWeb.getString(study['00100030']),
      patientSex: DICOMWeb.getString(study['00100040']),
      studyId: DICOMWeb.getString(study['00200010']),
      numberOfStudyRelatedSeries: DICOMWeb.getString(study['00201206']),
      numberOfStudyRelatedInstances: DICOMWeb.getString(study['00201208']),
      StudyDescription: DICOMWeb.getString(study['00081030']),
      SeriesDescription: DICOMWeb.getString(study['0008103E']),
      // Modality: DICOMWeb.getString(study['00080060']),
      // ModalitiesInStudy: DICOMWeb.getString(study['00080061']),
      modalities: DICOMWeb.getString(
        DICOMWeb.getModalities(study['00080060'], study['00080061'])
      ),
      SeriesInstanceUID: DICOMWeb.getString(study['0020000E']),
      PatientPosition: DICOMWeb.getString(study['00185100']),
      Manufacturer: DICOMWeb.getString(study['00080070']),
      SOPInstanceUID: DICOMWeb.getString(study['00080018']),
      Rows: study['00280010']? study['00280010'].Value[0] : null,
      Columns: study['00280011']? study['00280011'].Value[0] : null,
      
      BitsAllocated: DICOMWeb.getString(study['00280100']),

      InstanceNumber,
      PixelSpacing,
      ImagePositionPatient,
      ImageOrientationPatient,
      SliceThickness,
      RescaleIntercept,
      RescaleSlope,
      InstanceCreationDate,
      SliceLocation,
    });}
  );
  
  return(studies);
}

