
import vtkDataArray from 'vtk.js/Sources/Common/Core/DataArray';
import vtkImageData from 'vtk.js/Sources/Common/DataModel/ImageData';
import vtkMath from 'vtk.js/Sources/Common/Core/Math';

import DicomDataLoader from './DicomDataLoader.js'

import ImageProperties from './ImageProperties.js'


export default class ImageDataSet {
  constructor() {    
    this.images = [];
    this.listeners = [];
    
    this.dicomDataLoader = DicomDataLoader.getInstance(); //default
  }
  
  getDataLoader() {
    return this.dicomDataLoader;
  }
  
  setDataLoader(dataLoader) {
    this.dicomDataLoader = dataLoader;
  }
  
  loadAll() {
  
  }
  
  setData(metaData, data) {    
    const image = {
      StudyInstanceUID: metaData[0].StudyInstanceUID,
      SeriesInstanceUID: metaData[0].SeriesInstanceUID,
      image: new Image(new Int16Array(data), metaData)
    };
    
    this.images.push(image);
    
    const event = {
      target: metaData[0].SeriesInstanceUID,
      type: "created",
      value: null,
    };
    this.modified(event);
  }
  
  async loadDataAsync(StudyInstanceUID, SeriesInstanceUID, progressFunc) {
    if (this.getImage(SeriesInstanceUID))
      return; //Image already loaded
    
    const {data, metaData} = await this.dicomDataLoader.getSeriesData(StudyInstanceUID, SeriesInstanceUID, progressFunc);
    
    if (!data)
      return; // could happen if we abort data loading
      
    const image = {
      StudyInstanceUID,
      SeriesInstanceUID,
      image: new Image(data, metaData)
    };
    
    this.images.push(image);
    
    const event = {
      target: SeriesInstanceUID,
      type: "created",
      value: null,
    };
    this.modified(event);
  }
  
  async delete() {
    await this.dicomDataLoader.disable();
    this.images = [];
    this.listeners = [];
  }
  
  loadData(StudyInstanceUID, SeriesInstanceUID, progressFunc) {
    this.loadDataAsync(StudyInstanceUID, SeriesInstanceUID, progressFunc).then();
  }
  
  addListener(listener) {
    this.listeners.push(listener);
  }
  
  removeListener(listener) {
    const listenerIndex = this.listeners.indexOf(listener);
    
    if (listenerIndex>-1)
      this.listeners.splice(listenerIndex, 1)
  }
  
  modified(event) {
    this.listeners.slice().forEach(listener => listener(event)); // Very important to slice because this.listeners could be changed by one of the listener running in forEach
  }
  
  getImage(SeriesInstanceUID) {
    const filteredImage = this.images.filter(image => image.SeriesInstanceUID == SeriesInstanceUID);
    
    if (!filteredImage.length)
      return null;
      
    return filteredImage[0].image;
  }
  
  getImages() {
    return this.images.map(image => image.image);
  }
}


/*********
 * Image *
 *********/
class Image {
  constructor(data, metaData) {
    this.data = data;
    this.metaData = metaData;
    
    this.imageProperties = new ImageProperties();
    
    this.VTKData = null;
    
    this.trueDirectionDiffers = false;
    
    this._setVTKData();
  }
  
  _setVTKData() {
    const scalarArray = vtkDataArray.newInstance({
          name: "Scalars",
          numberOfComponents: 1,
          values: this.data, // We do not slice on purpose => Change to VTKData == change to this.data
      });
      
    const imageData = vtkImageData.newInstance();
    
    const metaData = this.metaData;
    
    let direction = metaData[0].ImageOrientationPatient;
    
    let columnStepToPatient = direction.slice(0,3);
    let rowStepToPatient = direction.slice(3,6);
    let direction3 = [0, 0, 0];
    vtkMath.cross(rowStepToPatient, columnStepToPatient, direction3);
    
    vtkMath.normalize(columnStepToPatient);
    vtkMath.normalize(rowStepToPatient);
    vtkMath.normalize(direction3);
    this.trueDirection = columnStepToPatient.concat(rowStepToPatient).concat(direction3);
    
    
    direction =  this.trueDirection;
    
    let dir0 = direction.slice(0, 3);
    let dir1 = direction.slice(3, 6);
    let dir2 = direction.slice(6, 9);
    
    let dirs = [dir0, dir1, dir2];
    
    let indM0 = dir0.map(e => Math.abs(e)).indexOf(Math.max.apply(null, dir0.map(e => Math.abs(e))));
    let indM1 = dir1.map(e => Math.abs(e)).indexOf(Math.max.apply(null, dir1.map(e => Math.abs(e))));
    let indM2 = dir2.map(e => Math.abs(e)).indexOf(Math.max.apply(null, dir2.map(e => Math.abs(e))));    
    
    const oldSliceMode = this.sliceMode;
    if(indM0 != 0 && indM1!=1 && indM2!=2) { 
      direction = [1, 0, 0, 0, 1, 0, 0, 0, -1];
      this.trueDirectionDiffers = true;
    }
    
    
    const patientSize = [
      metaData[0].Columns*metaData[0].PixelSpacing[1],
      metaData[0].Rows*metaData[0].PixelSpacing[0],
      metaData.length*metaData[0].SliceThickness,
      ];
    
    imageData.setDimensions(metaData[0].Columns, metaData[0].Rows, metaData.length);
    imageData.setSpacing(metaData[0].PixelSpacing[0], metaData[0].PixelSpacing[1], metaData[0].SliceThickness);
    imageData.setDirection(direction);
    //imageData.setOrigin(0, 0, 0);
    imageData.setOrigin(metaData[0].ImagePositionPatient[0], metaData[0].ImagePositionPatient[1], metaData[0].ImagePositionPatient[2]);
    imageData.getPointData().setScalars(scalarArray);
    
    this.VTKData = imageData;
  }
  
  getData() {
    return this.data;
  }
  
  //sliceNb starts at 0
  getSlice(sliceNb) {
    const zStride = this.metaData[0].Rows*this.metaData[0].Columns;
  
    return this.data.slice(sliceNb*zStride, (sliceNb+1)*zStride);
  }
  
  getImageProperties() {
    return this.imageProperties;
  }
  
  getMetaData() {
    return this.metaData;
  }
  
  getVTKData() {
    return this.VTKData;
  }
  
  getTrueDirectionDiffers() {
    return this.trueDirectionDiffers;
  }
  
  getTrueDirection() {
    return this.trueDirection.slice();
  }
}

