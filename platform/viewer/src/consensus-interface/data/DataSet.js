
import uniqBy from 'lodash/uniqBy';

import ImageDataSet from './ImageDataSet.js'
import SegDataSet from './SegDataSet.js'
import DicomDataLoader from './DicomDataLoader.js'
import {SETTING_NAMES, MainSettings} from '../utils/MainSettings.js';

export default class DataSet {
  static instance = null;
  imageDataSet = null;
  segDataSet = null;
  dicomDataLoader = DicomDataLoader.getInstance(); //Default
  progressArray = [];
  listeners = [];
  study = null;
  
  static getInstance() {
    if (!DataSet.instance)
      DataSet.instance = new DataSet();
      
    return this.instance;
  }  
  
  setDataSet(dataSet) {
    this.imageDataSet = dataSet.getImageDataSet();
    this.segDataSet = dataSet.getSegDataSet();
    this.study = dataSet.getStudy();
    this.dicomDataLoader = dataSet.getDataLoader();
   
    const forwardListener = (event) => {this.modified(event);};
    dataSet.addListener(forwardListener);    
  }
  
  getDataLoader() {
    return this.dicomDataLoader;
  }
  
  setDataLoader(dataLoader) {
    this.dicomDataLoader = dataLoader;
    
    if (this.imageDataSet)
      this.imageDataSet.setDataLoader(dataLoader);
      
    if (this.segDataSet)
      this.segDataSet.setDataLoader(dataLoader);
  }
  
  //StudyInstanceUID is mandatory. If mixed StudyInstanceUID are allowed. Put whatever you want
  async setData(series, segs, StudyInstanceUID) {
    await this.delete();
    
    this.imageDataSet = new ImageDataSet();
    this.segDataSet = new SegDataSet();
    
    this.imageDataSet.setDataLoader(this.dicomDataLoader);
    this.segDataSet.setDataLoader(this.dicomDataLoader);
    
    this.study = [];
    series.forEach(seriesEl => seriesEl.metaData.forEach(instance => this.study.push(instance)))
    
    event = {
      target: StudyInstanceUID,
      type: "created",
      value: null,
    };
    this.modified(event);
    
    const imageListener = (event) => {
      this.modified(event);
    }
    this.imageDataSet.addListener(imageListener);
    
    series.forEach(seriesEl => this.imageDataSet.setData(seriesEl.metaData, seriesEl.data))
    
    //TODO: segs
    //this.study.append(segs.map(seriesEl => seriesEl.metaData));
  }
  
  async loadStudyAsync(StudyInstanceUID) {
    await this.delete();
    
    this.imageDataSet = new ImageDataSet();
    this.segDataSet = new SegDataSet();
    
    this.imageDataSet.setDataLoader(this.dicomDataLoader);
    this.segDataSet.setDataLoader(this.dicomDataLoader);
    
    // Images
    this.dicomDataLoader.enable();
    
    this.study = await this.dicomDataLoader.getStudyMetaData(StudyInstanceUID);
    
    event = {
      target: StudyInstanceUID,
      type: "created",
      value: null,
    };
    this.modified(event);
    
    const SeriesInstanceUIDs = uniqBy(this.study, 'SeriesInstanceUID').filter(study => 
      (study.modalities=="CT" || study.modalities=="MR" || study.modalities=="PT")).map(study => study.SeriesInstanceUID);
    
    const progressFunc = (SeriesInstanceUID, progess) => {
      const index = this.progressArray.map(elem => elem.SeriesInstanceUID).indexOf(SeriesInstanceUID);
      
      if(index>-1)
        this.progressArray[index].progress = progess;
      else
        this.progressArray.push({SeriesInstanceUID, progess});
      
      event = {
        target: SeriesInstanceUID,
        type: "progress",
        value: progess,
      };
      this.modified(event);
    };
    
    const imageListener = (event) => {
      this.modified(event);
    }
    this.imageDataSet.addListener(imageListener);
    
    let loadingFuncs = SeriesInstanceUIDs.map(SeriesInstanceUID => {
      return new Promise(resolve => this.imageDataSet.loadDataAsync(StudyInstanceUID, SeriesInstanceUID, (progress) => {progressFunc(SeriesInstanceUID, progress)}));
    });
    
    // Segs
    const segListener = (event) => {
      this.modified(event);
    }
    this.segDataSet.addListener(segListener);
    
    let loadingFuncs2 = [];
    
    const SEGSeriesUIDs = uniqBy(this.study, 'SeriesInstanceUID').filter(study => (study.modalities=="SEG" || study.modalities=="RTSTRUCT")).map(study => study.SeriesInstanceUID);
    const SEGSOPUIDs = uniqBy(this.study, 'SeriesInstanceUID').filter(study => (study.modalities=="SEG" || study.modalities=="RTSTRUCT")).map(study => study.SOPInstanceUID);
      
    if (SEGSeriesUIDs){
      loadingFuncs2.push(SEGSeriesUIDs.map((SegSeriesUID, index) => {
        return new Promise(resolve => this.segDataSet.loadDataAsync(StudyInstanceUID, SegSeriesUID, SEGSOPUIDs[index]));
      }));
    }
      
    // load images and segs
    await new Promise(resolve => Promise.all(loadingFuncs.concat(loadingFuncs2)));
  }
  
  getStudy(){
    return this.study;
  }
  
  getImageDataSet() {
    return this.imageDataSet;
  }
  
  getSegDataSet() {
    return this.segDataSet;
  }
  
  modified(event) {
    this.listeners.forEach(listener => listener(event));
  }
  
  addListener(listener) {
    this.listeners.push(listener);
  }
  
  removeListener(listener) {
    const index = this.listeners.indexOf(listener);
    
    if (index>-1)
      this.listeners.splice(index, 1);
  }
  
  prioritizeSeriesUID(seriesUID) {
    //TODO
  }
  
  async delete() {
    if (this.imageDataSet)
      await this.imageDataSet.delete(); // Will also remove listeners
    if (this.segDataSet)
      await this.segDataSet.delete(); // Will also remove listeners
    
    this.study = null;
    this.imageDataSet = null;
    this.segDataSet = null;
    this.progressArray = [];
    this.listeners = [];
  }
}

