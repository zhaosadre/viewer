
import dcmjs from 'dcmjs';

import vtkImageData from 'vtk.js/Sources/Common/DataModel/ImageData';
import vtkDataArray from 'vtk.js/Sources/Common/Core/DataArray';
import vtkMath from 'vtk.js/Sources/Common/Core/Math';

import DicomDataLoader from './DicomDataLoader.js'
import polygonFill from '../utils/polygonFill.js';

import * as d3 from 'd3-contour';


export default class SegDataSet {
  constructor() {
    this.dicomDataLoader = DicomDataLoader.getInstance();
    this.listeners = [];
    this.segsS = [];
  }
  
  async delete() {
    await this.segsS.forEach(item => item.segs.delete());
    this.listeners = [];
    this.segsS = [];
  }
  
  getDataLoader() {
    return this.dicomDataLoader;
  }
  
  setDataLoader(dataLoader) {
    this.dicomDataLoader = dataLoader;
    
    if (this.segsS && this.segsS.length)
      this.segsS.forEach(segs => segs.setDataLoader(dataLoader));
  }
  
  async loadDataAsync(StudyInstanceUID, SegSeriesUID, SegSOPUID){
    const segs = new Segs();
    segs.setDataLoader(this.dicomDataLoader)
    await segs.loadData(StudyInstanceUID, SegSeriesUID, SegSOPUID);
    
    this.segsS.push({
      StudyInstanceUID,
      SegSeriesUID,
      SegSOPUID,
      segs,
    });
    
    const event = {
      target: SegSOPUID,
      type: "created",
      value: null,
    };
    this.modified(event);
  }
  
  addListener(listener) {
    this.listeners.push(listener);
  }
  
  modified(event) {
    this.listeners.slice().forEach(listener => listener(event)); // Very important to slice because this.listeners could be changed by one of the listener running in forEach
  }
  
  removeListener(listener) {
    const listenerIndex = this.listeners.indexOf(listener);
    
    if (listenerIndex>-1)
      this.listeners.splice(listenerIndex, 1)
  }
  
  getAllSegs() {
    return this.segsS.map(segs => segs.segs);
  }
  
  getSegs(SegSOPUID) {
    const filteredImage = this.segsS.filter(item => item.SegSOPUID == SegSOPUID);
    
    if (!filteredImage.length)
      return null;
    
    return filteredImage[0].segs;
  }
}

class Segs {
  constructor() {
    this.SegSeriesUID = null;
    this.SegSOPUID = null;
    this.StudyInstanceUID = null;
    this.Segs = [];

    this.dicomDataLoader = DicomDataLoader.getInstance();
        
    this.allSegData = null;
    this.listeners = [];
    this.listenersLastID = -1; //TODO change how listenerIDs are generated
    this.scalarArray = null;
    
    this.metaData = null;
  }
  
  async delete() {
    await this.dicomDataLoader.disable();
    this.Segs.forEach(seg => seg.delete());
    this.listeners = [];
  }
  
  getDataLoader() {
    return this.dicomDataLoader;
  }
  
  setDataLoader(dataLoader) {
    this.dicomDataLoader = dataLoader;
  }
  
  async loadData(StudyInstanceUID, SegSeriesUID, SegSOPUID) {
    // Populate the dataset
    if (SegSOPUID) {
      this.StudyInstanceUID = StudyInstanceUID;
      this.SegSeriesUID = SegSeriesUID;
      this.SegSOPUID = SegSOPUID;
      
      const metaData = await this.dicomDataLoader.getSEGData(this.StudyInstanceUID, this.SegSeriesUID, this.SegSOPUID);
      this.metaData = metaData;
      
      if(!metaData) // could happen if we abort data loading
        return;
      
      if(metaData) {
        let data = [];
              
        if (metaData.Modality=="SEG") {
          const segmentsNb = metaData.SegmentSequence.length;
                
          const int8data = new Uint8Array(dcmjs.data.BitArray.unpack(metaData.PixelData));
          const dataLength = metaData.PixelData.byteLength*8/(segmentsNb);
                
          for (let i=0; i<segmentsNb; i++) {
            const segmentData = int8data.slice(i*dataLength, (i+1)*dataLength);
            data.push(segmentData);
          }
                
          data.forEach( (d, index) => {this.Segs.push(new Seg(data[index], metaData,  metaData.SegmentSequence[index]))});
        }
              
        if (metaData.Modality=="RTSTRUCT") {
          const segmentsNb = metaData.StructureSetROISequence.length
      
          let Rows = null;
          let Columns = null;
          let Slices = null;
          let ImagePositionPatient = null;
          let PixelSpacing = null;
          let SliceThickness = null;
          let ImageOrientationPatient = null;
          
          // metaData.ImagePositionPatient is non standard but so much useful if we can have it...
          if (! metaData.ImagePositionPatient) {
            const RTReferencedSeriesSequence = metaData.ReferencedFrameOfReferenceSequence.RTReferencedStudySequence.RTReferencedSeriesSequence;
            const CTSeriesInstanceUID = RTReferencedSeriesSequence.SeriesInstanceUID;
          
            const CTMetaData = await this.dicomDataLoader.getSeriesMetaData(this.StudyInstanceUID, CTSeriesInstanceUID);
            
            if(!CTMetaData) // could happen if we abort data loading
              return;
            
            Rows = CTMetaData[0].Rows;
            Columns = CTMetaData[0].Columns;
            Slices = CTMetaData.length;
            ImagePositionPatient = CTMetaData[0].ImagePositionPatient;
            PixelSpacing = CTMetaData[0].PixelSpacing;
            SliceThickness = CTMetaData[0].SliceThickness;
            ImageOrientationPatient = CTMetaData[0].ImageOrientationPatient;
            
            metaData.Rows = Rows,
            metaData.Columns = Columns,
            metaData.Slices = Slices,
            metaData.ImagePositionPatient = ImagePositionPatient;
            metaData.PixelSpacing = PixelSpacing;
            metaData.SliceThickness = SliceThickness;
            metaData.ImageOrientationPatient = ImageOrientationPatient;
          }
          else {
            Rows = metaData.Rows;
            Columns = metaData.Columns;
            Slices = metaData.Slices;
            ImagePositionPatient = metaData.ImagePositionPatient;
            PixelSpacing = metaData.PixelSpacing;
            SliceThickness = metaData.SliceThickness
          }
            
          const segSize = Rows*Columns*Slices;
                  
          for (let i=0; i<segmentsNb; i++) {
            console.log('Computing segment ' + i + '/' + segmentsNb);
            
            const int8data = new Uint8Array(segSize);
            
            if (metaData.ROIContourSequence[i].ContourSequence && metaData.ROIContourSequence[i].ContourSequence.length) {
              const ContourSequence = metaData.ROIContourSequence[i].ContourSequence;
              
              
              for (let j=0; j<ContourSequence.length; j++) {
                const NumberOfContourPoints = ContourSequence[j].NumberOfContourPoints;
                const ContourData = ContourSequence[j].ContourData;
                
                
                const posX = ContourData.filter((elem, index) => !(index%3));
                const posY = ContourData.filter((elem, index) => !((index-1)%3));
                const posZ = ContourData[2];
                
                if (posX.length!=ContourSequence[j].NumberOfContourPoints)
                  error('Data length does not match NumberOfContourPoints');
                
                
                // TODO Use image orientation patient
                const indexX = posX.map(x => Math.round((x-ImagePositionPatient[0])/PixelSpacing[0]));
                const indexY = posY.map(y => Math.round((y-ImagePositionPatient[1])/PixelSpacing[1]));
                const indexZ = Math.round((ImagePositionPatient[2]-posZ)/SliceThickness);
                
                let vs = [] ;
                indexX.forEach((x, index) => {vs = vs.concat([x, indexY[index]]); return vs;});
                
                const yStride = Columns;
                const zStride = Columns*Rows;
                
                
                const fillFunction = (x1, x2, y) => {                
                  x1 = Math.round(x1);
                  x2 = Math.round(x2);
                  y = Math.round(y);
                  int8data.fill(1, y*yStride+indexZ*zStride+x1, y*yStride+indexZ*zStride+x2+1);
                }; //{int8data[x1*yStride+indexZ*zStride+y] = 1}; // 
                
                polygonFill(vs, fillFunction);
                
                //indexX.forEach((x, index) => {int8data[Math.round(indexY[index])*yStride+indexZ*zStride+Math.round(x)] = 1;});
              }
            }
            
            data.push(int8data.slice());
          }
                  
          data.forEach( (d, index) => {this.Segs.push(new Seg(data[index], metaData, {...metaData.StructureSetROISequence[index], ...metaData.ROIContourSequence[index]}))});
        }
      }
              
      this.modified();
    }
  }
  
  addListener(listener) {
    this.listeners.push(listener);
    
    this.listenersLastID = this.listenersLastID+1;
    return this.listenersLastID;
  }
  
  modified() {
    this.listeners.slice().forEach(listener => {listener();});
  }
  
  getSegSeriesUID() {
    return this.SegSeriesUID;
  }
  
  getSegSOPUID() {
    return this.SegSOPUID;
  }
  
  getStudyInstanceUID() {
    return this.StudyInstanceUID;
  }
  
  getSegs() {
    return this.Segs;
  }
  
  getSeg(segmentNumber) {
    return this.Segs.filter(s => s.getNumber()==segmentNumber)[0];
  }
  
  getSegLabels() {
    return this.Segs.map(s => s.getLabel());
  }
  
  getVTKData() {
    if (!this.Segs || !this.Segs.length)
      return;
  
    let data = new Uint8Array(this.Segs[0].getData().length);
    
    for (let i=0; i<this.Segs.length; i++) {
      const seg = this.Segs[i];
        
      seg.getData().forEach((val, index) => {
        data[index] = val? i+1 : data[index]; //TODO what if this correspond to several segments?
      });
    }
    
    this.allSegData = data;
    
    const scalarArray = vtkDataArray.newInstance({
          name: "Scalars",
          numberOfComponents: 1,
          values: this.allSegData,
      });
      
    this.scalarArray = scalarArray;
      
    const imageData = vtkImageData.newInstance();
    this.imageData = imageData;
    
    const metaData = this.Segs[0].getMetaData();
    
    let direction = null;
    let dimension = null;
    let spacing = null;
    let ImagePositionPatient = [0, 0, 0];
    
    if (metaData.Modality=='SEG') {
     direction = metaData.SharedFunctionalGroupsSequence.PlaneOrientationSequence.ImageOrientationPatient;
     dimension = [metaData.Columns, metaData.Rows, metaData.ReferencedSeriesSequence.ReferencedInstanceSequence.length]
     spacing = [metaData.SharedFunctionalGroupsSequence.PixelMeasuresSequence.PixelSpacing[0], metaData.SharedFunctionalGroupsSequence.PixelMeasuresSequence.PixelSpacing[1], metaData.SharedFunctionalGroupsSequence.PixelMeasuresSequence.SliceThickness];
    }
    
    if (metaData.Modality=='RTSTRUCT') {      
      direction = metaData.ImageOrientationPatient;
      dimension = [metaData.Columns, metaData.Rows, metaData.Slices];
      spacing = [metaData.PixelSpacing[0], metaData.PixelSpacing[1], metaData.SliceThickness];
      ImagePositionPatient = metaData.ImagePositionPatient;
    }
      
    let columnStepToPatient = direction.slice(0,3);
    let rowStepToPatient = direction.slice(3,6);
    let direction3 = [0, 0, 0];
    vtkMath.cross(rowStepToPatient, columnStepToPatient, direction3);
    
    /*columnStepToPatient = columnStepToPatient.map(elem => elem?-elem:elem);
    rowStepToPatient = rowStepToPatient.map(elem => elem?-elem:elem);
    direction3 = direction3.map(elem => elem?-elem:elem);*/
    vtkMath.normalize(columnStepToPatient);
    vtkMath.normalize(rowStepToPatient);
    vtkMath.normalize(direction3);
  
    direction = columnStepToPatient.concat(rowStepToPatient).concat(direction3);
    
    imageData.setDimensions(dimension[0], dimension[1], dimension[2]);
    imageData.setSpacing(spacing[0], spacing[1], spacing[2]);
    imageData.setDirection(direction);
    imageData.setOrigin(ImagePositionPatient[0], ImagePositionPatient[1], ImagePositionPatient[2]); //TODO for SEG
    imageData.getPointData().setScalars(scalarArray);
    
    return(imageData);
  }
  
  sendData() {
    if (this.metaData.Modality=='RTSTRUCT') {
      for (let segNb=0; segNb<this.Segs.length; segNb++) {        
        if (this.Segs[segNb].wasModified()) {
          this.Segs[segNb].updateContourSequence();
          const newSequence = this.Segs[segNb].getContourSequence();
          if (newSequence.length) {
            this.metaData.ROIContourSequence[segNb].changed = true;
            this.metaData.ROIContourSequence[segNb].ContourSequence = newSequence;
          }
        } else
          this.metaData.ROIContourSequence[segNb].changed = false
      }
      
      this.dicomDataLoader.sendSEGData(this.metaData)
    }
  }
}


/****************
 * Segmentation *
 ****************/
class Seg {
  constructor(data, metaData, SegmentSequenceItemMetadata) {
    let rgba = null;
    
    if (metaData.Modality=='SEG') {
      const cielab = SegmentSequenceItemMetadata.RecommendedDisplayCIELabValue;
      rgba = dcmjs.data.Colors.dicomlab2RGB(cielab).map(x => Math.round(x * 255));
    }
    
    if (metaData.Modality=='RTSTRUCT') {
      rgba = SegmentSequenceItemMetadata.ROIDisplayColor;
    }
    
    this.color = rgba;
    this.data = data;
    this.dataModified = false;
    this.listeners = [];
    this.metaData = metaData;
    this.SegmentSequenceItemMetadata = SegmentSequenceItemMetadata;
    this.visible = false;
    this.active = false;
    this.scalarArray = null;
    this.VTKData = null;
    
    this._setVTKData();
  }
  
  delete() {
    this.listeners = [];
    if(this.VTKData)
      this.VTKData.delete();
    if(this.scalarArray)
      this.scalarArray.delete();
  }
  
  addListener(listener) {
    const d = new Date();
    const n = d.getTime() + Math.round(Math.random()*100);
  
    const listenerObj = {
      listener,
      listenerId: n,
    };
    
    this.listeners.push(listenerObj);
    
    return n;
  }
  
  removeListener(listenerID) {
    const listenersIds = this.listeners.map(listenerObj => listenerObj.listenerId);
    
    const listenerIndex = listenersIds.indexOf(listenerID);
    
    if (listenerIndex<0)
      return;
    
    this.listeners.splice(listenerIndex, 1)
  }
  
  wasModified() {
    return this.dataModified;
  }
  
  updateContourSequence() {
    // Warning: we do not update all metada here. Just metaData.ContourSequence
    
    if (!this.dataModified)
      return;
    
    this.metaData.ContourSequence = [];
    
    const dims = this.VTKData.getDimensions();
        
    const getIndex = (point) => point[0] + point[1] * dims[0] + point[2] * dims[0] * dims[1];
    
    this.data = this.VTKData.getPointData().getScalars().getData();
    
    for (let k=0; k<dims[2]; k++) {
      let contourData = [];
      
      let isEmpty = true;
      for (let i=0; i<dims[0]*dims[1]; i++) {
        if (this.data[k*dims[0]*dims[1]+i]) {
          isEmpty = false;
          break;
        }
      }
    
      /*
      for (let j=0; j<dims[1]; j++) {
        for (let i=0; i<dims[0]; i++) {
          const el = this.data[getIndex([i, j, k])];
          
          let isBorder = false;
          
          // el==0 means outside
          if (el) {
            for (let dx= -1; dx <= 1 && !isBorder; dx++) {
              for (let dy = -1; dy <= 1 && !isBorder; dy++) {                
                const evalX = i + dx;
                const evalY = j + dy;
                    
                if (evalX >= 0 && evalX < dims[0] && evalY >= 0 && evalY < dims[1]) {
                  const hoodValue = this.data[getIndex([evalX, evalY, k])];
                      
                  if (!hoodValue)
                    isBorder = true;
                }
              }
            }
            if (isBorder) {
              const posX = i*this.metaData.PixelSpacing[0]+this.metaData.ImagePositionPatient[0];
              const posY = j*this.metaData.PixelSpacing[1]+this.metaData.ImagePositionPatient[1];
              const posZ = -(k+1-this.metaData.Slices)*this.metaData.SliceThickness+this.metaData.ImagePositionPatient[2];
                  
              contourData.push(posX);
              contourData.push(posY);
              contourData.push(posZ);
            }
          }
        }
      }*/
      
      if (!isEmpty) { //contourData.length
        console.log('Computing new contour data for slice ' + k)
        
        const contours = d3.contours()
          .size([dims[0], dims[1]])
          .thresholds([1])
          (this.data.slice(k*dims[0]*dims[1], (k+1)*dims[0]*dims[1]));
        
        for (let polyNb=0; polyNb<contours[0].coordinates.length; polyNb++){
          const poly = contours[0].coordinates[polyNb];
          
          let newContourData = [];
          
          for (let polyNb2=0; polyNb2<poly.length; polyNb2++) {
            const poly2 = poly[polyNb2];
            
            for (let pointNb=0; pointNb<poly2.length; pointNb++) {
              
              const posX = poly2[pointNb][0]*this.metaData.PixelSpacing[0]+this.metaData.ImagePositionPatient[0];
              const posY = poly2[pointNb][1]*this.metaData.PixelSpacing[1]+this.metaData.ImagePositionPatient[1];
              const posZ = this.metaData.ImagePositionPatient[2]-k*this.metaData.SliceThickness;
              
              newContourData.push(posX);
              newContourData.push(posY);
              newContourData.push(posZ);
            }
          }
          
          this.metaData.ContourSequence.push({
            ContourData: newContourData,
            NumberOfContourPoints: newContourData.length/3
          });
        }
      }
    }
    
    console.log('Computed a sequence of ' + this.metaData.ContourSequence.length)
    
    this.dataModified = false;
  }
  
  getContourSequence() {
    return this.metaData.ContourSequence;
  }
  
  modified(what) {
    if (what=="data")
      this.dataModified = true;
      
    this.listeners.slice().forEach(listenerObj => {listenerObj.listener(what);}); // Very important to slice because this.listeners could be changed by one of the listener running in forEach
  }
  
  setColor(color) {
    if (color == this.color)
      return;
      
    this.color = color.slice();
    this.modified('color');
  }
  
  setActive(active) {
    if (active == this.active)
      return;
      
    this.active = active;
    
    this.modified('active');
  }
  
  setVisible(visible) {
    if (visible == this.visible)
      return;
      
    this.visible = visible;
    
    this.modified('visible');
  }
  
  setData(data) {
    this.data = data;
    
    this.modified('data');
  }
  
  getColor() {
    return this.color;
  }
  
  getData() {
    return this.VTKData.getPointData().getScalars().getData();
  }
  
  getCenter() {
    if(this.metaData.Modality=='SEG')
      return null; //TODO
    
    if (!this.SegmentSequenceItemMetadata.ContourSequence || !this.SegmentSequenceItemMetadata.ContourSequence.length)
      return null;
      
    const cSliceNb = this.SegmentSequenceItemMetadata.ContourSequence.length;
    
    const ContourData = this.SegmentSequenceItemMetadata.ContourSequence[Math.floor(cSliceNb/2)].ContourData;
              
    const posX = ContourData.filter((elem, index) => !(index%3));
    const posY = ContourData.filter((elem, index) => !((index-1)%3));
    const posZ = ContourData.filter((elem, index) => !((index-2)%3));
              
    
    const sum = (arr) => arr.reduce((a, b) => a + b, 0);
    const avg = (arr) => (sum(arr) / arr.length) || 0;
    
    const metaData = this.metaData;
    const center = [avg(posX), avg(posY), avg(posZ)];
    
    return center;
  }
  
  isActive() {
    return this.active;
  }
  
  _setVTKData() {
    const scalarArray = vtkDataArray.newInstance({
          name: "Scalars",
          numberOfComponents: 1,
          values: this.data, // We do not slice on purpose => Change to VTKData == change to this.data
      });
    this.scalarArray = scalarArray;
      
    const imageData = vtkImageData.newInstance();
    
    scalarArray.onModified(() => this.modified('data'))
    
    const metaData = this.metaData;
    
    let direction = null;
    let dimension = null;
    let spacing = null;
    let ImagePositionPatient = [0, 0, 0];
    
    if (metaData.Modality=='SEG') {
      direction = metaData.SharedFunctionalGroupsSequence.PlaneOrientationSequence.ImageOrientationPatient;
      dimension = [metaData.Columns, metaData.Rows, metaData.ReferencedSeriesSequence.ReferencedInstanceSequence.length]
      spacing = [metaData.SharedFunctionalGroupsSequence.PixelMeasuresSequence.PixelSpacing[0],
        metaData.SharedFunctionalGroupsSequence.PixelMeasuresSequence.PixelSpacing[1],
        metaData.SharedFunctionalGroupsSequence.PixelMeasuresSequence.SliceThickness];
      /*ImagePositionPatient = [metaData.PerFrameFunctionalGroupsSequence[0].PlanePositionSequence.ImagePositionPatient[0],
        metaData.PerFrameFunctionalGroupsSequence[0].PlanePositionSequence.ImagePositionPatient[1],
        metaData.PerFrameFunctionalGroupsSequence[0].PlanePositionSequence.ImagePositionPatient[2]
      ]; *///TODO: INCORRECT
    }
    
    if (metaData.Modality=='RTSTRUCT') {      
      direction = metaData.ImageOrientationPatient;
      dimension = [metaData.Columns, metaData.Rows, metaData.Slices];
      spacing = [metaData.PixelSpacing[0], metaData.PixelSpacing[1], metaData.SliceThickness];
      ImagePositionPatient = metaData.ImagePositionPatient;
    }
    
    let columnStepToPatient = direction.slice(0,3);
    let rowStepToPatient = direction.slice(3,6);
    let direction3 = [0, 0, 0];
    vtkMath.cross(rowStepToPatient, columnStepToPatient, direction3);
    
    /*columnStepToPatient = columnStepToPatient.map(elem => elem?-elem:elem);
    rowStepToPatient = rowStepToPatient.map(elem => elem?-elem:elem);
    direction3 = direction3.map(elem => elem?-elem:elem);*/
    vtkMath.normalize(columnStepToPatient);
    vtkMath.normalize(rowStepToPatient);
    vtkMath.normalize(direction3);
  
    direction = columnStepToPatient.concat(rowStepToPatient).concat(direction3);
    
    imageData.setDimensions(dimension[0], dimension[1], dimension[2]);
    imageData.setSpacing(spacing[0], spacing[1], spacing[2]);
    imageData.setDirection(direction);
    imageData.setOrigin(ImagePositionPatient[0], ImagePositionPatient[1], ImagePositionPatient[2]); //TODO for SEG
    imageData.getPointData().setScalars(scalarArray);
    
    this.VTKData = imageData;
  }
  
  getVTKData() {
    return this.VTKData;
  }
  
  getLabel() {
    if (this.metaData.Modality=='SEG')
      return this.SegmentSequenceItemMetadata.SegmentDescription;
    
    if (this.metaData.Modality=='RTSTRUCT')
      return this.SegmentSequenceItemMetadata.ROIName; //TODO StructureSetROISequence and ROIContourSequence ordering might be different. Use ROINumber field to get the correct index in StructureSetROISequence
  }
  
  getNumber() {
    if (this.metaData.Modality=='SEG')
      return this.SegmentSequenceItemMetadata.SegmentNumber;
    if (this.metaData.Modality=='RTSTRUCT')
      return this.SegmentSequenceItemMetadata.ROINumber;
  }
  
  getVisible() {
    return this.visible;
  }
  
  getMetaData() {
    return this.metaData;
  }
}

