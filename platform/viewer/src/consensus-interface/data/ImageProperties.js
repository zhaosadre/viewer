
import presets from '../components/PatientModeling/presets.js';

export default class ImageProperties {
  constructor() {
    this.colors = [{pos: 0, color: [0, 0, 0]},
        {pos: 0.5, color: [0.5, 0.5, 0.5]},
        {pos: 1, color: [1, 1, 1]},
      ],
    
    this.opacity = 1;
    
    this.listeners = [];
    
    this.preset = null;
    
    const ctTransferFunctionPresetId = 'CT-Larynx';
    const preset = presets.find(
      preset => preset.id === ctTransferFunctionPresetId
    );
    
    this.setTransferFunction(preset);
    
    this.userPosition = null;
    
    this.wwl = [250, 50];
    
    this.api = {
      addImagePropertiesListener: (listener) => {this.addListener(listener)},
      getTransferFunction: () => {return this.getTransferFunction();},
      getWWL: () => {return this.getWWL();},
      removeImagePropertiesListener: (listener) => {this.removeListener(listener)},
      setWWL: (wwl) => {this.setWWL(wwl);},
      setTransferFunction: (preset) => {this.setTransferFunction(preset);},
    }
  }
  
  
  // Listeners
  addListener(listener) {
    this.listeners.push(listener);
  }
  
  removeListener(listener) {
    const index = this.listeners.indexOf(listener);
    
    if (index>-1)
      this.listeners.splice(index, 1);
  }
  
  // what can be colors, opacity, wwl, userPosition
  modified(what) {
    this.listeners.slice().forEach(listener => {listener(what);}); // Very important to slice because this.listeners could be changed by one of the listener running in forEach
  }
  
  
  // Get methods 
  getApi() {
    return this.api;
  }
   
  getColors() {
    return this.colors;
  }
  
  getOpacity() {
    return this.opacity;
  }
  
  getTransferFunction() {
    return this.preset;
  }
  
  getUserPosition() {
    if (!this.userPosition)
      return null;
      
    return this.userPosition.slice();
  }
  
  getWWL() {
    return this.wwl.slice();
  }
  
  
  // Set methods
  setColor(color) {
    this.colors = [{pos: 0, color: [0, 0, 0]},
      {pos: 0.5, color: [color[0], color[1], color[2]]},
      {pos: 1, color: [1, 1, 1]},
    ];
    
    this.modified('colors');
  }
  
  setColors(colors) {
    this.colors = colors.slice();
    
    this.modified('colors');
  }
  
  setOpacity(opacity) {
    this.opacity = opacity;
    
    this.modified('opacity');
  }
  
  setTransferFunction(preset) {
    this.preset = preset;
    
    let windowWidth = parseFloat(preset.windowWidth);
    let windowLevel = parseFloat(preset.windowLevel);
    
    this.setWWL([windowWidth, windowLevel]);
  }
  
  setUserPosition(position) {
    this.userPosition = position.slice();
    
    this.modified('userPosition');
  }
  
  setUserPositionCoordinate(values, indexes) {
    if(!this.userPosition)
      this.userPosition = [0, 0, 0];
      
    indexes.forEach((index, i) => {this.userPosition[index] = values[i]});
    
    this.modified('userPosition');
  }
  
  setWWL(wwl) {
    let windowWidth = wwl[0];
    let windowLevel = wwl[1];
    
    if(windowWidth<0.01 || isNaN(windowWidth))
      windowWidth = 0.01;
      
    if(windowLevel<-1000)
      windowLevel = -1000;
      
    if(windowLevel>3000)
      windowLevel = 3000;
      
    if (isNaN(windowLevel))
      windowLevel = 0;
      
      
    this.wwl = [windowWidth, windowLevel];
    
    this.modified('wwl');
  }
}

