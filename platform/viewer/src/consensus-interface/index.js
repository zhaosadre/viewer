
import React, { Component } from "react";
import ReactDOM from "react-dom";

import HomePage from './consensus-interface/components/HomePage.js';
import './consensus-interface/theme-tide.css';

ReactDOM.render(
  <HomePage />,
  document.getElementById('root')
);

