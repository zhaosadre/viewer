export default class ViewsApi 
{
	#apis = [];
	
	static activeWidget = null;
	static instance = null;
	
	static getInstance() 
	{
		if(ViewsApi.instance == null)
		{
			ViewsApi.instance = new ViewsApi();
		}
		
		return this.instance;
	}
	
	registerApi(newApi){
		this.#apis.push(newApi);
	}
	
	unregisterApi(api){		
		let indexToRemove = this.#apis.indexOf(api);
		
		if(indexToRemove > - 1){
			this.#apis.splice(indexToRemove, 1);
		}
	}
	
	clearApis(){
		this.#apis = [];
	}
	
  /************
   * ROITools *
   ************/ 
	
	callApiFunctionByName(name){
		let result = false;
		
		this.#apis.forEach(api => 
		{
			if(api.hasOwnProperty(name))
			{
				result = api[name]();
				
				if(!result)
				  result = true;
			}
		});
		
		return result;
	}
	
	callApiFunctionByNameWithArgs(name, args){
		let result = false;
		
		this.#apis.forEach(api => 
		{
			if(api.hasOwnProperty(name))
			{
				result = api[name](args);
				
				if(!result)
				  result = true;
			}
		});
		
		return result;
	}
	
	setActiveWidget(activeWidget) {
	  this.activeWidget = activeWidget;
	}
	
	getActiveWidget() {
	  return this.activeWidget;
	}
	
	getActiveWidgets(){
		let widgets = [];
		this.#apis.forEach(api => {
			if(api.hasOwnProperty(getActiveWidget))
				widgets.push(api.getActiveWidget())
		});
		return widgets;
	}
	
	addImagePropertiesListener(listener) {
	  return this.callApiFunctionByNameWithArgs("addImagePropertiesListener", listener);
	}
	
	removeImagePropertiesListener(listener) {
	  return this.callApiFunctionByNameWithArgs("removeImagePropertiesListener", listener);
	}
	
	getRadius() {
	  return this.callApiFunctionByName("getRadius");
	}
	
	getRenderWindow() {
		return this.callApiFunctionByName("getRenderWindow");
	}
	
	getTransferFunction() {
	  return this.callApiFunctionByName("getTransferFunction");
	}
	
	getWWL() {
	  return this.callApiFunctionByName("getWWL");
	}
	
	setWWL(ww, wl) {
	  const wwl = [ww, wl];
	  return this.callApiFunctionByNameWithArgs("setWWL", wwl);
	}
	
	setTransferFunction(preset) {
	   return this.callApiFunctionByNameWithArgs("setTransferFunction", preset);
	}
	
	startAdjust(){
		return this.callApiFunctionByName("startAdjust");
	}
	
	endAdjust(){
		return this.callApiFunctionByName("endAdjust");
	}
	
	startAdjustPrimary(){
		return this.callApiFunctionByName("startAdjustPrimary");
	}
	
	startAdjustSecondary(){
		return this.callApiFunctionByName("startAdjustSecondary");
	}
	
	adjustPrimarySecondary(value){
		return this.callApiFunctionByNameWithArgs("adjustPrimarySecondary", value);
	}
	
	setPaintRadius(radius){
		return this.callApiFunctionByNameWithArgs("setRadius", radius);
	}
	
	startBrush(){
		return this.callApiFunctionByName("startBrush");
	}
	
	endBrush(){
		return this.callApiFunctionByName("endBrush");
	}
	
	startCrossHairs(){
		return this.callApiFunctionByName("startCrossHairs");
	}
	
	endCrossHairs(){
		return this.callApiFunctionByName("endCrossHairs");
	}
	
	startErase(){
		return this.callApiFunctionByName("startErase");
	}
	
	endErase(){
		return this.callApiFunctionByName("endErase");
	}
	
	startSculpt(){
		return this.callApiFunctionByName("startSculpt");
	}
	
	endSculpt(){
		return this.callApiFunctionByName("endSculpt");
	}
	
	startPan(){
		return this.callApiFunctionByName("startPan");
	}
	
	endPan(){
		return this.callApiFunctionByName("endPan");
	}
	
	startZoom(){
		return this.callApiFunctionByName("startZoom");
	}
	
	endZoom(){
		return this.callApiFunctionByName("endZoom");
	}
	
	startLabel() {
	  return this.callApiFunctionByName("startLabel");
	}
	
	endLabel(){
		return this.callApiFunctionByName("endLabel");
	}
	
	undo() {
	  return this.callApiFunctionByName("undo");
	}
	
	redo() {
	  return this.callApiFunctionByName("redo");
	}
	
	adjustContourOpacity(opacity) {
	  return this.callApiFunctionByNameWithArgs("adjustContourOpacity", opacity);
	}
	
	adjustContourOutline(outline) {
	  return this.callApiFunctionByNameWithArgs("adjustContourOutline", outline);
	}
}
