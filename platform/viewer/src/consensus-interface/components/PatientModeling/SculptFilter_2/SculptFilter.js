
import { SlicingMode } from 'vtk.js/Sources/Rendering/Core/ImageMapper/Constants';
import { vec3 } from 'gl-matrix';


export default class SculptFilter {
  constructor() {
    this.globals = {
      data: null,
      buffer: null,
      dimensions: [0, 0, 0],
      prevPoint: null,
      slicingMode: null, // 2D or 3D painting
      fillValue: 1,
    };
    this.bufferSize = 0;
    this.bufferType = null;
  }
  
  start(bufferType, dimensions, slicingMode, data) {
    this.bufferType = bufferType;
    this.bufferSize = dimensions[0] * dimensions[1] * dimensions[2];
    this.globals.buffer = new self[this.bufferType](this.bufferSize);
    this.globals.dimensions = dimensions;
    this.globals.prevPoint = null;
    this.globals.slicingMode = slicingMode;
    this.globals.data = data;
  }
  
  end() {
    this.globals.buffer = null;
  }
  
  setData(data) {
    this.globals.data = data;
  }
  
  getBuffer() {
    const buffer2 = this.globals.buffer.buffer.slice();
    this.globals.buffer = new self[this.bufferType](this.bufferSize);
    
    return buffer2;
  }
  
  handleInitialPoint(center) {
    const yStride = this.globals.dimensions[0];
    const zStride = this.globals.dimensions[0] * this.globals.dimensions[1];
    
    const centerInd = center[1] * yStride + center[2] * zStride + center[0];
    
    let fillValue = 1;
    if (!this.globals.data[centerInd])
      fillValue = 255; // 255 means erase
      
    this.globals.fillValue = fillValue;
  }
  
  handleSculptEllipse(center, scale3) {      
    const radius3 = [...scale3];
    if (this.globals.slicingMode != null && this.globals.slicingMode !== SlicingMode.NONE) {
      const sliceAxis = this.globals.slicingMode % 3;
      radius3[sliceAxis] = 0.25;
    }

    const yStride = this.globals.dimensions[0];
    const zStride = this.globals.dimensions[0] * this.globals.dimensions[1];

    const zmin = Math.round(Math.max(center[2] - radius3[2], 0));
    const zmax = Math.round(
      Math.min(center[2] + radius3[2], this.globals.dimensions[2] - 1)
    );

    for (let z = zmin; z <= zmax; z++) {
      const dz = (center[2] - z) / radius3[2];
      const ay = radius3[1] * Math.sqrt(1 - dz * dz);

      const ymin = Math.round(Math.max(center[1] - ay, 0));
      const ymax = Math.round(
        Math.min(center[1] + ay, this.globals.dimensions[1] - 1)
      );

      for (let y = ymin; y <= ymax; y++) {
        const dy = (center[1] - y) / radius3[1];
        const ax = radius3[0] * Math.sqrt(1 - dy * dy - dz * dz);

        const xmin = Math.round(Math.max(center[0] - ax, 0));
        const xmax = Math.round(
          Math.min(center[0] + ax, this.globals.dimensions[0] - 1)
        );
        if (xmin <= xmax) {
          const index = y * yStride + z * zStride;
          this.globals.buffer.fill(this.globals.fillValue, index + xmin, index + xmax + 1);
        }
      }
    }
  }
  
  
  getDistanceToLabel(point) {
    this.handleInitialPoint(point);
    
    const max_ball_radius = 40;
    
    let currentRadius = 0;

    const yStride = this.globals.dimensions[0];
    const zStride = this.globals.dimensions[0] * this.globals.dimensions[1];

    const z = point[2];
    
    for (currentRadius=1; currentRadius<=max_ball_radius; currentRadius++){
      for (let YDelta=-currentRadius; YDelta<=currentRadius; YDelta++){
        for (let XDelta=-currentRadius; XDelta<=currentRadius; XDelta++){
          if (Math.sqrt(XDelta*YDelta)>currentRadius)
            continue;
            
            const x = point[0] + XDelta;
            const y = point[1] + YDelta
            
            const index = y * yStride + z * zStride + x;
            
            if((this.globals.data[index]==0 && this.globals.fillValue==1) || (this.globals.data[index]!=0 && this.globals.fillValue==255))
              return Math.max(currentRadius, 0);
        }
      }
    }
    
    return currentRadius;
  }
  
  handlePaint(point, radius, paintOrErase) {
    if (!this.globals.prevPoint) {
      this.globals.prevPoint = point;
    }

    // DDA params
    const delta = [
      point[0] - this.globals.prevPoint[0],
      point[1] - this.globals.prevPoint[1],
      point[2] - this.globals.prevPoint[2],
    ];
    const inc = [1, 1, 1];
    for (let i = 0; i < 3; i++) {
      if (delta[i] < 0) {
        delta[i] = -delta[i];
        inc[i] = -1;
      }
    }
    const step = Math.max(...delta);

    // DDA
    const thresh = [step, step, step];
    const pt = [...this.globals.prevPoint];
    for (let s = 0; s <= step; s++) {
      //handlePaintEllipse({ center: pt, scale3: radius, paintOrErase });
      this.handleSculptEllipse(pt, radius);
      
      for (let ii = 0; ii < 3; ii++) {
        thresh[ii] -= delta[ii];
        if (thresh[ii] <= 0) {
          thresh[ii] += step;
          pt[ii] += inc[ii];
        }
      }
    }

    this.globals.prevPoint = point;
  }
}

