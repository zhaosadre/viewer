import { vec3 } from 'gl-matrix';

import macro from 'vtk.js/Sources/macro';
import vtkImageData from 'vtk.js/Sources/Common/DataModel/ImageData';
import vtkDataArray from 'vtk.js/Sources/Common/Core/DataArray';
import vtkPolygon from 'vtk.js/Sources/Common/DataModel/Polygon';

import SculptFilter from './SculptFilter.js';

// ----------------------------------------------------------------------------
// vtkPaintFilter methods
// ----------------------------------------------------------------------------

export default class vtkSculptFilter {
  constructor() {
    this.active = false;
    this.history = {};
    this.model = {
      backgroundImage: null,
      labelMap: null,
      maskWorldToIndex: null,
      voxelFunc: null,
      radius: 1,
      label: 0,
      slicingMode: null,
    };
    this.paintFilter = new SculptFilter();
    
    this.resetHistory();
  }
  
  isActive() {
    return (this.active);
  }

  resetHistory() {
    this.history.index = -1;
    this.history.snapshots = [];
    this.history.labels = [];
  }

  pushToHistory(snapshot, label) {
    // Clear any "redo" info
    const spliceIndex = this.history.index + 1;
    const spliceLength = this.history.snapshots.length - this.history.index;
    this.history.snapshots.splice(spliceIndex, spliceLength);
    this.history.labels.splice(spliceIndex, spliceLength);

    // Push new snapshot
    this.history.snapshots.push(snapshot);
    this.history.labels.push(label);
    this.history.index++;
  }

  startStroke() {
    if (this.model.labelMap) {
      const scalars = this.model.labelMap.getPointData().getScalars();
      const data = scalars.getData();
    
      this.paintFilter.start('Uint8Array', this.model.labelMap.getDimensions(), this.model.slicingMode, data);
    }
    
    this.active = true;
  }
  
  applyBinaryMask (maskBuffer) {
    const scalars = this.model.labelMap.getPointData().getScalars();
    const data = scalars.getData();
    const maskLabelMap = new Uint8Array(maskBuffer);

    let diffCount = 0;
    for (let i = 0; i < maskLabelMap.length; i++) {
      // maskLabelMap is a binary mask
      diffCount += maskLabelMap[i];
    }

    // Format: [ [index, oldLabel], ...]
    // I could use an ArrayBuffer, which would place limits
    // on the values of index/old, but will be more efficient.
    const snapshot = new Array(diffCount);
    const label = this.model.label;

    let diffIdx = 0;
    if (this.model.voxelFunc) {
      const bgScalars = this.model.backgroundImage.getPointData().getScalars();
      for (let i = 0; i < maskLabelMap.length; i++) {
        if (maskLabelMap[i]) {
          const voxel = bgScalars.getTuple(i);
          // might not fill up snapshot
          if (this.model.voxelFunc(voxel, i, label)) {
            snapshot[diffIdx++] = [i, data[i]];
            data[i] = label;
          }
        }
      }
    } else {
      for (let i = 0; i < maskLabelMap.length; i++) {
        if (maskLabelMap[i]==1) {
          if (!data[i]) { // We do not use the label value in our application. Otherwise : if (data[i] != label)
            snapshot[diffIdx++] = [i, data[i]];
            data[i] = label;
          }
        }
        else if (maskLabelMap[i]==255) { // In our application 1=set, 0=not set, 255=erase
          if (data[i]) { // We do not use the label value in our application. Otherwise if (data[i]== label)
            snapshot[diffIdx++] = [i, data[i]];
            data[i] = 0;
          }
        }
      }
    }
    this.pushToHistory(snapshot, label);

    scalars.setData(data);
    scalars.modified();
    this.model.labelMap.modified();
    //this.modified();
  };

  endStroke(){
    this.paintFilter.end();
    this.active = false;
  }

  addPoint(point, paintOrErase) {
      if (!this.active)
        return;
        
      const worldPt = [point[0], point[1], point[2]];
      const indexPt = [0, 0, 0];
      vec3.transformMat4(indexPt, worldPt, this.model.maskWorldToIndex);
      indexPt[0] = Math.round(indexPt[0]);
      indexPt[1] = Math.round(indexPt[1]);
      indexPt[2] = Math.round(indexPt[2]);

      const spacing = this.model.labelMap.getSpacing();
      const radius = spacing.map((s) => this.model.radius / s);

      this.paintFilter.handlePaint(indexPt, radius, paintOrErase);
      const buffer = this.paintFilter.getBuffer();
      this.applyBinaryMask(buffer);
      const scalars = this.model.labelMap.getPointData().getScalars();
      const data = scalars.getData();
      this.paintFilter.setData(data);
  }
  
  addInitialPoint(point) {
    const worldPt = [point[0], point[1], point[2]];
    const indexPt = [0, 0, 0];
    vec3.transformMat4(indexPt, worldPt, this.model.maskWorldToIndex);
    indexPt[0] = Math.round(indexPt[0]);
    indexPt[1] = Math.round(indexPt[1]);
    indexPt[2] = Math.round(indexPt[2]);

    this.paintFilter.handleInitialPoint(indexPt);
  }
  
  getDistanceToLabel(point) {
    const worldPt = [point[0], point[1], point[2]];
    const indexPt = [0, 0, 0];
    vec3.transformMat4(indexPt, worldPt, this.model.maskWorldToIndex);
    indexPt[0] = Math.round(indexPt[0]);
    indexPt[1] = Math.round(indexPt[1]);
    indexPt[2] = Math.round(indexPt[2]);
    
    return (this.paintFilter.getDistanceToLabel(indexPt));
  }
  
  setLabelMap(labelMap) {
    this.model.labelMap = labelMap;
    this.model.maskWorldToIndex = this.model.labelMap.getWorldToIndex();
    
    const scalars = this.model.labelMap.getPointData().getScalars();
    const data = scalars.getData();
    this.paintFilter.setData(data);
  }
  
  setSlicingMode(sm) {
    this.model.slicingMode = sm;
  }
  
  setLabel(l) {
    this.model.label = l;
  }
  
  setRadius(r) {
    this.model.radius = r;
  }
  
  setBackgroundImage(bI) {
    this.model.backgroundImage = bI;
  }
}

