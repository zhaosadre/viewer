import React, { Component } from 'react';
import PropTypes from 'prop-types';
import vtkGenericRenderWindow from 'vtk.js/Sources/Rendering/Misc/GenericRenderWindow';
import vtkRenderer from 'vtk.js/Sources/Rendering/Core/Renderer';
import vtkWidgetManager from 'vtk.js/Sources/Widgets/Core/WidgetManager';

import vtkPaintWidget from 'vtk.js/Sources/Widgets/Widgets3D/PaintWidget';

import { ViewTypes } from 'vtk.js/Sources/Widgets/Core/WidgetManager/Constants';

import * as vtkMath from 'vtk.js/Sources/Common/Core/Math';
import vtkImageMapper from 'vtk.js/Sources/Rendering/Core/ImageMapper';
import vtkImageSlice from 'vtk.js/Sources/Rendering/Core/ImageSlice';
import vtkColorTransferFunction from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction';
import vtkPiecewiseFunction from 'vtk.js/Sources/Common/DataModel/PiecewiseFunction';
import vtkBoundingBox from 'vtk.js/Sources/Common/DataModel/BoundingBox';

import vtkPaintFilter from './PaintFilter';
import customInteractorStyle_slice from './customInteractorStyle_slice.js';

import vtkMatrixBuilder from 'vtk.js/Sources/Common/Core/MatrixBuilder';

import vtkVolume from 'vtk.js/Sources/Rendering/Core/Volume';
import vtkVolumeMapper from 'vtk.js/Sources/Rendering/Core/VolumeMapper'

//import vtkImageOutlineFilter from 'vtk.js/Sources/Filters/General/ImageOutlineFilter';
import vtkImageOutlineFilter from './ImageOutlineFilter';


import vtkOrientationMarkerWidget from 'vtk.js/Sources/Interaction/Widgets/OrientationMarkerWidget';
import vtkAnnotatedCubeActor from 'vtk.js/Sources/Rendering/Core/AnnotatedCubeActor';

import CrossHairs from "./CrossHairs.js";
import Overlay from "./Overlay.js";

import { vec3 } from 'gl-matrix';

import vtkActor from 'vtk.js/Sources/Rendering/Core/Actor';
import vtkMapper from 'vtk.js/Sources/Rendering/Core/Mapper';
import vtkSTLReader from 'vtk.js/Sources/IO/Geometry/STLReader';

import vtkLabelWidget from 'vtk.js/Sources/Interaction/Widgets/LabelWidget';
import TextAlign from 'vtk.js/Sources/Interaction/Widgets/LabelRepresentation/Constants';

import ViewsApi from './Services/ViewsApi'

import SVGContour from "./SVGContour.js";
import * as d3 from 'd3-contour';

import human_standing from "../../../../public/stl/human_standing.stl";


export default class View2D extends Component {
  static propTypes = {
    imageDataSet: PropTypes.object.isRequired,
    segDataSet: PropTypes.object,
    labelIndices: PropTypes.array,
    onCreated: PropTypes.func,
    onDestroyed: PropTypes.func,
    sliceMode: PropTypes.string.isRequired, // IJK
  };
  
  constructor(props) {
    super(props);

    this.state = {
      SeriesInstanceUID: this.props.SeriesInstanceUID,
      secondarySeriesInstanceUID: this.props.secondarySeriesInstanceUID,
      segDataSet: this.props.segDataSet,
    };    
    
    this.contourOpacity = 0.8;
    this.contourOutline = 1;
    this.overlay = null;
    this.upperLeftText = [];
    this.upperRightText = [];
    this.bottomLeftText = [];
    
    this.apiEnabled = false;
    this.activeLabelMapIndex = null;
    this.activeWidget = null;// this.props.activeWidget;
    this.activeSecondarySeriesInstanceUID = null;
    this.activeSeriesInstanceUID = null;
    
    this.crossHairs = null;
    this.currentSecondaryWWL = null;
    this.currentWWL = null;
    
    this.imagePropertiesListener = (whatChange) => {this.onPropertyChange(whatChange)};
    this.secondaryPropertiesListener = (whatChange) => {this.onSecondaryChange(whatChange)};
  
    this.labelMaps = [];
    this.listenerIds = [];
    
    this.orientationActor = null;
    this.orientationMapper = null;
    this.orientationWidget = null;
      
    this.painter = null;
    this.paintOption = "paint";
    this.primaryColors = null;
    
    this.reader = null;
    this.resizeObserver = null;
    
    this.secondaryColors = null;
    this.svgContour = [];
    
    this.scene = {
      camera:null,
      container: React.createRef(),
      iStyle: null,
      genericRenderWindow: null,
      openGLRenderWindow: null,
      renderer: null,
      renderWindow: null,
      widgetManager: null,
      paintHandle: null,
    };
    
    this.sliceMode = 'IJKXYZ'.indexOf(this.props.sliceMode) % 3;
    this.volumeMode = false;
    this.widgets = {};
    
	  this.api = {
	    adjustContourOpacity: (value) => {this.apiEnabled? this.adjustContourOpacity(value) : null},
	    adjustContourOutline: (value) => {this.apiEnabled? this.adjustContourOutline(value) : null;},
      adjustPrimarySecondary: (value) => {this.apiEnabled? this.adjustPrimarySecondary(value) : null;},
      endAdjust: () => {this.apiEnabled? this.endAdjust() : null;},
      endBrush: () => {this.apiEnabled? this.endBrush() : null;},
      endCrossHairs: () => {this.apiEnabled? this.endCrossHairs() : null;},
      endErase: () => {this.apiEnabled? this.endErase() : null;},
      endLabel: () => {this.apiEnabled? this.endLabel() : null;},
      endPan: () => {this.apiEnabled? this.endPan() : null;},
      endSculpt: () => {this.apiEnabled? this.endSculpt() : null;},
      endZoom: () => {this.apiEnabled? this.endZoom() : null;},
      //getActiveWidget: () => {return this.getActiveWidget()},
      getRadius: () => {return (this.apiEnabled?  this.getRadius() : null);},
      getRenderWindow: () => {return (this.apiEnabled? this.scene.renderWindow : null);},
      setActiveLabelMap: (labelVolumeIndex) => {return (this.apiEnabled? this.setActiveLabelMap(labelVolumeIndex) : null);},
      setRadius: (radius) => {this.apiEnabled? this.setRadius(radius) : null;},
      startAdjust: () => {this.apiEnabled? this.startAdjust() : null;},
      startAdjustPrimary: () => {this.apiEnabled? this.startAdjustPrimary() : null;},
      startAdjustSecondary: () => {this.apiEnabled? this.startAdjustSecondary() : null;},
      startBrush: () => {this.apiEnabled? this.startBrush() : null;},
      startCrossHairs: () => {this.apiEnabled? this.startCrossHairs() : null;},
      startErase: () => {this.apiEnabled? this.startErase() : null;},
      startLabel: () => {this.apiEnabled? this.startLabel() : null;},
      startPan: () => {this.apiEnabled? this.startPan() : null;},
      startSculpt: () => {this.apiEnabled? this.startSculpt() : null;},
      startZoom: () => {this.apiEnabled? this.startZoom() : null;},
    };
  }
  
  
  /*********
   * Tools *
   *********/ 
  getActiveWidget(){
	  return this.activeWidget;
  }
  
  adjustContourOpacity(opacity) {
    this.contourOpacity = opacity;
    
    let segs = [];
    
    if (this.props.segDataSet)
      segs = this.props.segDataSet.getSegs();
    
    segs.forEach((seg, index) => {
      const color = seg.getColor();
      this.setLabelColor(index, color, 1);
    });
  }
  
  adjustContourOutline(outline) {
    this.contourOutline = outline;
    
    let segs = [];
    
    if (this.props.segDataSet)
      segs = this.props.segDataSet.getSegs();
    
    segs.forEach((seg, index) => {
      const color = seg.getColor();
      this.setLabelColor(index, color, 1);
    });
  }
   
  endAdjust() {
    this.endAll();
  }
  
  endAll() {
    this.activeWidget = null;
    
    this.scene.widgetManager.disablePicking();
    this.scene.widgetManager.releaseFocus();
    
    this.scene.iStyle.setActiveTool(null);
    
    if (this.crossHairs)
      this.crossHairs.setPoint(null);
      
    this.upperLeftText[1] = "";
    this.upperLeftText[2] = "";
    
    if (this.upperRightText.length>1) {
      this.upperRightText[1] = "";
      this.upperRightText[2] = "";
    }
    
    if (this.overlay)
      this.overlay.setText(this.upperLeftText, this.upperRightText, this.bottomLeftText);
    
    this.readyAll();
    
    this.startAdjustPrimary()
  }
  
  endBrush() {
    this.endAll();
  }
  
  endCrossHairs() {
    this.endAll();
  }
  
  endErase() {
    this.endAll();
  }
  
  endLabel() {
    this.endAll();
  }
  
  endPan() {
    this.endAll();
  }
  
  endSculpt() {
    this.endAll();
  }
  
  endZoom() {
    this.endAll();
  }
  
  setPrimaryWWL() {
    const wwl = this.image.imageProperties.getWWL();
    const windowWidth = wwl[0];
    const windowLevel = wwl[1];
    
    const opacity = this.image.imageProperties.getOpacity();
    
    const colors = this.image.imageProperties.getColors();
    
    const cfun = vtkColorTransferFunction.newInstance();
    const ofun = vtkPiecewiseFunction.newInstance();
    
    ofun.addPoint(windowLevel-windowWidth/2, 1*opacity);
    ofun.addPoint(windowLevel, 1*opacity);
    ofun.addPoint(windowLevel+windowWidth/2, 1*opacity);
    
    colors.forEach(item => {
      cfun.addRGBPoint(windowLevel-windowWidth/2+item.pos*windowWidth, item.color[0], item.color[1], item.color[2])
    });
    
    this.image.actor.getProperty().setRGBTransferFunction(cfun);
    this.image.actor.getProperty().setPiecewiseFunction(ofun);
    
    this.upperLeftText[0] = 'Window W/L: ' + Number.parseFloat(windowWidth).toPrecision(4) + ' / ' + Number.parseFloat(windowLevel).toPrecision(4);
    if (this.overlay)
      this.overlay.setText(this.upperLeftText, this.upperRightText, this.bottomLeftText);
    
    this.scene.renderWindow.render();
  }
  
  getRadius() {
    return this.widgets.paintWidget.getRadius();
  }
  
  setRadius(radius) {
    if(radius<1)
      return;
      
    this.widgets.paintWidget.setRadius(radius);
    this.painter.setRadius(radius);
    
    this.scene.paintHandle.updateRepresentationForRender();
  }
  
  setSecondaryWWL() {
    const wwl = this.secondaryImage.imageProperties.getWWL();
    const windowWidth = wwl[0];
    const windowLevel = wwl[1];
    
    const opacity = this.secondaryImage.imageProperties.getOpacity();
    
    const colors = this.secondaryImage.imageProperties.getColors();
    
    const cfun = vtkColorTransferFunction.newInstance();
    const ofun = vtkPiecewiseFunction.newInstance();
    
    ofun.addPoint(windowLevel-windowWidth/2, 0*opacity);
    ofun.addPoint(windowLevel, 1*opacity);
    ofun.addPoint(windowLevel+windowWidth/2, 1*opacity);
    
    cfun.setRange(windowLevel-windowWidth/2, windowLevel+windowWidth/2);
    
    colors.forEach(item => {
      cfun.addRGBPoint(windowLevel-windowWidth/2+item.pos*windowWidth, item.color[0], item.color[1], item.color[2])
    });
    
    this.secondaryImage.actor.getProperty().setRGBTransferFunction(cfun);
    this.secondaryImage.actor.getProperty().setPiecewiseFunction(ofun);
    
    this.upperRightText[0] = 'Window W/L: ' + Number.parseFloat(windowWidth).toPrecision(4) + ' / ' + Number.parseFloat(windowLevel).toPrecision(4);
    if (this.overlay)
      this.overlay.setText(this.upperLeftText, this.upperRightText, this.bottomLeftText);
      
    this.scene.renderWindow.render();
  }
  
  setSlice() {
    let wPos = this.image.imageProperties.getUserPosition();
    
    if (!wPos)
      return;
    
    let worldPos = wPos.slice();
    
    const bounds = this.image.data.getBounds();
    
    if(worldPos[0]<bounds[0])
      worldPos[0] = bounds[0];
    if(worldPos[1]<bounds[2])
      worldPos[1] = bounds[2];
    if(worldPos[2]<bounds[4])
      worldPos[2] = bounds[4];
    if(worldPos[0]>bounds[1])
      worldPos[0] = bounds[1];
    if(worldPos[1]>bounds[3])
      worldPos[1] = bounds[3];
    if(worldPos[2]>bounds[5])
      worldPos[2] = bounds[5];
      
    const spacing = this.image.data.getSpacing();
    worldPos = [bounds[0]+Math.round((worldPos[0]-bounds[0])/spacing[0])*spacing[0],
      bounds[2]+Math.round((worldPos[1]-bounds[2])/spacing[1])*spacing[1],
      bounds[4]+Math.round((worldPos[2]-bounds[4])/spacing[2])*spacing[2]];
    
    let direction = this.getDataDirection();    
    
    let normalVec;   
    switch (this.sliceMode) {
      case 'IJKXYZ'.indexOf("I") % 3:
        normalVec = direction.slice(0, 3);
        normalVec = normalVec.map(d => -d);
        break;
      case 'IJKXYZ'.indexOf("J") % 3:
        normalVec = direction.slice(3, 6);
        normalVec = normalVec.map(d => -d);
        break;
      case 'IJKXYZ'.indexOf("K") % 3:
        normalVec = direction.slice(6, 9);
        break;
    }
    
    let cameraPosition = this.scene.camera.getPosition().slice();
    let d = -(normalVec[0]*cameraPosition[0] + normalVec[1]*cameraPosition[1] + normalVec[2]*cameraPosition[2]);
    
    let dist = Math.abs(worldPos[0]*normalVec[0]+worldPos[1]*normalVec[1]+worldPos[2]*normalVec[2]+d)/Math.sqrt(normalVec[0]*normalVec[0]+normalVec[1]*normalVec[1]+normalVec[2]*normalVec[2]);
    
    let focalPoint = cameraPosition.map((p, index) => p-dist*normalVec[index]);
  
    this.scene.camera.set({focalPoint});
    //this.scene.camera.setDistance(dist);
    
    let position = focalPoint.slice(); 
      if (this.activeWidget == "crossHairs") {
        if (this.crossHairs)
          this.crossHairs.setPoint(worldPos);
        
        let indexPt = [0, 0, 0];
        vec3.transformMat4(indexPt, worldPos, this.image.data.getWorldToIndex());
        indexPt = indexPt.map(elem => Math.round(elem));
        
        const dimensions = this.image.data.getDimensions();
        const yStride = dimensions[0];
        const zStride = dimensions[0] * dimensions[1];
        
        const ind = indexPt[1] * yStride + indexPt[2] * zStride + indexPt[0];
          
        this.upperLeftText[1] = 'Position: ' +  Number.parseFloat(worldPos[0]).toPrecision(4)
          + ' ' + Number.parseFloat(worldPos[1]).toPrecision(4)
          + ' ' + Number.parseFloat(worldPos[2]).toPrecision(4)
          + ' mm';
        
        const data = this.image.data.getPointData().getScalars().getData();
        this.upperLeftText[2] = "Pixel value: " + data[ind];
        
        if (this.secondaryImage.data) {
          this.upperRightText[1] = 'Position: ' +  Number.parseFloat(worldPos[0]).toPrecision(4)
            + ' ' + Number.parseFloat(worldPos[1]).toPrecision(4)
            + ' ' + Number.parseFloat(worldPos[2]).toPrecision(4);
            + ' mm';
          
          const data = this.secondaryImage.data.getPointData().getScalars().getData();
          this.upperRightText[2] = "Pixel value: " + data[ind];
        }
        
        const image = this.image.datasetImage;
        if(image.getTrueDirectionDiffers()) {
          this.bottomLeftText[0] = "Image orientation is not taken into account."
          this.bottomLeftText[1] = "Pixel position is incorrect!"
        }
        if (this.overlay)
          this.overlay.setText(this.upperLeftText, this.upperRightText, this.bottomLeftText);
    }
    
    this.scene.renderWindow.render();
    
    if (this.props.segDataSet) {
      const segs = this.props.segDataSet.getSegs();
      segs.forEach((seg, index) => {this.updateSegSlice(index)});
    }
  }
  
  updateSegSlice(index) {
    if (!this.props.segDataSet)
      return;
      
    let worldPos = this.image.imageProperties.getUserPosition();
    
    if (!worldPos)
      return;
      
    const bounds = this.image.data.getBounds();
    const spacing = this.image.data.getSpacing();
    worldPos = [bounds[0]+Math.round((worldPos[0]-bounds[0])/spacing[0])*spacing[0],
      bounds[2]+Math.round((worldPos[1]-bounds[2])/spacing[1])*spacing[1],
      bounds[4]+Math.round((worldPos[2]-bounds[4])/spacing[2])*spacing[2]];
    
    
    const segs = this.props.segDataSet.getSegs();
    const seg = segs[index]
    
    if (!seg.getVisible()) {
      this.svgContour[index].setData([], []);
        return;
    }
    
    const dimensions = this.image.data.getDimensions();
    const yStride = dimensions[0];
    const zStride = dimensions[0] * dimensions[1];
    
    const data = seg.getData();
    let svgData = [];
    let contours = null;
    let indexPt = [0, 0, 0];
    
    switch (this.sliceMode) {
      case 0:
        for (let z=dimensions[2]-1 ; z>=0 ; z--) {
          for (let y=0 ; y<dimensions[1] ; y++) {
            vec3.transformMat4(indexPt, worldPos, this.image.data.getWorldToIndex());
            indexPt = indexPt.map(elem => Math.round(elem));
            
            const ind = y*yStride + z*zStride + indexPt[0];
            svgData.push(data[ind]);
          }
        }
        
        try {
          contours = d3.contours()
            .size([dimensions[1], dimensions[2]], 1)
            .thresholds([0.5])
            (svgData);
        } catch (e) {
          this.svgContour[index].setData([], []);
          return;
        }
        break;
      case 1:
        for (let z=dimensions[2]-1 ; z>=0 ; z--) {
          for (let x=0 ; x<dimensions[1] ; x++) {
            vec3.transformMat4(indexPt, worldPos, this.image.data.getWorldToIndex());
            indexPt = indexPt.map(elem => Math.round(elem));
            
            const ind = indexPt[1]*yStride + z*zStride + x;
            svgData.push(data[ind]);
          }
        }
        
        try {
          contours = d3.contours()
            .size([dimensions[1], dimensions[2]])
            .thresholds([0.5])
            (svgData);
        } catch (e) {
          this.svgContour[index].setData([], []);
          return;
        }
        break;
      case 2:
        vec3.transformMat4(indexPt, worldPos, this.image.data.getWorldToIndex());
        indexPt = indexPt.map(elem => Math.round(elem));
        
        const ind = indexPt[2] * zStride;
    
        svgData = data.slice(ind, (ind+1)*zStride);
        try {
          contours = d3.contours()
            .size([dimensions[0], dimensions[1]])
            .thresholds([0.5])
            (svgData);
        } catch (e) {
          this.svgContour[index].setData([], []);
          return;
        }
    }
        
    let newContourDataArray = []
          
    for (let polyNb=0; polyNb<contours[0].coordinates.length; polyNb++){
      const poly = contours[0].coordinates[polyNb];
          
      for (let polyNb2=0; polyNb2<poly.length; polyNb2++) {
        const poly2 = poly[polyNb2];
        
        let newContourData = [];
        
        for (let pointNb=0; pointNb<poly2.length; pointNb++) {
          let posX, posY, posZ;
          
          switch (this.sliceMode) {
            case 0:
              posX = 0;
              posY = poly2[pointNb][0]*spacing[1]+bounds[2]-spacing[1]/2;
              posZ = poly2[pointNb][1]*spacing[2]+bounds[4]-spacing[2]/2;
              break;
            case 1:
              posX = poly2[pointNb][0]*spacing[0]+bounds[0]-spacing[0]/2;
              posY = 0;
              posZ = poly2[pointNb][1]*spacing[2]+bounds[4]-spacing[2]/2;
              break;
            case 2:
              posX = poly2[pointNb][0]*spacing[0]+bounds[0]-spacing[0]/2;
              posY = poly2[pointNb][1]*spacing[1]+bounds[2]-spacing[1]/2;
              posZ = 0;
          }
              
          //TODO: get posX and posY for sliceMode 0 and 1
             
          newContourData.push([posX, posY, posZ]);
        }
        
        newContourDataArray.push(newContourData);
      }
    }
        
    const color = seg.getColor();
    this.svgContour[index].setData(newContourDataArray, color);
  }
  
  updateSegColor(index) {
    const segs = this.props.segDataSet.getSegs();
    const seg = segs[index]
    const color = seg.getColor();
    this.svgContour[index].setData(this.svgContour[index].getData(), color);
  }
  
  startAdjust() {
    this.startAdjustPrimary();
  }
  
  startBrush() {
    if (this.activeWidget)
      this.endAll();
        
    this.activeWidget = 'paintWidget';
    this.paintOption = 'paint';
    
    this.scene.iStyle.setActiveTool("noLeft");
    
    this.updateHandles();
    this.scene.widgetManager.grabFocus(this.widgets.paintWidget);
    this.scene.widgetManager.enablePicking();
    
    this.readyAll();
  }
  
  startCrossHairs() {
    if (this.activeWidget)
      this.endAll();
        
    this.activeWidget = 'crossHairs';
    this.scene.iStyle.setSliceMode(this.sliceMode);
    this.scene.iStyle.setActiveTool('crosshairs');
    this.readyAll();
    
    this.setSlice();
  }
  
  startErase() {
    if (this.activeWidget)
      this.endAll();
        
    this.activeWidget = 'paintWidget';
    this.paintOption = "erase";
    
    this.scene.iStyle.setActiveTool("noLeft");
    
    this.updateHandles();
    this.scene.widgetManager.grabFocus(this.widgets.paintWidget);
    this.scene.widgetManager.enablePicking();
    
    this.readyAll();
  }
  
  startLabel() {
    const widget = vtkLabelWidget.newInstance();
    widget.setInteractor(this.scene.renderWindow.getInteractor());
    widget.setEnabled(1);
    widget.getWidgetRep().setLabelText('New label');
    widget.getWidgetRep().setLabelStyle({
      fontColor: 'red',
      fontStyle: 'normal',
      fontSize: '12',
      fontFamily: 'Arial',
      strokeColor: 'red',
      strokeSize: '1',
    });
    widget2.getWidgetRep().setWorldPosition(this.image.imageProperties.getUserPosition());
  }
  
  startPan() {
    if (this.activeWidget)
        this.endAll();

    this.activeWidget = 'pan';
    
    this.scene.iStyle.setActiveTool('pan');
    
    this.readyAll();
  }
  
  startSculpt() {
    if (this.activeWidget)
      this.endAll();
        
    this.activeWidget = 'paintWidget';
    this.paintOption = "sculpt";
    
    this.scene.iStyle.setActiveTool("noLeft");
    
    this.updateHandles();
    this.scene.widgetManager.grabFocus(this.widgets.paintWidget);
    this.scene.widgetManager.enablePicking();
    
    this.readyAll();
  }
  
  startZoom() {
    if (this.activeWidget)
        this.endAll();

    this.activeWidget = 'zoom';
    
    this.scene.iStyle.setActiveTool('zoom');
    
    this.readyAll();
  }
  
  startWidget(activeWidget) {
    switch (activeWidget) {
      case "adjust":
        this.startAdjust();
        break;
      case "brush":
        this.startBrush();
        break;
      case "crossHairs":
        this.startCrossHairs();
        break;
      case "erase":
        this.startErase();
        break;
      case "sculpt":
        this.startSculpt();
        break;
      case "zoom":
        this.startZoom();
        break;
    }
  }
  
  /**********
   * Fusion *
   **********/
  adjustPrimarySecondary(value) {
    this.image.actor.getProperty().setOpacity(Math.min((1-value)*2, 1));
    this.secondaryImage.actor.getProperty().setOpacity(value);
    
    this.scene.genericRenderWindow.resize();
  }
  
  startAdjustPrimary() {
    if (this.activeWidget)
        this.endAll();

    this.activeWidget = 'adjust';
    
    const imageProperties = this.image.imageProperties;
    const currentWWL = imageProperties.getWWL();
    
    this.scene.iStyle.setWWLFunc((ww, wl) => {imageProperties.setWWL([ww, wl])});
    this.scene.iStyle.getWWLFunc(() => {return imageProperties.getWWL()});
    this.scene.iStyle.setActiveTool('wwl');
  }
  
  startAdjustSecondary() {
    if (this.activeWidget)
        this.endAll();

    this.activeWidget = 'adjust';
    
    const imageProperties = this.secondaryImage.imageProperties;
    
    if (imageProperties) {
      const currentWWL = imageProperties.getWWL();
      
      this.scene.iStyle.setWWLFunc((ww, wl) => {imageProperties.setWWL([ww, wl])});
      this.scene.iStyle.getWWLFunc(() => {return imageProperties.getWWL()});
      this.scene.iStyle.setActiveTool('wwl');
    }
  }
  
  
  /******************
   * Main functions *
   ******************/
  onPropertyChange(whatChange) {
    switch  (whatChange) {
      case 'colors':
        this.setPrimaryWWL();
        break;
      case "opacity":
        this.setPrimaryWWL();
        break;
      case "userPosition":
        this.setSlice();
        break;
      case 'wwl':
        this.setPrimaryWWL();
        break;
    }
  }
  
  onSecondaryChange(whatChange) {
    switch  (whatChange) {
      case 'colors':
        this.setSecondaryWWL();
        break;
      case "opacity":
        this.setSecondaryWWL();
        break;
      case "userPosition":
        break; // setSlice is arleady called by primary image
      case 'wwl':
        this.setSecondaryWWL();
        break;
    }
  }
  
  onSegChange(labelVolumeIndex, whatChange) {
    const segs = this.state.segDataSet.getSegs();
    const seg = segs[labelVolumeIndex];
    
    switch  (whatChange) {
      case 'color': 
        //this.setLabelColor(labelVolumeIndex, seg.getColor(), 1);
        this.updateSegColor(labelVolumeIndex);
        break;
      case 'visible': 
        //this.setVisibleLabelMap(labelVolumeIndex, seg.getVisible());
        this.updateSegSlice(labelVolumeIndex);
        break;
      case 'active':
        if (!seg.isActive())
          break; // TODO: Deactivate
        this.setActiveLabelMap(labelVolumeIndex);
        break;
      case 'data':
        this.updateSegSlice(labelVolumeIndex);
        break;
    }
  }

  readyAll() {
    this.updateHandles();
    
    //this.scene.genericRenderWindow.resize();
  }
  
  setActiveLabelMap(labelVolumeIndex) {
    this.activeLabelMapIndex = labelVolumeIndex;
    
    this.labelMap = this.labelMaps[this.activeLabelMapIndex];
    
    if (this.labelMap) {
      const labelData = this.labelMap.data; // TODO
      this.painter.setLabelMap(labelData);
      
      //TODO: I did not found how to remove this callback. This should be done in componentWillUnmount. In the meantime I just call resize if component was not unmounted.
      labelData.onModified(() => {if(!this.scene.genericRenderWindow) return; this.scene.genericRenderWindow.resize();});
      
      this.updatePaintWidget();
    }
  }
  
  setLabelColor(labelVolumeIndex, color, opacity) {
    const cfun = vtkColorTransferFunction.newInstance();
    const ofun = vtkPiecewiseFunction.newInstance();
      
    cfun.addRGBPoint(1, 0, 0, 0);
    cfun.addRGBPoint(1, color[0]/255.0, color[1]/255.0, color[2]/255.0);
    cfun.addRGBPoint(2, color[0]/255.0, color[1]/255.0, color[2]/255.0);
    ofun.addPoint(0, 0); // our background value, 0, will be invisible
    ofun.addPoint(0.5, 1);
    ofun.addPoint(1, this.contourOutline); // 1 after outline filter means outline
    ofun.addPoint(2, this.contourOpacity); // 2 after outline filter means inside

    this.labelMaps[labelVolumeIndex].cfun = cfun;
    this.labelMaps[labelVolumeIndex].ofun = ofun;
    
    this.labelMaps[labelVolumeIndex].actor.getProperty().setRGBTransferFunction(cfun);
    this.labelMaps[labelVolumeIndex].actor.getProperty().setPiecewiseFunction(ofun);
    this.labelMaps[labelVolumeIndex].actor.getProperty().setInterpolationTypeToNearest();
    this.labelMaps[labelVolumeIndex].actor.getProperty().setOpacity(opacity);
    
    this.scene.renderWindow.render();
  }
  
  clamp(value, min, max) {
  if (value < min) {
    return min;
  }
  if (value > max) {
    return max;
  }
  return value;
}

  getDataDirection() {
    /*let direction =  Array.from(this.image.data.getDirection());
    
    console.log(direction)
    
    let dir0 = direction.slice(0, 3);
    let dir1 = direction.slice(3, 6);
    let dir2 = direction.slice(6, 9);
    
    let dirs = [dir0, dir1, dir2];
    
    let indM0 = dir0.map(e => Math.abs(e)).indexOf(Math.max.apply(null, dir0.map(e => Math.abs(e))));
    let indM1 = dir1.map(e => Math.abs(e)).indexOf(Math.max.apply(null, dir1.map(e => Math.abs(e))));
    let indM2 = dir2.map(e => Math.abs(e)).indexOf(Math.max.apply(null, dir2.map(e => Math.abs(e))));
    
    let inds = [indM0, indM1, indM2];
    
    let ind0 = inds.indexOf(0);
    direction = dirs[ind0];
    
    let ind1 = inds.indexOf(1);
    direction = direction.concat(dirs[ind1]);
    
    let ind2 = inds.indexOf(2);
    direction = direction.concat(dirs[ind2]);
    
    direction = direction.map(e => Math.round(e));
    
    console.log(direction)
    
    return direction;*/
    
    return [1, 0, 0, 0, 1, 0, 0, 0, -1].slice();
  }
  setCamera(sliceMode, renderer, data) {    
    let direction = this.getDataDirection();
    
    let dir; // dir = DirectionOfProjection of the camera
    
    switch (this.sliceMode) {
      case 'IJKXYZ'.indexOf("I") % 3:
        dir = direction.slice(0, 3);
        dir = dir.map(d => -d);
        break;
      case 'IJKXYZ'.indexOf("J") % 3:
        dir = direction.slice(3, 6);
        dir = dir.map(d => -d);
        break;
      case 'IJKXYZ'.indexOf("K") % 3:
        dir = direction.slice(6, 9);
        break;
    }
    
    let ijk = this.image.data.getDimensions().map(d => d/2);
    let focalPoint = [0, 0, 0];
    data.indexToWorldVec3(ijk, focalPoint); // Position = first voxel
    
    let initialFocalPoint = focalPoint.slice();
    let position = focalPoint.map((p, i) => p + dir[i]);
    
    let viewUp; 

    switch (this.sliceMode) {
      case 'IJKXYZ'.indexOf("I") % 3:
        viewUp = direction.slice(6, 9);
        break;
      case 'IJKXYZ'.indexOf("J") % 3:
        viewUp = direction.slice(6, 9);
        break;
      case 'IJKXYZ'.indexOf("K") % 3:
        viewUp = direction.slice(3, 6);
        break;
    }
    
    viewUp = [-viewUp[0], -viewUp[1], -viewUp[2]];
    
    
    const camera = renderer.getActiveCamera();
    camera.setParallelProjection(true);
    camera.set({position, focalPoint, viewUp}); 
    
    this.cameraPosition = position.slice();

    renderer.resetCamera();
    
    camera.zoom(1.5);
    
    position = initialFocalPoint.map((p, i) => p + camera.getDistance()*dir[i]);
    camera.set({position, focalPoint:initialFocalPoint}); 
    
    this.scene.renderWindow.render();
    this.scene.genericRenderWindow.resize();
    
    this.setSlice();
  }
  
  setVisibleLabelMap(labelVolumeIndex, visible) {
    this.labelMaps[labelVolumeIndex].actor.setVisibility(visible);
    this.scene.renderWindow.render();
    this.readyAll();
  }
  
  initializeHandles() {
    this.updateHandles();
    
    this.scene.paintHandle.onStartInteractionEvent(() => {
      this.painter.startStroke();
        
      let pt = this.widgets.paintWidget.getWidgetState().getTrueOrigin();
      this.painter.addInitialSculptPoint(pt);
      this.painter.addPoint(pt, this.paintOption);
    });
      
    this.scene.paintHandle.onInteractionEvent(() => {
      let pt = this.widgets.paintWidget.getWidgetState().getTrueOrigin();
      this.painter.addPoint(pt, this.paintOption);
    });
      
    this.scene.paintHandle.onEndInteractionEvent(() => {
      this.painter.endStroke();
    });
  }
  
  updateHandles() {
    this.scene.paintHandle.setVisibility(this.activeWidget === 'paintWidget');
    this.scene.paintHandle.updateRepresentationForRender(); 
  }
  
  updatePaintWidget() {
      if (this.labelMap) {
        let position = this.scene.camera.getFocalPoint().slice();
        const normal = this.scene.camera.getDirectionOfProjection().slice();
        vtkMath.normalize(normal);
      
        const spacing = this.labelMap.data.getSpacing();
        position[this.sliceMode] = position[this.sliceMode] ; //- spacing[this.sliceMode]; //???
        
        this.widgets.paintWidget.getManipulator().setOrigin(position);
        this.widgets.paintWidget.getManipulator().setNormal(normal);
        
        this.painter.setSlicingMode(this.sliceMode);
        
      
        this.labelMaps.forEach(labelMap => {labelMap.imageMapper.set(this.image.imageMapper.get('slice', 'slicingMode'));}); // TODO: slice might differ. Recompute like for secondaryImage
      }
    };
  
  
  regrabFocus() {
    if (this.activeWidget=="paintWidget")
      this.scene.widgetManager.grabFocus(this.widgets.paintWidget);
  }
  
  releaseFocus() {
    if (this.activeWidget=="paintWidget")
      this.scene.widgetManager.releaseFocus();
  }
  
  /********
   * Main *
   ********/
  componentDidMount() {
    console.log('Component did mount');
    if(!this.props.imageDataSet.getImage(this.props.SeriesInstanceUID))
      return;
      
    if(this.props.secondarySeriesInstanceUID && !this.props.imageDataSet.getImage(this.props.secondarySeriesInstanceUID))
      return;
      
    this.scene.genericRenderWindow = vtkGenericRenderWindow.newInstance({
      background: [0, 0, 0],
    });
    
    this.scene.genericRenderWindow.setContainer(this.scene.container.current);
    this.scene.renderer = this.scene.genericRenderWindow.getRenderer();
    this.scene.renderWindow = this.scene.genericRenderWindow.getRenderWindow();
    this.scene.openGLRenderWindow = this.scene.genericRenderWindow.getOpenGLRenderWindow();
    this.scene.camera = this.scene.renderer.getActiveCamera();
    
    this.resizeObserver = new ResizeObserver(() => {
      console.log('Resize detected'); 
      this.readyAll();
      
      this.scene.genericRenderWindow.resize();
      
      if(this.crossHairs)
        this.crossHairs.resize();
        
      this.svgContour.forEach(contour => contour.resize());
        
      this.scene.container.current.style.cursor = "default";
      })
    this.resizeObserver.observe(this.scene.container.current);

    let sliceMode = 'IJKXYZ'.indexOf(this.props.sliceMode) % 3;
    this.sliceMode = sliceMode;
    
    // background image pipeline
    this.image = {
      datasetImage: this.props.imageDataSet.getImage(this.props.SeriesInstanceUID),
      actor: vtkImageSlice.newInstance(),
      cfun: vtkColorTransferFunction.newInstance(),
      data: null,
      imageMapper: vtkImageMapper.newInstance(),
      imageProperties: null,
      ofun: vtkPiecewiseFunction.newInstance(),
    };
    
    this.secondaryImage = {
      data: null,
      imageProperties: null,
      imageMapper: vtkImageMapper.newInstance(),
      actor: vtkImageSlice.newInstance(),
      cfun: vtkColorTransferFunction.newInstance(),
      ofun: vtkPiecewiseFunction.newInstance(),
    };
    
    this.image.actor.setMapper(this.image.imageMapper);
    
    const image = this.props.imageDataSet.getImage(this.props.SeriesInstanceUID);
    
    this.image.data = image.getVTKData();
    this.image.imageProperties = image.getImageProperties();
    
    this.image.imageMapper.setInputData(this.image.data);
    
    this.image.imageMapper.setSlicingMode(this.sliceMode);
    this.image.imageMapper.setSliceAtFocalPoint(true);
    
    this.activeSeriesInstanceUID = this.props.SeriesInstanceUID; // To avoid component from being unecessarily remounted
    
    // add actors to renderers
    this.scene.renderer.addViewProp(this.image.actor);
    
    this.setPrimaryWWL();
    
    
    // Secondary image
    const secondaryImage = this.props.imageDataSet.getImage(this.props.secondarySeriesInstanceUID);
    
    if (secondaryImage) {    
      this.secondaryImage.actor.setMapper(this.secondaryImage.imageMapper);
      
      this.secondaryImage.data = secondaryImage.getVTKData();
      this.secondaryImage.imageProperties = secondaryImage.getImageProperties();
      
      this.secondaryImage.imageProperties.addListener(this.secondaryPropertiesListener);
      
      this.secondaryImage.imageMapper.setInputData(this.secondaryImage.data);
      
      this.scene.renderer.addViewProp(this.secondaryImage.actor);
    
      this.setSecondaryWWL();
      
      this.secondaryImage.imageMapper.setSlicingMode(this.sliceMode);
      this.secondaryImage.imageMapper.setSliceAtFocalPoint(true);
      
      this.activeSecondarySeriesInstanceUID = this.props.secondarySeriesInstanceUID;
    }
    else
      this.activeSecondarySeriesInstanceUID = null;
    
    // Label data
    let segs = [];
    let activeLabelMap = null;
    
    if (this.props.segDataSet)
      segs = this.props.segDataSet.getSegs();
    
    segs.forEach((seg, index) => {
      const listnerID = seg.addListener((whatChange) => {this.onSegChange(index, whatChange)});
      this.listenerIds.push(listnerID);
      
      const data = seg.getVTKData();
      
      let image = null;
      
      image = {
        imageMapper: vtkImageMapper.newInstance(),
        actor: vtkImageSlice.newInstance(),
        cfun: null,
        ofun: null,
        outline: vtkImageOutlineFilter.newInstance(),
      };
      
      const color = seg.getColor();
        
      image.imageMapper.setSlicingMode(this.sliceMode);
      image.imageMapper.setSliceAtFocalPoint(true);
      
      image.data = data;
      
      image.actor.setMapper(image.imageMapper);
      
      image.outline.setInputData(image.data);
      image.outline.setSlicingMode(this.sliceMode);
      image.imageMapper.setInputConnection(image.outline.getOutputPort());
      
      
      this.scene.renderer.addViewProp(image.actor);
      
      //image.actor.setVisibility(seg.getVisible());
      image.actor.setVisibility(false);
      
      image.labelIndex = index;
      
      this.labelMaps.push(image);
      
      this.setLabelColor(index, color, 1);
      
      if (seg.isActive())
        activeLabelMap = index;
        
      this.svgContour.push(SVGContour.newInstance({genericRenderWindow: this.scene.genericRenderWindow, container:this.scene.container.current, sliceMode:this.sliceMode}));
    });

    
    this.scene.iStyle = customInteractorStyle_slice.newInstance();
    
    this.scene.renderWindow.getInteractor().setInteractorStyle(this.scene.iStyle);
    
    this.scene.iStyle.setSliceMode(this.sliceMode);
    this.scene.iStyle.setVolumeMapper(this.image.imageMapper);
    this.scene.iStyle.setActiveTool(null);
    
    // TODO only do this for secondary and primary image, not for all
    this.scene.iStyle.addCrossHairsListener((values, indexes) => 
      this.props.imageDataSet.getImages().forEach(image => image.getImageProperties().setUserPositionCoordinate(values, indexes)));
    this.scene.iStyle.setRegrabFocus(() => this.regrabFocus());
    this.scene.iStyle.setReleaseFocus(() => this.releaseFocus());
    
    this.setCamera(this.sliceMode, this.scene.renderer, this.image.data);
    
    
    const ijk = [0, 0, 0];
    ijk[this.sliceMode] = 1;
    
    /*
    // Nothing below is working without an interaction of the user => Why???
    this.scene.iStyle.setSliceNormal(ijk[0], ijk[1], ijk[2]);
    const range = this.scene.iStyle.getSliceRange();
    this.scene.iStyle.setSlice((range[0] + range[1]) / 2);
    this.scene.renderer.resetCamera();
    this.scene.renderWindow.render();
    this.scene.genericRenderWindow.resize();*/
    
    
    // Widgets
    this.scene.widgetManager = vtkWidgetManager.newInstance();
    this.scene.widgetManager.setRenderer(this.scene.renderer);
    
    this.scene.widgetManager.releaseFocus();
    this.scene.widgetManager.disablePicking();
    
    this.widgets.paintWidget = vtkPaintWidget.newInstance();
    
    this.scene.paintHandle = this.scene.widgetManager.addWidget(
      this.widgets.paintWidget,
      ViewTypes.SLICE // error when using VOLUME
    );
    
    // Paint filter
    this.painter = vtkPaintFilter.newInstance();    
    
    this.painter.setSlicingMode(ijk);    
    
    this.scene.paintHandle
      .getWidgetState()
      .getHandle()
      .setDirection(ijk);
    
    if (this.labelMaps.length) {
      this.labelMap = this.labelMaps[0];
      const labelData = this.labelMap.data; // TODO
      this.painter.setLabelMap(labelData);
    }

    // update paint filter
    this.painter.setBackgroundImage(this.image.data);
    // don't set to 0, since that's our empty label color from our pwf
    this.painter.setLabel(1);
    // set custom threshold
    // this.painter.setVoxelFunc((bgValue, idx) => bgValue < 145);

    this.setRadius(5);
    
    // TODO We hsould remove the callback defined here: this.image.imageMapper.onModified(update) when component is unmounted. In the meantime I check whether the labelmap is defined.    
    this.image.imageMapper.onModified(()=>this.updatePaintWidget());
    
    this.readyAll();
    this.initializeHandles();
    this.updateHandles(); 
    
    this.crossHairs = CrossHairs.newInstance({genericRenderWindow: this.scene.genericRenderWindow, container:this.scene.container.current});
    
    this.overlay = Overlay.newInstance({genericRenderWindow: this.scene.genericRenderWindow, container:this.scene.container.current});
      
    this.overlay.setText(this.upperLeftText, this.upperRightText, this.bottomLeftText);
    
    // Crazy stuff to get WEBGL context deleted correctly
    const axes = vtkAnnotatedCubeActor.newInstance();
    axes.setDefaultStyle({
      text: '+X',
      fontStyle: 'bold',
      fontFamily: 'Arial',
      fontColor: 'black',
      fontSizeScale: (res) => res / 2,
      faceColor: '#0000ff',
      faceRotation: 0,
      edgeThickness: 0.1,
      edgeColor: 'black',
      resolution: 400,
    });
    // axes.setXPlusFaceProperty({ text: '+X' });
    axes.setXMinusFaceProperty({
      text: '-X',
      faceColor: '#ffff00',
      faceRotation: 90,
      fontStyle: 'italic',
    });
    axes.setYPlusFaceProperty({
      text: '+Y',
      faceColor: '#00ff00',
      fontSizeScale: (res) => res / 4,
    });
    axes.setYMinusFaceProperty({
      text: '-Y',
      faceColor: '#00ffff',
      fontColor: 'white',
    });
    axes.setZPlusFaceProperty({
      text: '+Z',
      edgeColor: 'yellow',
    });
    axes.setZMinusFaceProperty({ text: '-Z', faceRotation: 45, edgeThickness: 0 });
    
    
    
    
    //TODO: check for active tool and restore them.
    
    
    if (!image.getTrueDirectionDiffers()) {
      this.reader = vtkSTLReader.newInstance();
      this.orientationMapper = vtkMapper.newInstance({ scalarVisibility: false });
      this.orientationActor = vtkActor.newInstance();
      
      this.orientationActor.setMapper(this.orientationMapper);
      this.orientationMapper.setInputConnection(this.reader.getOutputPort());
      
      this.reader.setUrl(`${human_standing}`, { binary: true }).then(() => {
          this.orientationWidget = vtkOrientationMarkerWidget.newInstance({
            actor: this.orientationActor,
            interactor: this.scene.renderWindow.getInteractor(),
          });        
          
          this.orientationWidget.setEnabled(true);
          this.orientationWidget.setViewportCorner(
            vtkOrientationMarkerWidget.Corners.BOTTOM_RIGHT
          );
          this.orientationWidget.setViewportSize(0.2);
          this.orientationWidget.setMinPixelSize(100);
          this.orientationWidget.setMaxPixelSize(300);
      });
    }
        
    this.scene.renderWindow.render();
    
    const viewsApi = ViewsApi.getInstance()
	  viewsApi.registerApi(this.api);
	  
	  this.setActiveLabelMap(activeLabelMap);
	  this.startWidget(viewsApi.getActiveWidget());
	  
	  this.updateHandles();
	  
	  if (!this.image.imageProperties.getUserPosition()) {
	    const bounds = this.image.data.getBounds();
      const spacing = this.image.data.getSpacing();
      const center = [(bounds[0]+bounds[1])/2,
        (bounds[2]+bounds[3])/2,
        (bounds[4]+bounds[5])/2];

      const newPos = [bounds[0]+Math.round((center[0]-bounds[0])/spacing[0])*spacing[0],
        bounds[2]+Math.round((center[1]-bounds[2])/spacing[1])*spacing[1],
        bounds[4]+Math.round((center[2]-bounds[4])/spacing[2])*spacing[2]];
      
      // TODO only do this for secondary and primary image, not for all
      this.props.imageDataSet.getImages().forEach(image => image.getImageProperties().setUserPosition(newPos));
    }
	  
	  this.setSlice();
	  
    this.image.imageProperties.addListener(this.imagePropertiesListener);
    
    
    this.scene.container.current.style.cursor = "default";
    
    this.apiEnabled = true;
  }
  
  componentWillUnmount() {
    this.apiEnabled = false;
    
    console.log('Unmounting 2D view');
    
    this.endAll();
    
    
    // Listeners
    if (this.resizeObserver) {
      this.resizeObserver.disconnect();
      this.resizeObserver = null;
    }
    
    this.scene.iStyle.deleteCrossHairsListeners();
    
    let segs = [];
    if (this.state.segDataSet) {
      segs = this.state.segDataSet.getSegs();
    }
   
    segs.forEach((seg, index) => {
      this.listenerIds.forEach(id => {
        seg.removeListener(id);
      });
    });
    
    this.listenerIds = [];
    
    if(this.image.imageProperties)
      this.image.imageProperties.removeListener(this.imagePropertiesListener);
    
    
    // Overlays
    if (this.overlay) {
      this.overlay.delete();
      this.overlay = null;
    }
    
    if (this.svgContour.length) {
      this.svgContour.forEach(contour => contour.delete());
      this.svgContour = [];
    }
    
    if (this.crossHairs) {
      this.crossHairs.delete();
      this.crossHairs = null;
    }
    
    
    // vtk
    //if (this.image.imageMapper) {
      this.image.imageMapper.delete();
      this.image.actor.delete();
      
      this.image.imageMapper = null;
      this.image.actor = null;
    
    
    this.labelMaps.forEach(labelMap => {
      labelMap.imageMapper.delete();
      labelMap.actor.delete();
      labelMap.outline.delete();
    });
    
    this.labelMaps = [];
    

    if (this.secondaryImage.imageProperties) {
      this.secondaryImage.imageProperties.removeListener(this.secondaryPropertiesListener);
      this.secondaryImage.imageMapper.delete();
      this.secondaryImage.actor.delete();
      
      this.secondaryImage.imageMapper = null;
      this.secondaryImage.actor = null;
    }
    
    if(this.scene.genericRenderWindow) {      
      this.scene.genericRenderWindow.setContainer(null);
      this.scene.genericRenderWindow.getOpenGLRenderWindow().delete();
      this.scene.genericRenderWindow.delete();
      
      this.scene.openGLRenderWindow = null;
      this.scene.genericRenderWindow = null;
    }
    
    if(this.scene.renderer) {
      this.scene.renderer.delete();
      this.scene.renderer = null;
    }
    
    if(this.scene.renderWindow) {
      this.scene.renderWindow.getInteractor().delete();
      this.scene.renderWindow.delete();
      this.scene.renderWindow = null;
    }
    
    if(this.scene.widgetManager) {
      this.scene.widgetManager.delete();
      this.scene.widgetManager = null;
    }
    
    if (this.scene.iStyle) {
      this.scene.iStyle.delete();
      this.scene.iStyle = null; 
    }
    
    if (this.orientationMapper) {
      this.orientationMapper.delete();
      this.orientationActor.delete();
      this.reader.delete();
      this.orientationWidget.delete();
      
      this.orientationActor = null;
      this.orientationMapper = null;
      this.orientationWidget = null;
      this.reader = null;
    }
    
    if(this.widgets.paintWidget) {
      this.widgets.paintWidget.delete();
      this.painter.delete();
      this.scene.paintHandle.delete();
      
      this.widgets.paintWidget = null;
      this.painter = null;
      this.scene.paintHandle = null;
    }
	
	  
	  // API
    ViewsApi.getInstance().unregisterApi(this.api);
  }
  
  componentDidUpdate() {
    console.log('Updating component');    
    const sliceMode = 'IJKXYZ'.indexOf(this.sliceMode) % 3;
    
    if (this.activeSeriesInstanceUID==this.props.SeriesInstanceUID &&
      this.activeSecondarySeriesInstanceUID==this.props.secondarySeriesInstanceUID &&
      this.sliceMode == sliceMode)
      return;
      
    this.upperLeftText = [];
    this.upperRightText = [];
    this.bottomLeftText = [];
      
    this.componentWillUnmount();
    this.componentDidMount();
  }
  
  render() {
    const style = { width: '100%', height: '100%', position: 'relative' };
    
    return (
      <div style={style}>
        <div class="view2D" ref={this.scene.container} style={style}/>
      </div>
    );
  }
}

