
import React, { Component } from "react";
import ContourModification from './ContourModification.js';

import './styles.css'


export default class HistoryBrowser extends React.Component {
  constructor(props) {
    super(props);
    
    this.contourModification = ContourModification.getInstance();
    
    this.state = {
      history: this.contourModification.getHistory(),
    }
    
    this.endListener = () => {
      this.setState({
        history: this.contourModification.getHistory(),
      })
    }
    
    this.contourModification.onModicationEnd(this.endListener);
  }
  
  componentWillUnmount() {
    this.contourModification.removeModificationEndListener(this.endListener);
  }
  
  componentDidMount() {
  }
  
  componentDidUpdate() {
    
  }
  
  renderHistory() {
    if (!this.state.history.labels.length)
      return null;
    
    const historyList = this.state.history.labels.map((label, index) => {
        return (
          <tr>
            <td>{label}</td>
            <td>{this.state.history.descriptions[index]}</td>
            <td>{this.state.history.names[index]}</td>
          </tr>
        );
      });
      
    return historyList;
  }
  
  render() {
    const historyList = this.renderHistory();
    
    return (
      <div className="imagePreviews">
        <div className="scrollHistoryList">
          <h1>Contour History</h1>
          {historyList?
            (<table>
              <tr><th>Label</th><th>Description</th><th>Contour</th></tr>
              {historyList}
            </table>) : (<p>No modification recorded yet.</p>)}
        </div>
      </div>
    );
  }
}

