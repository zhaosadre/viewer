
export default class ContourModification {
  static instance = null;
  onModicationStartFunc = [];
  onModicationEndFunc = [];
  activeSeg = null;
  labelInput = null;
  descriptionInput = null;
  labelHistory = [];
  descriptionHistory = [];
  nameHistory = [];
  segs = null;
  descriptionInputValue = null;
  labelInputValue  = null;

  static getInstance(){
    if (!ContourModification.instance)
      ContourModification.instance = new ContourModification();
      
    return this.instance;
  }
  
  cancel() {
    this.activeSeg = null;
    this.descriptionInputValue = null;
    this.labelInputValue = null;
    this.onModicationEndFunc.forEach(func => func());
  }
  
  // setActiveSeg to null will save the modifications
  setActiveSeg(segNb){
    switch(segNb) {
      case null:
        if(!this.activeSeg)
          return;
          
        const description = this.descriptionInput.value.slice();
        const label = this.labelInput.value.slice();
        
        if (!label || !description)
          throw new Error("Please, add a label and a description to your change");
      
        this.labelHistory.push(label);
        this.descriptionHistory.push(description);
        this.nameHistory.push(this.segs.filter(seg=>seg.getNumber()==this.activeSeg)[0].getLabel());
        
        this.activeSeg = null;
        this.descriptionInputValue = null;
        this.labelInputValue = null;
        this.descriptionInput = null;
        this.labelInput = null;
    
        this.onModicationEndFunc.forEach(func => func());
        break;
      default:
        this.activeSeg = segNb,
        this.onModicationStartFunc.forEach(func => func());
    }
  }
  
  setSegDataSet(segDataSet) {
    this.segs = segDataSet.getSegs();
  }
  
  setDescriptionInput(labelInput, descriptionInput) {
    this.descriptionInput = descriptionInput;
    this.labelInput = labelInput;
  }
  
  saveInputForLater() {
    if(this.descriptionInput) {
      this.descriptionInputValue = this.descriptionInput.value.slice();
      this.labelInputValue = this.labelInput.value.slice();
    }
  }
  
  getSavedInputValues() {
    return {labelInputValue: this.labelInputValue, descriptionInputValue: this.descriptionInputValue};
  }
  
  onModicationStart(func) {
    this.onModicationStartFunc.push(func);
  }
  
  onModicationEnd(func) {
    this.onModicationEndFunc.push(func);
  }
  
  removeModificationStartListener(listener) {
    const listenerIndex = this.onModicationStartFunc.indexOf(listener);
    
    if (listenerIndex>-1)
      this.onModicationStartFunc.splice(listenerIndex, 1)
  }
  
  removeModificationEndListener(listener) {
    const listenerIndex = this.onModicationEndFunc.indexOf(listener);
    
    if (listenerIndex>-1)
      this.onModicationEndFunc.splice(listenerIndex, 1)
  }
  
      
  getHistory() {
    return {
      labels: this.labelHistory,
      descriptions: this.descriptionHistory,
      names: this.nameHistory,
    }
  }
}

