
import React, { Component } from "react";
import {api} from "dicomweb-client";
import uniqBy from 'lodash/uniqBy';

import {SETTING_NAMES, MainSettings} from '../../utils/MainSettings.js';

import './styles.css'
import { Icon } from '../../../../../ui/src/elements/Icon';

import CustomClickMenu from "./CustomClickMenu.js";
import ContourModification from "./ContourModification.js";


//https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

//https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function rgbToHex(r, g, b) {
  r = Math.round(r); g = Math.round(g); b = Math.round(b);
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

//https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? [parseInt(result[1], 16),
    parseInt(result[2], 16),
    parseInt(result[3], 16)
  ] : null;
}

class SegmentItem extends React.Component {
  constructor(props) {
    super(props);
    
    let modifying = this.props.seg.getNumber()==this.props.activeSegmentNumber;
    
    this.state = {
      activeSegmentNumber: this.props.activeSegmentNumber,
      seg: this.props.seg,
      visible: this.props.seg.getVisible() || modifying,
    };
    
    this.colorId = "colorInput"+this.props.seg.getNumber();
    this.colorListener = (event) => this.colorEventListener(event);
  }
  
  toggleVisibility() {
    if (this.state.seg.getNumber() == this.props.activeSegmentNumber)
      return;
      
    this.state.seg.setVisible(!this.state.visible);
    this.setState({visible: !this.state.visible});
  }
  
  toggleModification() {
    switch (this.props.activeSegmentNumber) {
      case null:
        this.props.onModificationStart(this.state.seg.getNumber());
        break;
      case this.state.seg.getNumber():
        this.props.onModificationEnd(this.state.seg.getNumber());
        break;
      default:
        alert("Please save current contour before proceeding to a new contour edition.");
    }
  }
  
  componentDidMount() {
    const color = this.state.seg.getColor();   
    let colorValue = rgbToHex(color[0], color[1], color[2]);
    
    const input = document.getElementById(this.colorId);

    input.addEventListener('input', this.colorListener);
    
    input.value = colorValue;
    
    this.componentDidUpdate();
  }
  
  colorEventListener(event) {
    const input = document.getElementById(this.colorId);
    this.state.seg.setColor(hexToRgb(input.value));  
  }
  
  componentDidUpdate() {
    let modifying = this.props.seg.getNumber()==this.props.activeSegmentNumber;
    
    if (modifying && !this.state.visible) {
      this.props.seg.setVisible(true);
      this.setState({visible:true});
    }
  }
  
  handleNavigate() {
    const position = this.state.seg.getCenter();
    
    if (position)
      this.props.setUserPosition(position);
  }
  
  render() {
    let modifying = this.props.seg.getNumber()==this.props.activeSegmentNumber;
    
    const segmentNumber = this.state.seg.getNumber();
    
    const tdStyle = {
      width: "20px",
      height: "20px",
      border: "none",
      cursor: "pointer",
      border: "none",
      outline: "none",
      padding: 0,
    };
    
    const iconName = this.state.visible ? "eye" : "eye-closed";
    const secondIconName = modifying ? "save" : "edit";
    
    const itemContent = ["Navigate to segment", 'Cancel'];
    const onItemClick = [
      () => this.handleNavigate(),
      () => {},
    ];
    
    const targetId = 'segmentTr'+segmentNumber;
    
    return(
      <React.Fragment>
        <CustomClickMenu
          isEnabled = {() => true}
          targetId={targetId}
          itemContent={itemContent}
          onItemClick={onItemClick}
        />
        <tr className={modifying ? "segment" + " active" : "segment"}>
          <td><Icon name={iconName} onClick={() => this.toggleVisibility()} className="segmentVisibility"/></td>
          <td><Icon name={secondIconName} onClick={() => this.toggleModification()} className="segmentVisibility"/></td>
          <td><input id={this.colorId} style={tdStyle} type="color"/></td>
          <td id={targetId} style={{cursor: "pointer"}}>{this.state.seg.getLabel()}</td>
        </tr>
      </React.Fragment>
    );
  }
}

export default class ContourBrowser extends React.Component {
  constructor(props) {
    super(props);
    
    if (!this.props.segDataSet) {
      this.state = {segDataSet: null, segs: null};
      return;
    }
    
    this.state = {
      activeSegmentNumber: null,
      segDataSet: this.props.segDataSet,
      allVisible: false,
    };
    
    this.modificationEndListener = () => {
      const segs = this.props.segDataSet.getSegs();
      segs.forEach(seg => {seg.setActive(false);});
      this.setState({activeSegmentNumber:null});
    };
    
    this.contourModification = ContourModification.getInstance();
    this.contourModification.setSegDataSet(this.props.segDataSet);
    this.contourModification.onModicationEnd(this.modificationEndListener);
  }
  
  componentWillUnmount() {
    this.contourModification.removeModificationEndListener(this.modificationEndListener);
    
    const input = document.getElementById(this.colorId);
    if(input)
     input.removeEventListener('input', this.colorListener);
  }
  
  componentDidMount() {
    this.state.segDataSet.addListener(() => {this.onSegChange();});
  }
  
  onSegChange() {
    this.setState({segs: this.state.segDataSet.getSegs()});
  }
  
  onModificationStart(activeSegmentNumber) {
    const segs = this.state.segDataSet.getSegs();
  
    segs.forEach(seg => {
      seg.getNumber()==activeSegmentNumber? seg.setActive(true):seg.setActive(false);
    });
    
    this.contourModification.setActiveSeg(activeSegmentNumber);
    
    this.setState({activeSegmentNumber});
  }
  
  onModificationEnd(activeSegmentNumber) {
    try {
      this.contourModification.setActiveSeg(null);
    }
    catch (err) {
      alert(err.message);
      return;
    }
  
    const segs = this.state.segDataSet.getSegs();
    segs.forEach(seg => {seg.setActive(false);});
    this.state.segDataSet.sendData();

    
    this.setState({activeSegmentNumber:null});
  }
  
  toggleAll() {
    const segs = this.state.segDataSet.getSegs();
      
    segs.forEach(seg => {
      if(seg.getVisible() == this.state.allVisible)
        seg.setVisible(!this.state.allVisible);
    });
    
    this.setState({allVisible: !this.state.allVisible});
  }
  
  render() {
    if (!this.state.segDataSet)
      return (<div>No contour available</div>)
    
    const segs = this.state.segDataSet.getSegs();
    const SEGItems = segs.map(seg=> {
        return(
          <SegmentItem
            onModificationStart={(activeSegmentNumber) => this.onModificationStart(activeSegmentNumber)}
            onModificationEnd={(activeSegmentNumber) => this.onModificationEnd(activeSegmentNumber)}
            seg={seg}
            activeSegmentNumber={this.state.activeSegmentNumber}
            setUserPosition = {(userPosition) => this.props.setUserPosition(userPosition)}
          />
        );
      });

    return (
      <div class="SEGTable">
        <table>
          <tr className="segment">
            <td><Icon name={this.state.allVisible? "eye-closed" : "eye"} onClick={() => this.toggleAll()} className="segmentVisibility"/></td>
            <td/>
            <td/>
            <td>Show/Hide all</td>
          </tr>
          {SEGItems}
        </table>
      </div>
    );
  }
}

