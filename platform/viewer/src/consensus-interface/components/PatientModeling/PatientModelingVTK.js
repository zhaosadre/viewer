
import React, { Component } from "react";
import uniqBy from 'lodash/uniqBy';

import ImagePreview from './ImagePreview.js';
import VTKViews from './VTKViews.js';
import ImagePreviews from './ImagePreviews.js';
import ContourBrowser from './ContourBrowser.js';
import SegSelection from './SegSelection.js';
import ToolbarSelection from './ToolbarSelection.js';
import DataSet from './../../data/DataSet.js';
import ViewsApi from './Services/ViewsApi'
import { Icon } from '../../../../../ui/src/elements/Icon';
import HistoryBrowser from "./HistoryBrowser.js";

import './styles.css';


export default class PatientModeling extends React.Component {
  constructor(props) {
    super(props);
    
    this.dataSet = DataSet.getInstance();
    
    this.dataListener = (event) => this.dataSetListener(event);
    this.dataSet.addListener(this.dataListener);
    
    this.imageApi = null;
    
    this.promiseSeriesInstanceUID = null;
    this.promiseSecondarySeriesInstanceUID = null;
    this.promiseSEGSeriesUID = null;
    this.promiseSEGSOPUID = null;
    
    this.state = {
      activeSeriesInstanceUID: null,
      activeSecondarySeriesInstanceUID: null,
      activeSEGSeriesUID: null,
      activeSEGSOPUID: null,
      imageDataSet: this.dataSet.getImageDataSet(),
      segs: null,
      StudyInstanceUID:this.props.StudyInstanceUID,
      study: this.dataSet.getStudy(),
      previewsOn: true,
      historyOn: false,
      layout: 3,
    };
  }
  
  dataSetListener(event) {
    if (event.type=="created") { // In case not already loaded when calling constructor
      switch(event.target) {
        case this.props.StudyInstanceUID:
          this.setState({study: this.dataSet.getStudy()});
          break;
        case this.promiseSeriesInstanceUID:
          break;
        case this.promiseSecondarySeriesInstanceUID:
          break;
        case this.promiseSEGSOPUID:
          this.setState({
            segs: this.dataSet.getSegDataSet().getSegs(this.promiseSEGSOPUID),
            activeSEGSeriesUID: this.promiseSEGSeriesUID,
            activeSEGSOPUID: this.promiseSEGSOPUID,
          });
          break;
      }
    }
  }
    
  componentWillUnmount() {
    this.dataSet.removeListener(this.dataListener);
  }
  
  getSeriesInstanceUIDs() {
    if (!this.state.study)
      return null;
      
    return uniqBy(this.state.study, 'SeriesInstanceUID').filter(study => (study.modalities=="CT" || study.modalities=="MR" || study.modalities=="PT")).map(study => study.SeriesInstanceUID);
  }
  
  handleActiveSeriesInstanceUIDSelected(activeSeriesInstanceUID) {
    const imageDataSet = this.dataSet.getImageDataSet();
    if (imageDataSet && imageDataSet.getImage(activeSeriesInstanceUID)) {
      this.setState({imageDataSet, activeSeriesInstanceUID});
      
      if (this.imageApi)
        ViewsApi.getInstance().unregisterApi(this.imageApi);
        
      this.imageApi = imageDataSet.getImage(activeSeriesInstanceUID).getImageProperties().getApi();
        
      ViewsApi.getInstance().registerApi(this.imageApi);
    }
  }
  
  handleActiveSecondarySeriesInstanceUIDSelected(activeSecondarySeriesInstanceUID) {
    let layout = this.state.layout;
    
    if(!activeSecondarySeriesInstanceUID)
      layout = 3;
      
    this.setState({layout, activeSecondarySeriesInstanceUID});
  }
  
  handlesSelectedSEG(activeSEGSeriesUID, activeSEGSOPUID) {
    this.promiseSEGSOPUID = activeSEGSOPUID;
      
    const segs = this.dataSet.getSegDataSet().getSegs(activeSEGSOPUID);
    if (segs)
      this.setState({segs, activeSEGSeriesUID, activeSEGSOPUID});
  }
  
  setUserPosition(userPosition) {
    if (this.state.imageDataSet)
      this.state.imageDataSet.getImages().forEach(image => image.getImageProperties().setUserPosition(userPosition));
  }
  
  setLayout(layout) {
    if (layout == this.state.layout)
      return;
    if (layout == 2 && !this.state.activeSecondarySeriesInstanceUID) {
      alert("You must select a secondary image to use this layout.");
      return;
    }
    this.setState({layout});
  }
  
  renderPreviews() {
    if (this.state.study)
      return (
        <ImagePreviews
          clickEnabled={true}
          activeSeriesInstanceUID = {this.state.activeSeriesInstanceUID}
          activeSecondarySeriesInstanceUID = {this.state.activeSecondarySeriesInstanceUID}
          SeriesInstanceUIDs={this.getSeriesInstanceUIDs()}
          onPrimaryItemClick={(activeSeriesInstanceUID)=>{this.handleActiveSeriesInstanceUIDSelected(activeSeriesInstanceUID);}}
          onSecondaryItemClick={(activeSeriesInstanceUID)=>{this.handleActiveSecondarySeriesInstanceUIDSelected(activeSeriesInstanceUID);}}
      />);
    else
      return (<div>No study selected</div>);
  }
  
  renderImageView() {
    if (this.state.activeSeriesInstanceUID) {
      if (this.state.imageDataSet && this.state.imageDataSet.getImage(this.state.activeSeriesInstanceUID)) {
        return (
          <VTKViews
            SeriesInstanceUID={this.state.activeSeriesInstanceUID}
            secondarySeriesInstanceUID={this.state.activeSecondarySeriesInstanceUID}
            segDataSet={this.state.segs}
            imageDataSet={this.state.imageDataSet}
            layout={this.state.layout}
          />);
      }
      else {
        return (<div>Loading...</div>);
      }
    }
    else
      return (<div>No series selected</div>);
  }
  
  storeApis(api, apis){
	  this.api = api;
	  this.vtkViewsApis = apis;
  }
  
  componentWillUnmount() {
    if (this.imageApi)
      ViewsApi.getInstance().unregisterApi(this.imageApi);
  }
  
  render() {
    let previews = null;
    
    if(this.state.previewsOn && !this.state.historyOn)
      previews = this.renderPreviews();
      
    if(this.state.previewsOn && this.state.historyOn)
      previews = <HistoryBrowser/>
      
    const imageView = this.renderImageView();
    
    let segPanel = <div></div>;
        
    if (this.state.study) {
      let cBrowser = null;
      
      if (!this.state.segs) {
        segPanel =
          <div className="SegPanelContent">
            Loading...
          </div>;
      }
      else {        
        cBrowser = <ContourBrowser
          segDataSet={this.state.segs}
          setUserPosition={(userPosition) => this.setUserPosition(userPosition)}
        /> ;
      }
    
      segPanel =
        <div className="SegPanelContent">
          <SegSelection
            study={this.state.study}
            onSelectedSEG={(activeSEGSeriesUID, activeSEGSOPUID) => {this.handlesSelectedSEG(activeSEGSeriesUID, activeSEGSOPUID)}}/>
            {cBrowser}
        </div>;
    }
    else if (this.state.StudyInstanceUID) {
      segPanel = <div>Loading...</div>
    }
    

    let caretStyle = {
      width: "100%",
      height: "20px",
      verticalAlign: "center",
      textAlign: "center",
    }
    let iconName = "caret-up";    
    if (this.state.previewsOn)
      iconName = "caret-down";
    
    let caretIcon = <Icon style={caretStyle} name={iconName} onClick={() => this.setState({previewsOn: !this.state.previewsOn})}/>;;
    let stepIcon = null;
    let stepStyle = {
      width: "calc(100% - 20px)",
      height: "calc(50px - 10px)",
      verticalAlign: "center",
      textAlign: "center",
      transform: "rotate(-90deg)",
      padding: "5px 0 5px 0",
    }
    if (this.state.previewsOn)
      stepIcon = 
          <table style={stepStyle} onClick={()=>this.setState({historyOn: !this.state.historyOn})}>
            <tr>
              <Icon name={this.state.historyOn? "caret-down":"caret-up"}/>
            </tr>
            <tr>
              History
            </tr>
          </table>
    
    return (
      <div className="PatientModelingVTK">
        <div className="PatientModelingVTKToolbar">
          <ToolbarSelection
            layout={this.state.layout}
            setLayout={(layout) => this.setLayout(layout)}
            SeriesInstanceUID={this.state.activeSeriesInstanceUID}
            secondarySeriesInstanceUID={this.state.activeSecondarySeriesInstanceUID}
            imageDataSet={this.state.imageDataSet}
          />
        </div>
        <div className="PatientModelingVTKMain">
          <div className="SegPanel">
            {segPanel}
          </div>
          <div className="ImageViews">
            <div className={this.state.previewsOn? "ImageView":"ImageView extended"}>
              {imageView}
            </div>
            <div className={this.state.previewsOn? "ImagePreviewsBottom extended":"ImagePreviewsBottom"}>
              <div className="previewsList">
                {this.state.previewsOn && previews}
              </div>
              <div className="previewsIcon">
                {caretIcon}
                {stepIcon}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

