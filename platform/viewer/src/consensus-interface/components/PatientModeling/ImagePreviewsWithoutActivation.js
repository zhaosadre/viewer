
import ImagePreviews from './ImagePreviews.js';

import './styles.css'


export default class ImagePreviewsWithoutActivation extends ImagePreviews {
  constructor(props) {
    super(props);
    
    this.state = {
      SeriesInstanceUIDs: this.props.SeriesInstanceUIDs,
    };
  }
  
  handlePrimaryClick(activeSeriesInstanceUID) {
    if (this.props.onItemClick)
      this.props.onItemClick(activeSeriesInstanceUID);
  }
}

