import macro from 'vtk.js/Sources/macro';
import * as vtkMath from 'vtk.js/Sources/Common/Core/Math';
import vtkMatrixBuilder from 'vtk.js/Sources/Common/Core/MatrixBuilder';
import vtkInteractorStyleManipulator from 'vtk.js/Sources/Interaction/Style/InteractorStyleManipulator';
import vtkMouseCameraTrackballRotateManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballRotateManipulator';
import vtkMouseCameraTrackballPanManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballPanManipulator';
import vtkMouseCameraTrackballZoomManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseCameraTrackballZoomManipulator';
import vtkMouseRangeManipulator from 'vtk.js/Sources/Interaction/Manipulators/MouseRangeManipulator';
import vtkCoordinate from 'vtk.js/Sources/Rendering/Core/Coordinate';
import vtkInteractorStyleTrackballCamera from 'vtk.js/Sources/Interaction/Style/InteractorStyleTrackballCamera';

// ----------------------------------------------------------------------------
// Global methods
// ----------------------------------------------------------------------------

function boundsToCorners(bounds) {
  return [
    [bounds[0], bounds[2], bounds[4]],
    [bounds[0], bounds[2], bounds[5]],
    [bounds[0], bounds[3], bounds[4]],
    [bounds[0], bounds[3], bounds[5]],
    [bounds[1], bounds[2], bounds[4]],
    [bounds[1], bounds[2], bounds[5]],
    [bounds[1], bounds[3], bounds[4]],
    [bounds[1], bounds[3], bounds[5]],
  ];
}

// ----------------------------------------------------------------------------

function clamp(value, min, max) {
  if (value < min) {
    return min;
  }
  if (value > max) {
    return max;
  }
  return value;
}

// ----------------------------------------------------------------------------
// vtkInteractorStyleMPRSlice methods
// ----------------------------------------------------------------------------

function customInteractorStyle_slice(publicAPI, model) {
  // Set our className
  model.classHierarchy.push('customInteractorStyle_slice');

  model.trackballManipulator = vtkMouseCameraTrackballRotateManipulator.newInstance(
    {
      button: 1,
    }
  );
  model.panManipulator = vtkMouseCameraTrackballPanManipulator.newInstance({
    button: 1,
  });
  model.zoomManipulator = vtkMouseCameraTrackballZoomManipulator.newInstance({
    button: 3,
  });
   model.scrollManipulator = vtkMouseRangeManipulator.newInstance({
    scrollEnabled: true,
    dragEnabled: false,
  });

  // cache for sliceRange
  const cache = {
    sliceNormal: [0, 0, 0],
    sliceRange: [0, 0],
  };

  let cameraSub = null;

  function updateScrollManipulator() {
    const range = publicAPI.getSliceRange();
    model.scrollManipulator.removeScrollListener();
 
    let scrollIncrement = 1;
    if (model.hasOwnProperty('imSpacing'))
      scrollIncrement = model.imSpacing[model.sliceMode];
      
    model.scrollManipulator.setScrollListener(
      range[0],
      range[1],
      scrollIncrement,
      publicAPI.getSlice,
      publicAPI.setSlice
    );
  }

  function setManipulators(panButton, zoomButton) {
    publicAPI.removeAllMouseManipulators();
    
    model.panManipulator = vtkMouseCameraTrackballPanManipulator.newInstance({
      button: panButton,
    });
    model.zoomManipulator = vtkMouseCameraTrackballZoomManipulator.newInstance({
      button: zoomButton,
    });
  
    //publicAPI.addMouseManipulator(model.scrollManipulator);
    //updateScrollManipulator();
    publicAPI.addMouseManipulator(model.panManipulator);
    publicAPI.addMouseManipulator(model.zoomManipulator);
  }

  publicAPI.superDelete = publicAPI.delete;
  publicAPI.delete = () => {
    model.onSetSlice = null;
    model.crossHairsListeners = [];
    model.regrabFocus = null;
    model.releaseFocus = null;
    
    publicAPI.removeAllMouseManipulators();
    
    publicAPI.superDelete()
  }
  
  const superSetInteractor = publicAPI.setInteractor;
  publicAPI.setInteractor = (interactor) => {
    superSetInteractor(interactor);

    if (cameraSub) {
      cameraSub.unsubscribe();
      cameraSub = null;
    }

    if (interactor) {
      const renderer = interactor.getCurrentRenderer();
      const camera = renderer.getActiveCamera();

      cameraSub = camera.onModified(() => {
        publicAPI.modified();
      });
    }
  };
  
  publicAPI.setSliceMode = (sliceMode) => {
    model.sliceMode = sliceMode;
  };
  
  /* New methods */
  publicAPI.addCrossHairsListener = (listener) => {
    if (!model.hasOwnProperty("crossHairsListeners"))
      model.crossHairsListeners = Array.isArray(listener)? listener : [listener];
    else {
      if (Array.isArray(listener))
        model.crossHairsListeners = model.crossHairsListeners.concat(listener);
      else
        model.crossHairsListeners.push(listener);
    }
  };
  
  publicAPI.deleteCrossHairsListeners = () => {
    model.crossHairsListeners = [];
  };
  
  publicAPI.setRegrabFocus = (regrabFocus) => {
    model.regrabFocus = () => regrabFocus();
  };
  
  publicAPI.setReleaseFocus = (releaseFocus) => {
    model.releaseFocus = () => releaseFocus();
  };
  
  const handleCrossHairs = (callData) => {
    model.activeSliceMode = model.sliceMode;
    
    const pos = callData.position;
    let newPos = [pos.y, pos.x];
    
    const renderer = model.interactor.getCurrentRenderer();
    const dPos = vtkCoordinate.newInstance();
    dPos.setCoordinateSystemToDisplay();
    
    const camera = renderer.getActiveCamera();
    const focalPoint = camera.getFocalPoint();
    
    dPos.setValue(pos.x, pos.y, 0);
    let worldPos = dPos.getComputedWorldValue(renderer);
    
    worldPos[model.sliceMode] = focalPoint[model.sliceMode];
    
    model.crossHairsListeners.forEach(listener => {listener(worldPos, [0, 1, 2]);});
  };
  
  publicAPI.superHandleLeftButtonPress = publicAPI.handleLeftButtonPress;
  publicAPI.handleLeftButtonPress = (callData) => {
    if (model.hasOwnProperty("activeTool")) {
      model.leftButtonPress = true;
      
      if (model.activeTool=="crosshairs") {
        handleCrossHairs(callData);

        //publicAPI.invokeInteractionEvent({ type: 'InteractionEvent' });
        return;
      }
      
      if (model.activeTool=="wwl") {
        const pos = callData.position;
        
        model.windowLevelStartPosition[0] = pos.x;
        model.windowLevelStartPosition[1] = pos.y;
        
        model.windowLevelInitial = model.getWWLFunc().slice()
        
        return;
      }
    }

    //else
    publicAPI.superHandleLeftButtonPress(callData);
  };
  
  publicAPI.superHandleLeftButtonRelease = publicAPI.handleLeftButtonRelease;
  publicAPI.handleLeftButtonRelease = () => {
    model.leftButtonPress = false;
    publicAPI.superHandleLeftButtonRelease();
    //model.crossHairsListeners.forEach(listener => {listener([], []);});
  };
  
  
  publicAPI.superHandleMiddleButtonPress = publicAPI.handleMiddleButtonPress;
  publicAPI.handleMiddleButtonPress = (callData) => {
    model.middleButtonPress = true;
    publicAPI.superHandleMiddleButtonPress(callData);
    model.releaseFocus();
  };
  
  publicAPI.superHandleMiddleButtonRelease = publicAPI.handleMiddleButtonRelease;
  publicAPI.handleMiddleButtonRelease = () => {
    model.middleButtonPress = false;
    model.regrabFocus();
    publicAPI.superHandleMiddleButtonRelease();
    //model.crossHairsListeners.forEach(listener => {listener([], []);});
  };
  
  
  publicAPI.superHandleRightButtonPress = publicAPI.handleRightButtonPress;
  publicAPI.handleRightButtonPress = (callData) => {
    model.rightButtonPress = true;
    publicAPI.superHandleRightButtonPress(callData);
    model.releaseFocus();
  };
  
  publicAPI.superHandleRightButtonRelease = publicAPI.handleRightButtonRelease;
  publicAPI.handleRightButtonRelease = () => {
    model.rightButtonPress = false;
    model.regrabFocus();
    publicAPI.superHandleRightButtonRelease();
    //model.crossHairsListeners.forEach(listener => {listener([], []);});
  };
  
  publicAPI.setActiveTool = (tool) => {
    publicAPI.removeAllMouseManipulators();
    
    model.leftButtonPress = false;
    model.middleButtonPress = false;
    model.rightButtonPress = false;
    model.activeTool = tool;
    
    console.log('Active tool is: ' + tool);
    
    switch(tool) {
      case "wwl":
        setManipulators(2, 3); // default manipulators
        model.windowLevelStartPosition = [0, 0];
        break;
      case "crosshairs":
        setManipulators(2, 3); // default manipulators
        //publicAPI.addMouseManipulator(model.scrollManipulator);
        //updateScrollManipulator();
        break;
      case "noLeft":
        setManipulators(2, 3); // default manipulators
        //publicAPI.addMouseManipulator(model.scrollManipulator);
        //updateScrollManipulator();
        break;
      case "pan":
        setManipulators(1, 3); // default manipulators
        break;
      case "zoom":
        setManipulators(2, 1); // default manipulators
        break;
      default:
        setManipulators(2, 3); // default manipulators
    }
  };
  
  publicAPI.onSetSlice = (func) => {
    model.onSetSlice = () => {func();};
  };
  
  publicAPI.setWWLFunc = (WWLFunc) => {
    model.setWWLFunc = WWLFunc;
  };
  
  publicAPI.getWWLFunc = (WWLFunc) => {
    return model.getWWLFunc = WWLFunc;
  };
  
  publicAPI.windowLevel = (renderer, position) => {
    model.windowLevelCurrentPosition = [position.x, position.y];

    const rwi = model.interactor;

      const size = rwi.getView().getViewportSize(renderer);

      const mWindow = model.windowLevelInitial[0];
      let level = model.windowLevelInitial[1];

      // Compute normalized delta
      let dx =
        ((model.windowLevelCurrentPosition[0] -
          model.windowLevelStartPosition[0]) *
          4.0) /
        size[0];
      let dy =
        ((model.windowLevelStartPosition[1] -
          model.windowLevelCurrentPosition[1]) *
          4.0) /
        size[1];

      // Scale by current values
      if (Math.abs(mWindow) > 0.01) {
        dx *= mWindow;
      } else {
        dx *= mWindow < 0 ? -0.01 : 0.01;
      }
      /*
      if (Math.abs(level) > 0.01) {
        dy *= level;
      } else {
        dy *= level < 0 ? -0.01 : 0.01;
      }*/

      // Abs so that direction does not flip
      if (mWindow < 0.0) {
        dx *= -1;
      }
      /*if (level < 0.0) {
        dy *= -1;
      }*/

      // Compute new mWindow level
      let newWindow = dx + mWindow;
      
      //const newLevel = level - dy;
      const newLevel = level + 100*dy;

      if (newWindow < 0.01) {
        newWindow = 0.01;
      }

      //model.windowLevelCurrentPosition[0] = newWindow;
      //model.windowLevelCurrentPosition[1] = newLevel;
      
      model.setWWLFunc(newWindow, newLevel);
  };

  publicAPI.superHandleMouseMove = publicAPI.handleMouseMove;
  
  /* */
  publicAPI.handleMouseMove = (callData) => {
    if (model.hasOwnProperty("activeTool")) {
      if (model.activeTool=="crosshairs" && model.leftButtonPress) {
        handleCrossHairs(callData);
        return;
      }
      
      if(model.activeTool=="wwl" && model.leftButtonPress) {
        const pos = callData.position;
        const renderer = model.interactor.getCurrentRenderer();
        
        publicAPI.windowLevel(renderer, pos);
        publicAPI.invokeInteractionEvent({ type: 'InteractionEvent' });
        
        return;
      }
      
      if ((model.activeTool=="zoom" && (model.leftButtonPress || model.rightButtonPress))
        || (model.activeTool=="pan" && (model.leftButtonPress || model.rightButtonPress))
        || model.middleButtonPress
        || (model.activeTool=="noLeft" && model.rightButtonPress)
        || (!model.activeTool && model.rightButtonPress)
        || ((model.activeTool=="crosshairs" || model.activeTool=="wwl") && model.rightButtonPress)){
        const renderer = model.interactor.getCurrentRenderer();
        const camera = renderer.getActiveCamera();
        
        const focalPoint = camera.getFocalPoint();
        const pos = focalPoint[model.sliceMode];
        
        if (model.leftButtonPress || model.rightButtonPress)
          model.crossHairsListeners.forEach(listener => {listener([pos], [model.sliceMode]);}); //TODO Remove if too slow
        
        publicAPI.superHandleMouseMove(callData);  
        return;
      }
      
      //else
      
      publicAPI.superHandleMouseMove(callData);
      
      /*
      const renderer = model.interactor.getCurrentRenderer();
      const camera = renderer.getActiveCamera();
      const dist = camera.getDistance();
      camera.setClippingRange(dist, dist + 0.1);*/
    }
    
    /*if(model.rightButtonPress) {
      const position = callData.position;
      const renderer = model.interactor.getCurrentRenderer();
      
      const dy = model.previousPosition.y - position.y;
      const k = dy * model.zoomScale;
      vtkInteractorStyleManipulator.dollyToPosition(
        1.0 - k,
        model.zoomPosition,
        renderer,
        model.interactor
      );

      if (model.interactor.getLightFollowCamera()) {
        renderer.updateLightsGeometryToFollowCamera();
      }

      model.previousPosition = position;
      
      model.interactor.getCurrentRenderer().getRenderWindow().render();
    }*/
  };

  //const superSetVolumeMapper = publicAPI.setVolumeMapper;
  publicAPI.setVolumeMapper = (mapper) => {
    model.imSpacing = mapper.getInputData().getSpacing();
    
    /*
    if (superSetVolumeMapper(mapper)) {
      const renderer = model.interactor.getCurrentRenderer();
      const camera = renderer.getActiveCamera();

      if (mapper) {
        model.imSpacing = mapper.getInputData().getSpacing();
        // prevent zoom manipulator from messing with our focal point
        camera.setFreezeFocalPoint(true);
        publicAPI.setSliceNormal(...publicAPI.getSliceNormal());
      } else {
        camera.setFreezeFocalPoint(false);
      }
    }*/
  };

  publicAPI.getSlice = () => {
    const renderer = model.interactor.getCurrentRenderer();
    const camera = renderer.getActiveCamera();
    const sliceNormal = publicAPI.getSliceNormal();

    // Get rotation matrix from normal to +X (since bounds is aligned to XYZ)
    const transform = vtkMatrixBuilder
      .buildFromDegree()
      .identity()
      .rotateFromDirections(sliceNormal, [1, 0, 0]);

    const fp = camera.getFocalPoint();
    transform.apply(fp);
    return fp[0];
  };

  publicAPI.setSlice = (slice) => {
    const renderer = model.interactor.getCurrentRenderer();
    const camera = renderer.getActiveCamera();

    if (model.volumeMapper) {
      const range = publicAPI.getSliceRange();
      const bounds = model.volumeMapper.getBounds();

      let clampedSlice = clamp(slice, ...range);
      clampedSlice = Math.round(clampedSlice/model.imSpacing[model.sliceMode])*model.imSpacing[model.sliceMode];
      
      const center = [
        (bounds[0] + bounds[1]) / 2.0,
        (bounds[2] + bounds[3]) / 2.0,
        (bounds[4] + bounds[5]) / 2.0,
      ];

      const distance = camera.getDistance();
      const dop = camera.getDirectionOfProjection();
      vtkMath.normalize(dop);

      const midPoint = (range[1] + range[0]) / 2.0;
      const zeroPoint = [
        center[0] - dop[0] * midPoint,
        center[1] - dop[1] * midPoint,
        center[2] - dop[2] * midPoint,
      ];
      
      const slicePoint = [
        zeroPoint[0] + dop[0] * clampedSlice,
        zeroPoint[1] + dop[1] * clampedSlice,
        zeroPoint[2] + dop[2] * clampedSlice,
      ];

      const newPos = [
        slicePoint[0] - dop[0] * distance,
        slicePoint[1] - dop[1] * distance,
        slicePoint[2] - dop[2] * distance,
      ];
      
      camera.setPosition(...newPos);
      camera.setFocalPoint(...slicePoint);
      
      model.interactor.getCurrentRenderer().getRenderWindow().render();
      
      if (model.hasOwnProperty("onSetSlice"))
        model.onSetSlice();
    }
  };

  publicAPI.getSliceRange = () => {
    if (model.volumeMapper) {
      const sliceNormal = publicAPI.getSliceNormal();

      if (
        sliceNormal[0] === cache.sliceNormal[0] &&
        sliceNormal[1] === cache.sliceNormal[1] &&
        sliceNormal[2] === cache.sliceNormal[2]
      ) {
        return cache.sliceRange;
      }

      const bounds = model.volumeMapper.getBounds();
      const points = boundsToCorners(bounds);

      // Get rotation matrix from normal to +X (since bounds is aligned to XYZ)
      const transform = vtkMatrixBuilder
        .buildFromDegree()
        .identity()
        .rotateFromDirections(sliceNormal, [1, 0, 0]);

      points.forEach((pt) => transform.apply(pt));

      // range is now maximum X distance
      let minX = Infinity;
      let maxX = -Infinity;
      for (let i = 0; i < 8; i++) {
        const x = points[i][0];
        if (x > maxX) {
          maxX = x;
        }
        if (x < minX) {
          minX = x;
        }
      }

      cache.sliceNormal = sliceNormal;
      cache.sliceRange = [minX, maxX];
      return cache.sliceRange;
    }
    return [0, 0];
  };

  // Slice normal is just camera DOP
  publicAPI.getSliceNormal = () => {
    if (model.volumeMapper) {
      const renderer = model.interactor.getCurrentRenderer();
      const camera = renderer.getActiveCamera();
      return camera.getDirectionOfProjection();
    }
    return [0, 0, 0];
  };

  // in world space
  publicAPI.setSliceNormal = (...normal) => {
    const renderer = model.interactor.getCurrentRenderer();
    const camera = renderer.getActiveCamera();

    vtkMath.normalize(normal);

    if (model.volumeMapper) {
      const bounds = model.volumeMapper.getBounds();

      // diagonal will be used as "width" of camera scene
      const diagonal = Math.sqrt(
        vtkMath.distance2BetweenPoints(
          [bounds[0], bounds[2], bounds[4]],
          [bounds[1], bounds[3], bounds[5]]
        )
      );

      // center will be used as initial focal point
      const center = [
        (bounds[0] + bounds[1]) / 2.0,
        (bounds[2] + bounds[3]) / 2.0,
        (bounds[4] + bounds[5]) / 2.0,
      ];

      const angle = 90;
      // distance from camera to focal point
      const dist = diagonal / (2 * Math.tan((angle / 360) * Math.PI));

      const cameraPos = [
        center[0] - normal[0] * dist,
        center[1] - normal[1] * dist,
        center[2] - normal[2] * dist,
      ];

      // set viewUp based on DOP rotation
      const oldDop = camera.getDirectionOfProjection();
      const transform = vtkMatrixBuilder
        .buildFromDegree()
        .identity()
        .rotateFromDirections(oldDop, normal);

      const viewUp = [0, 1, 0];
      transform.apply(viewUp);

      camera.setPosition(...cameraPos);
      camera.setDistance(dist);
      // should be set after pos and distance
      camera.setDirectionOfProjection(...normal);
      //camera.setViewUp(...viewUp);
      camera.setViewAngle(angle);
      camera.setClippingRange(dist, dist + 0.1);

      publicAPI.setCenterOfRotation(center);
    }
  };
  
  
  publicAPI.handleStartMouseWheel = (callData) => {
    //publicAPI.startSlice();
    publicAPI.handleMouseWheel(callData);
  };

  //--------------------------------------------------------------------------
  publicAPI.handleEndMouseWheel = (callData) => {    
    publicAPI.endSlice();
    /*
    const renderer = model.interactor.getCurrentRenderer();
    const camera = renderer.getActiveCamera();
    
    const focalPoint = camera.getFocalPoint();
    const pos = focalPoint[model.sliceMode];
    
    model.crossHairsListeners.forEach(listener => {listener([pos], [model.sliceMode]);});*/
  };
  
  publicAPI.handleMouseWheel = (callData) => {
    const camera = callData.pokedRenderer.getActiveCamera();

    let distance = camera.getDistance();
    distance += callData.spinY*model.imSpacing[model.sliceMode];

    // clamp the distance to the clipping range
    const range = camera.getClippingRange();
    if (distance < range[0]) {
      distance = range[0];
    }
    if (distance > range[1]) {
      distance = range[1];
    }
    camera.setDistance(distance);
    
    const focalPoint = camera.getFocalPoint();
    const pos = focalPoint[model.sliceMode];
    
    model.crossHairsListeners.forEach(listener => {listener([pos], [model.sliceMode]);}); //TODO: Remove if too slow
  }; 
  
  publicAPI.slice = (renderer, position) => {
    const rwi = model.interactor;

    const dy = position.y - model.lastSlicePosition;

    const camera = renderer.getActiveCamera();
    const range = camera.getClippingRange();
    let distance = camera.getDistance();

    // scale the interaction by the height of the viewport
    let viewportHeight = 0.0;
    if (camera.getParallelProjection()) {
      viewportHeight = camera.getParallelScale();
    } else {
      const angle = vtkMath.radiansFromDegrees(camera.getViewAngle());
      viewportHeight = 2.0 * distance * Math.tan(0.5 * angle);
    }

    const size = rwi.getView().getViewportSize(renderer);
    const delta = (dy * viewportHeight) / size[1];
    distance += delta;

    // clamp the distance to the clipping range
    if (distance < range[0]) {
      distance = range[0] + viewportHeight * 1e-3;
    }
    if (distance > range[1]) {
      distance = range[1] - viewportHeight * 1e-3;
    }
    camera.setDistance(distance);

    model.lastSlicePosition = position.y;
  };
  
  //----------------------------------------------------------------------------
  // This is a way of dealing with images as if they were layers.
  // It looks through the renderer's list of props and sets the
  // interactor ivars from the Nth image that it finds.  You can
  // also use negative numbers, i.e. -1 will return the last image,
  // -2 will return the second-to-last image, etc.
  publicAPI.setCurrentImageNumber = (i) => {
    if (i === null) {
      return;
    }

    const renderer = model.interactor.getCurrentRenderer();
    if (!renderer) {
      return;
    }
    model.currentImageNumber = i;

    function propMatch(j, prop, targetIndex) {
      if (
        prop.isA('vtkImageSlice') &&
        j === targetIndex &&
        prop.getPickable()
      ) {
        return true;
      }
      return false;
    }

    const props = renderer.getViewProps();
    let targetIndex = i;
    if (i < 0) {
      targetIndex += props.length;
    }
    let imageProp = null;
    let foundImageProp = false;
    for (let j = 0; j < props.length && !foundImageProp; j++) {
      if (propMatch(j, props[j], targetIndex)) {
        foundImageProp = true;
        imageProp = props[j];
      }
    }

    if (imageProp) {
      publicAPI.setCurrentImageProperty(imageProp.getProperty());
    }
  };

  //----------------------------------------------------------------------------
  publicAPI.setCurrentImageProperty = (imageProperty) => {
    model.currentImageProperty = imageProperty;
  };
  

  publicAPI.setActiveTool(null);
  model.interactionMode=='IMAGE_SLICING';
}

// ----------------------------------------------------------------------------
// Object factory
// ----------------------------------------------------------------------------

const DEFAULT_VALUES = {
  lastSlicePosition: 0,
  currentImageProperty: 0,
  currentImageNumber: -1,
  interactionMode: 'IMAGE2D',
  xViewRightVector: [0, 1, 0],
  xViewUpVector: [0, 0, -1],
  yViewRightVector: [1, 0, 0],
  yViewUpVector: [0, 0, -1],
  zViewRightVector: [1, 0, 0],
  zViewUpVector: [0, 1, 0],
};

// ----------------------------------------------------------------------------

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  // Inheritance
  vtkInteractorStyleManipulator.extend(publicAPI, model, initialValues);
  //vtkInteractorStyleTrackballCamera.extend(publicAPI, model, initialValues);

  macro.setGet(publicAPI, model, ['interactionMode']);

  // Object specific methods
  customInteractorStyle_slice(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(
  extend,
  'customInteractorStyle_slice'
);

// ----------------------------------------------------------------------------

export default { newInstance, extend };
