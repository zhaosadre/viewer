import React from "react";
import presets from '../presets.js';
import "./ModalStyle.css";
import ViewsApi from '../Services/ViewsApi'


export default class WindowWLSettings extends React.Component {
  constructor(props) {
    super(props);
    
    this.viewApi = ViewsApi.getInstance();
    
    this.viewApiListener = (whatChange) => {this.onPropertyChange(whatChange)};
    this.viewApi.addImagePropertiesListener(this.viewApiListener);
    
    this.initialWWL = this.viewApi.getWWL().slice();
    
    this.initialWWL[0] = Math.round(this.initialWWL[0]);
    this.initialWWL[1] = Math.round(this.initialWWL[1]);
    
    this.initialPresetID = null;
    if (this.viewApi.getTransferFunction())
      this.initialPresetID = this.viewApi.getTransferFunction().id;
    
    this.state = {
      isManual: false,
      currentPresetID: this.initialPresetID,
      windowWidthValue: this.initialWWL[0],
      windowLevelValue: this.initialWWL[1],
    };
  }
  
  componentWillUnmount() {
    this.viewApi.removeImagePropertiesListener(this.viewApiListener);
  }
  
  onPropertyChange(whatChange) {
    if (whatChange=="wwl") {
      const wwl = this.viewApi.getWWL().slice();
      
      if (wwl[0] == this.state.windowWidthValue && wwl[1] == this.state.windowLevelValue)
        return;
        
      this.setState({
        windowWidthValue: Math.round(wwl[0]),
        windowLevelValue: Math.round(wwl[1]),
      });
    }
  }
  
  changePresetID = event => {
    let preset = null;
    preset = presets.find(preset => preset.id === event.target.value);
    
    this.viewApi.setTransferFunction(preset);
    
    this.setState({currentPresetID: event.target.value});
  };
  
  changeWindowWidth = event => {
    this.viewApi.setWWL(event.target.value, this.state.windowLevelValue);
    this.setState({windowWidthValue: event.target.value});
  };
  
  changeWindowLevel = event => {
    this.viewApi.setWWL(this.state.windowWidthValue, event.target.value);
    this.setState({windowLevelValue: event.target.value});
  };
  
  reset() {
    this.viewApi.setWWL(this.initialWWL[0], this.initialWWL[1]);
    
    this.setState({
      windowWidthValue: this.initialWWL[0],
      windowLevelValue: this.initialWWL[1],
    });
  }
  
  render() {  
    const presetsOptions = presets.map(preset => {
      return (
        <option key={preset.id} value={preset.id}>
          {preset.name}
        </option>
      );
    });
  
    return (
      <div class="modal" id="modal">
        <h2>WindowWL Settings</h2>
        <div class="content">
      <div class="flex-container-column">
        <select value={this.state.currentPresetID} onChange={this.changePresetID}>
          {presetsOptions}
        </select>
        <div class="flex-container-row">
          <label>Window width</label>
          <input type="range" id="widthSlider" min="0" max="1800" value={this.state.windowWidthValue} onChange={this.changeWindowWidth} />
          <label>{this.state.windowWidthValue}</label>
        </div>
        <div class="flex-container-row">
          <label>Window level</label>
          <input type="range" id="levelSlider" min="-1000" max="3000" value={this.state.windowLevelValue} onChange={this.changeWindowLevel} />
          <label>{this.state.windowLevelValue}</label>
        </div>
      </div>
    </div>
        <div class="actions">
        <button class="applyButton" onClick={() => this.reset()}>
            Reset
          </button>
          <button class="closeButton" onClick={this.props.onClose}>
            Close
          </button>
        </div>
      </div>
    );
  }
}
