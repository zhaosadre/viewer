import React from "react";
import "./ModalStyle.css";
import ViewsApi from '../Services/ViewsApi'

export default class PaintSettings extends React.Component {
  constructor(props) {
    super(props);
    
    this.viewApi = ViewsApi.getInstance();
    
    this.state = {
      currentRadius: this.viewApi.getRadius(),
    };
  }
  
  changePaintRadius = event => {
    this.setState({currentRadius: event.target.value});
  };
  
  apply(){
    if(this.viewApi.setPaintRadius(this.state.currentRadius))
    this.props.onClose();
  };
  
  render() {  
    return (
      <div class="modal" id="modal">
        <h2>Paint Settings</h2>
        <div class="content">
          <div class="flex-container-column">
            <div class="flex-container-row">
              <label>Radius</label>
              <input type="range" id="paintRadius" min="1" max="50" value={this.state.currentRadius} onChange={this.changePaintRadius} />
              <label>{this.state.currentRadius}</label>
            </div>
          </div>
        </div>
        <div class="actions">
          <button class="applyButton" onClick={() => this.apply()}>
            apply
          </button>
          <button class="closeButton" onClick={this.props.onClose}>
            close
          </button>
        </div>
      </div>
    );
  }
}
