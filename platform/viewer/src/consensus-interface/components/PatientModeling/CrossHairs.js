
import macro from 'vtk.js/Sources/macro';
import vtkCoordinate from 'vtk.js/Sources/Rendering/Core/Coordinate';

function CrossHairs (publicAPI, model) {
  const scale = 1; //TODO
  
  model.svgContainer = document.createElement('svg');
  model.svgContainer.setAttribute(
    'style',
    'position: absolute; top: 0; left: 0; width: 100%; height: 100%;'
  );
  model.svgContainer.setAttribute('version', '1.1');
  model.svgContainer.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
  
  model.renderer = model.genericRenderWindow.getRenderer();
  model.renderWindow = model.genericRenderWindow.getRenderWindow();
  model.interactor = model.renderWindow.getInteractor();
  model.openGLRenderWindow = model.genericRenderWindow.getOpenGLRenderWindow();
  
  model.scale = scale;
    
  let [width, height] = model.openGLRenderWindow.getSize();
  width = width*scale;
  height = height*scale;

  model.container.appendChild(model.svgContainer);
  
  model.svgContainer.setAttribute(
   'viewBox',
    `0 0 ${width} ${height}`
  );
    
  model.svgContainer.setAttribute('width', `${width}`);
  model.svgContainer.setAttribute('height', `${height}`);
    
  model.node = document.createElement('g');
  model.svgContainer.appendChild(model.node);
 
 
  publicAPI.resize = () => {
    let [width, height] = model.openGLRenderWindow.getSize();
    width = width*model.scale;
    height = height*model.scale;
    
    model.svgContainer.setAttribute('width', `${width}`);
    model.svgContainer.setAttribute('height', `${height}`);
  }
 
  publicAPI.delete = () => {
    model.svgContainer.removeChild(model.node);
    model.container.removeChild(model.svgContainer);
  }
  
  publicAPI.setPoint = (worldPos) => {
    requestAnimationFrame(() => {
      if (!worldPos) {
        model.node.innerHTML = '';
        return;
      }
      
      const scale = model.scale;  
      
      const width = parseInt(model.svgContainer.getAttribute('width'), 10);
      const height = parseInt(model.svgContainer.getAttribute('height'), 10);   

      const wPos = vtkCoordinate.newInstance();
      wPos.setCoordinateSystemToWorld();
      wPos.setValue(...worldPos);

      const displayPosition = wPos.getComputedDisplayValue(model.renderer);
      
      const p = displayPosition.slice();
      p[0] = displayPosition[0] * scale;
      p[1] = height - displayPosition[1] * scale;

      const left = [0, height / scale / 2];
      const top = [width / scale / 2, 0];
      const right = [width / scale, height / scale / 2];
      const bottom = [width / scale / 2, height / scale];
        
        
      const strokeColor= '#d19c4d';
      const strokeWidth= 1;
      const strokeDashArray= '';
      const padding= 20;

      model.node.innerHTML = `
        <g id="container" fill-opacity="1" stroke-dasharray="none" stroke="none" stroke-opacity="1" fill="none">
         <g>
         <!-- TODO: Why is this <svg> necessary?? </svg> If I don't include it, nothing renders !-->
         <svg version="1.1" viewBox="0 0 ${width} ${height}" width=${width} height=${height} style="width: 100%; height: 100%">
         <!-- Top !-->
          <line
            x1="${p[0]}"
            y1="${top[1]}"
            x2="${p[0]}"
            y2="${p[1] - padding}"
            stroke="${strokeColor}"
            stroke-dasharray="${strokeDashArray}"
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width="${strokeWidth}"
          ></line>
          <!-- Right !-->
          <line
            x1="${right[0]}"
            y1="${p[1]}"
            x2="${p[0] + padding}"
            y2="${p[1]}"
            stroke-dasharray="${strokeDashArray}"
            stroke="${strokeColor}"
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width=${strokeWidth}
          ></line>
          <!-- Bottom !-->
          <line
            x1="${p[0]}"
            y1="${bottom[1]}"
            x2="${p[0]}"
            y2="${p[1] + padding}"
            stroke-dasharray="${strokeDashArray}"
            stroke="${strokeColor}"
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width=${strokeWidth}
          ></line>
          <!-- Left !-->
          <line
            x1="${left[0]}"
            y1="${p[1]}"
            x2="${p[0] - padding}"
            y2="${p[1]}"
            stroke-dasharray="${strokeDashArray}"
            stroke="${strokeColor}"
            stroke-linecap="round"
            stroke-linejoin="round"
            stroke-width=${strokeWidth}
          ></line>
         </g>
        </g>
            `;
    });
  };
}
  
const DEFAULT_VALUES = {
};

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  macro.obj(publicAPI, model);
  macro.setGet(publicAPI, model, ['container', 'genericRenderWindow']);
  CrossHairs(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(extend, 'CrossHairs');

// ----------------------------------------------------------------------------

export default { newInstance, extend };

