
import React, { Component } from "react";
import {api} from "dicomweb-client";
import uniqBy from 'lodash/uniqBy';
import ReactDOM from 'react-dom';

import ImagePreview from './ImagePreview.js';
import CustomClickMenu from './CustomClickMenu.js';
import DataSet from './../../data/DataSet.js';

import './styles.css'


export default class ImagePreviews extends React.Component {
  constructor(props) {
    super(props);
    
    this.dataSet = DataSet.getInstance();
    
    this.dataListener = (event) => this.dataSetListener(event);
    this.dataSet.addListener(this.dataListener);
    
    this.state = {
      SeriesInstanceUIDs: this.props.SeriesInstanceUIDs,
      activeSeriesInstanceUID: this.props.activeSeriesInstanceUID,
      activeSecondarySeriesInstanceUID: this.props.activeSecondarySeriesInstanceUID,
      study: this.dataSet.getStudy(),
    };
    
    this.dataSet = DataSet.getInstance();
  }
  
  dataSetListener(event) {
    if (event.type=="created") { // In case not already loaded when calling constructor
      switch(event.target) {
        case this.props.StudyInstanceUID:
          this.setState({study: this.dataSet.getStudy()});
          break;
        case this.promiseSeriesInstanceUID:
          this.handlePrimaryClick(this.promiseSeriesInstanceUID);
          break;
      }
    }
  }
  
  componentWillUnmount() {
    this.dataSet.removeListener(this.dataListener);
  }
  
    componentDidMount() {
    this.componentDidUpdate();
  }
  
  componentDidUpdate() {
    if(!this.state.study)
      return;
  
    if(!this.state.activeSeriesInstanceUID) {
      const allSeries = uniqBy(this.state.study, 'SeriesInstanceUID');
      
      if (!allSeries.length)
        return;
        
      const CTUIds = allSeries.filter(study => (study.modalities=="CT")).map(study => study.SeriesInstanceUID);
      
      let CTUId;
      
      if(CTUIds.length)
        CTUId = CTUIds[0];
      else
        CTUId = allSeries.filter(study => (study.modalities=="MR" || study.modalities=="PT")).map(study => study.SeriesInstanceUID)[0];
      
      if (this.dataSet.getImageDataSet().getImage(CTUId))
        this.handlePrimaryClick(CTUId);
      else
        this.promiseSeriesInstanceUID = CTUId;
    }
  }
  
  handlePrimaryClick(activeSeriesInstanceUID) {
    let activeSecondarySeriesInstanceUID = this.state.activeSecondarySeriesInstanceUID;
    
    if(this.state.activeSeriesInstanceUID==activeSeriesInstanceUID) {
      activeSeriesInstanceUID = null; //unset
      activeSecondarySeriesInstanceUID = null;
    }
    
    if (this.state.activeSecondarySeriesInstanceUID==activeSeriesInstanceUID)
      activeSecondarySeriesInstanceUID = null;
    
    if (this.props.onPrimaryItemClick)
      this.props.onPrimaryItemClick(activeSeriesInstanceUID);
    if (this.props.onSecondaryItemClick)
      this.props.onSecondaryItemClick(activeSecondarySeriesInstanceUID);
      
    this.setState({activeSeriesInstanceUID, activeSecondarySeriesInstanceUID});
  }
  
  handleSecondaryClick(activeSecondarySeriesInstanceUID) {
    if(!this.state.activeSeriesInstanceUID)
      return;
      
    if(this.state.activeSeriesInstanceUID==activeSecondarySeriesInstanceUID)
      return;
    
    if(this.state.activeSecondarySeriesInstanceUID==activeSecondarySeriesInstanceUID)
      activeSecondarySeriesInstanceUID = null; //unset
    
    if (this.props.onSecondaryItemClick)
      this.props.onSecondaryItemClick(activeSecondarySeriesInstanceUID);
      
    this.setState({activeSecondarySeriesInstanceUID});
  }
  
  renderPreview() {
    const itemContent = ["Set/Unset primary image", "Set/Unset secondary image", "Cancel"];

    return this.state.SeriesInstanceUIDs.map(
      SeriesInstanceUID => {
      
        let className = "scrollItem";
        className = SeriesInstanceUID==this.state.activeSeriesInstanceUID ? className + " active" : className;
        className = SeriesInstanceUID==this.state.activeSecondarySeriesInstanceUID ? className + " active_secondary" : className;
        
        const onItemClick = [
          () => this.handlePrimaryClick(SeriesInstanceUID),
          () => this.handleSecondaryClick(SeriesInstanceUID),
          () => {},
        ];
        
        let isEnabled = () => false;
        
        if (this.props.clickEnabled) {
          isEnabled = () => {return !!this.dataSet.getImageDataSet().getImage(SeriesInstanceUID);};
        }
        
        return (
          <div id={"preview_ "+SeriesInstanceUID} className={className}>
            <CustomClickMenu
              isEnabled={isEnabled}
              targetId={"preview_ "+SeriesInstanceUID}
              itemContent={itemContent}
              onItemClick={onItemClick}
            />
            <ImagePreview
              SeriesInstanceUID={SeriesInstanceUID}
            />
          </div>
        );
      }
    );
  }
  
  render() {
    return (
      <div className="imagePreviews">
        <div className="scrollPreviews">
          {this.renderPreview()}
        </div>
      </div>
    );
  }
}

