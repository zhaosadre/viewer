import React from 'react';
import { Component } from 'react';

import VTKView from './VTKView.js';
import './styles.css'


export default class VTKViews extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      activeLabel: '1',
      displayCrosshairs: true,
      fullScreen: false,
      secondarySeriesInstanceUID: this.props.secondarySeriesInstanceUID,
      SeriesInstanceUID: this.props.SeriesInstanceUID,
      sliceMode1: "K",
    };
  }
  
  setFullScreen(sliceMode, fullScreen) {
    this.setState({sliceMode1: sliceMode, fullScreen})
  }
  
  shouldComponentUpdate(nextProps, nextState) {
    return (this.props.secondarySeriesInstanceUID!=nextProps.secondarySeriesInstanceUID
      || this.props.SeriesInstanceUID!=nextProps.SeriesInstanceUID
      || this.props.layout!=nextProps.layout
      || this.state.fullScreen!=nextState.fullScreen)
  }
  
  render() {
    console.log('VTKVIEWS', this.props.secondarySeriesInstanceUID, this.props.SeriesInstanceUID)
    if(!this.state.SeriesInstanceUID)
      return(null);
    
    if (this.state.fullScreen) {
      
      return (
        <div className="Views3">
          <VTKView
            SeriesInstanceUID={this.props.SeriesInstanceUID}
            secondarySeriesInstanceUID={this.props.secondarySeriesInstanceUID}
            fullScreen={this.state.fullScreen}
            imageDataSet={this.props.imageDataSet}
            segDataSet={this.state.segDataSet}
            sliceMode={this.state.sliceMode1}
            setFullScreen={(sliceMode, value) => {this.setFullScreen(sliceMode, value)}}
            viewType = {"2D"}
          />
        </div>
      );
    }

    switch (this.props.layout) {
      case 2:
        return (
          <>
            <div className="Views3">
              <div className="col">
                <div className="row-sm-1">
                  <VTKView
                    SeriesInstanceUID={this.props.SeriesInstanceUID}
                    secondarySeriesInstanceUID={this.props.secondarySeriesInstanceUID}
                    fullScreen={this.state.fullScreen}
                    imageDataSet={this.props.imageDataSet}
                    segDataSet={this.props.segDataSet}
                    sliceMode={"K"}
                    setFullScreen={(sliceMode, value) => {this.setFullScreen(sliceMode, value)}}
                    viewType = {"2D"}
                  />
                </div>
              </div>
              <div className="col">
                <div className="row-sm-1">
                  <VTKView
                    SeriesInstanceUID={this.props.secondarySeriesInstanceUID}
                    secondarySeriesInstanceUID={null}
                    fullScreen={this.state.fullScreen}
                    imageDataSet={this.props.imageDataSet}
                    segDataSet={this.props.segDataSet}
                    sliceMode={'I'}
                    setFullScreen={(sliceMode, value) => {this.setFullScreen(sliceMode, value)}}
                    viewType = {"2D"}
                  />
                </div>
              </div>
            </div>
          </>
        );
      case 3:
        return (
          <>
            <div className="Views3">
              <div className="col">
                <div className="row-sm-1">
                  <VTKView
                    SeriesInstanceUID={this.props.SeriesInstanceUID}
                    secondarySeriesInstanceUID={this.props.secondarySeriesInstanceUID}
                    fullScreen={this.state.fullScreen}
                    imageDataSet={this.props.imageDataSet}
                    segDataSet={this.props.segDataSet}
                    sliceMode={"K"}
                    setFullScreen={(sliceMode, value) => {this.setFullScreen(sliceMode, value)}}
                    viewType = {"2D"}
                  />
                </div>
              </div>
                <div className="col">
                  <div className="row-sm-1">
                    <div className="row-sm-2">
                      <VTKView
                        SeriesInstanceUID={this.props.SeriesInstanceUID}
                        secondarySeriesInstanceUID={this.props.secondarySeriesInstanceUID}
                        fullScreen={this.state.fullScreen}
                        imageDataSet={this.props.imageDataSet}
                        segDataSet={this.props.segDataSet}
                        sliceMode={'I'}
                        setFullScreen={(sliceMode, value) => {this.setFullScreen(sliceMode, value)}}
                        viewType = {"2D"}
                      />
                    </div>
                    <div className="row-sm-2">
                      <VTKView
                        SeriesInstanceUID={this.props.SeriesInstanceUID}
                        secondarySeriesInstanceUID={this.props.secondarySeriesInstanceUID}
                        fullScreen={this.state.fullScreen}
                        imageDataSet={this.props.imageDataSet}
                        segDataSet={this.props.segDataSet}
                        sliceMode={'J'}
                        setFullScreen={(sliceMode, value) => {this.setFullScreen(sliceMode, value)}}
                        viewType = {"2D"}
                      />
                    </div>
                </div>
              </div>
            </div>
          </>
        );
    }
  }
}

