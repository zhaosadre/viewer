
import macro from 'vtk.js/Sources/macro';
import vtkCoordinate from 'vtk.js/Sources/Rendering/Core/Coordinate';


//https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? "0" + hex : hex;
}

//https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function rgbToHex(r, g, b) {
  r = Math.round(r); g = Math.round(g); b = Math.round(b);
  return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

//https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? [parseInt(result[1], 16),
    parseInt(result[2], 16),
    parseInt(result[3], 16)
  ] : null;
}

function SVGContour (publicAPI, model) {
  const scale = 1; //TODO
  
  model.svgContainer = document.createElement('svg');
  model.svgContainer.setAttribute(
    'style',
    'position: absolute; top: 0; left: 0; width: 100%; height: 100%;'
  );
  model.svgContainer.setAttribute('version', '1.1');
  model.svgContainer.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
  
  model.renderer = model.genericRenderWindow.getRenderer();
  model.renderWindow = model.genericRenderWindow.getRenderWindow();
  model.interactor = model.renderWindow.getInteractor();
  model.openGLRenderWindow = model.genericRenderWindow.getOpenGLRenderWindow();
  
  model.scale = scale;
    
  let [width, height] = model.openGLRenderWindow.getSize();
  width = width*scale;
  height = height*scale;

  model.container.appendChild(model.svgContainer);
  
  model.svgContainer.setAttribute(
   'viewBox',
    `0 0 ${width} ${height}`
  );
    
  model.svgContainer.setAttribute('width', `${width}`);
  model.svgContainer.setAttribute('height', `${height}`);
    
  model.node = document.createElement('g');
  model.svgContainer.appendChild(model.node);
  
  model.polyData = [];
  model.color = [];
 
 
  publicAPI.resize = () => {
    let [width, height] = model.openGLRenderWindow.getSize();
    width = width*model.scale;
    height = height*model.scale;
    
    model.svgContainer.setAttribute('width', `${width}`);
    model.svgContainer.setAttribute('height', `${height}`);
    
    if(model.polyData.length)
      publicAPI.setData(model.polyData, model.color);
  }
 
  publicAPI.delete = () => {
    model.svgContainer.removeChild(model.node);
    model.container.removeChild(model.svgContainer);
    
    model.node = null;
    model.svgContainer = null;
    model.container = null;
    model.renderer = null;
    model.renderWindow = null;
    model.interactor = null;
    model.openGLRenderWindow = null;
  }
  
  const getPolygonString = (polyData, colorValue) => {    
    const width = parseInt(model.svgContainer.getAttribute('width'), 10);
    const height = parseInt(model.svgContainer.getAttribute('height'), 10);   

    const strokeColor= colorValue;
    const strokeWidth= 3;
    const strokeDashArray= '';
    const padding= 20;
      
    let polygonString = "";

    for (let index1=0; index1<polyData.length; index1++) {
      const polyDataItem = polyData[index1];
      
      let broken = false;
      
      let initialPos = polyDataItem[0];
      let prevPos = polyDataItem[0];
      
      if (!broken) {
        let displayPositionsString = "";
          
          polyDataItem.forEach((pos, index) => {        
            const wPos = vtkCoordinate.newInstance();
            
            wPos.setCoordinateSystemToWorld();
            wPos.setValue(...[pos[0], pos[1], pos[2]]);
            
            let dispPos = wPos.getComputedDisplayValue(model.renderer);
            
            if (index==0) {
              displayPositionsString += "M ";
              displayPositionsString += dispPos[0];
              displayPositionsString += " ";
              displayPositionsString += height-dispPos[1];
              displayPositionsString += " L ";
            } else {
              displayPositionsString += dispPos[0];
              displayPositionsString += " ";
              displayPositionsString += height-dispPos[1];
              
              if (index<polyDataItem.length-1)
                displayPositionsString += " , ";
            }
          });
            
          polygonString += `<path
                d="${displayPositionsString}"
                fill="none"
                stroke="${strokeColor}"
                stroke-dasharray="${strokeDashArray}"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="${strokeWidth}"
              ></path>`;
      }
    }
    return polygonString;
  };
  
  
  publicAPI.setData = (polyData, color) => {
      model.polyData = polyData;
      model.color = color;
         
      if (!polyData.length){
        model.node.innerHTML = '';
        return;
      }
      
      let colorValue = rgbToHex(color[0], color[1], color[2]);
      
      const width = parseInt(model.svgContainer.getAttribute('width'), 10);
      const height = parseInt(model.svgContainer.getAttribute('height'), 10);   

      
      let polygonString = getPolygonString(polyData, colorValue);
      
      model.node.innerHTML = `
        <g id="container" fill-opacity="1" stroke-dasharray="none" stroke="none" stroke-opacity="1" fill="none">
         <g>
         <!-- TODO: Why is this <svg> necessary?? </svg> If I don't include it, nothing renders !-->
         <svg version="1.1" viewBox="0 0 ${width} ${height}" width=${width} height=${height} style="width: 100%; height: 100%">
          ${polygonString}
         </g>
        </g>
            `;
  };
  
  publicAPI.getData = () => {
    return model.polyData;
  };
}
  
const DEFAULT_VALUES = {
};

export function extend(publicAPI, model, initialValues = {}) {
  Object.assign(model, DEFAULT_VALUES, initialValues);

  macro.obj(publicAPI, model);
  macro.setGet(publicAPI, model, ['container', 'genericRenderWindow']);
  SVGContour(publicAPI, model);
}

// ----------------------------------------------------------------------------

export const newInstance = macro.newInstance(extend, 'SVGContour');

// ----------------------------------------------------------------------------

export default { newInstance, extend };

