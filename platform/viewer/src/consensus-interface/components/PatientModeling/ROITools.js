
import React, { Component } from "react";
import { Icon } from '../../../../../ui/src/elements/Icon';
import {WindowWLSettings, PaintSettings, LayoutSettings} from './ROIToolsSettings'
import ContourModification from './ContourModification.js';

import './styles.css';


class TabLinks extends React.Component {
  render() {
    return (<button class={this.props.active ? "tablinks" + " active" : "tablinks"} id={this.props.id} onClick={() => {this.props.onClick()}}> {this.props.value} </button>);
  }
}

export default class ROITools extends React.Component {
  constructor(props) {
    super(props);
    
    this.contourModification = ContourModification.getInstance();
    
    this.state = {
      settingWindow: null,
      layoutWindow: null,
    };
  }
  
  componentWillUnmount() {
    this.contourModification.saveInputForLater();
  }
  
  componentDidMount() {
    this.componentDidUpdate();
  }
  
  componentDidUpdate() {
    // TODO: set input value if there were already entered values?
  
    const labelInput = document.getElementById("labelInput");
    const descriptionInput = document.getElementById("descriptionInput");
    
    const {labelInputValue, descriptionInputValue} = this.contourModification.getSavedInputValues();
    
    if (labelInput && descriptionInput) {
      this.contourModification.setDescriptionInput(labelInput, descriptionInput);
      
      if (labelInputValue) {
        labelInput.value = labelInputValue.slice();
        descriptionInput.value = descriptionInputValue.slice();
      }
    }
  }
  
  closeSettingWindow(){
	  this.setState({settingWindow: null});
  }
  
  toggleLayoutWindow() {
    if (this.state.layoutWindow)
	    this.setState({layoutWindow: null});
	  else
	    this.setState({
	      settingWindow: null,
	      layoutWindow:
	        <LayoutSettings setLayout={(layout) => {this.props.api.setLayout(layout); this.toggleLayoutWindow();}} layout={this.props.layout}/>
	    });
  }
  
  toggleSettingsWindow() {
	  let windowSetting = null;
    switch(this.props.activeTool) {
      case "brush":
        windowSetting = <PaintSettings onClose={() => this.closeSettingWindow()} />
        break;
      case "sculpt":
        windowSetting = <PaintSettings onClose={() => this.closeSettingWindow()} />
        break;
      case "erase":
        windowSetting = <PaintSettings onClose={() => this.closeSettingWindow()} />
        break;
      case "crossHairs":
        
        break;
      case "adjust":
        windowSetting = <WindowWLSettings onClose={() => this.closeSettingWindow()} />;
        break;
      case "pan":
        
        break;
      case "zoom":
        
        break;
      case "label":
      
        break;
      default:
        windowSetting = null;
        break;
      }
    if(windowSetting !== null) {
      this.setState({layoutWindow: windowSetting?null:this.state.layoutWindow, settingWindow: windowSetting});
    }
  }
  
  render() {
    let brushState = null;
    let sculptState = null;
    let eraseState = null;
    let modificationInput = null;
    let modificationCancel = null;
    
    if (this.props.modifying) {      
      brushState = this.props.activeTool=="brush"? " active" : "";
      sculptState = this.props.activeTool=="sculpt"? " active" : "";
      eraseState = this.props.activeTool=="erase"? " active" : "";
      
      modificationInput =
        <form>
          <input id="labelInput" class="formInput" type="text" name="fname" placeholder="Label (eg. change1)"/><br></br>
          <input id="descriptionInput" class="formInput" type="text" name="description" placeholder="Description (eg. Missing the distal part of the prostate)"/>
        </form>
      
      const mSyle = {
        verticalAlign: "center",
        textAlign: "center",
      };
      
      modificationCancel =
        <table onClick={() => this.props.api.cancelModification()}>
          <tr style={mSyle}>
            <Icon className="smallIcon"name="times" onClick={() => this.props.api.endAll()}/>
          </tr>
          <tr style={mSyle}>
            Cancel
          </tr>
        </table>
    }
    else {
      brushState = " disabled";
      sculptState = " disabled";
      eraseState = " disabled";
    }
    
    let optionMenu = null
    let optionLabel = null
    let iconName = null
    switch(this.props.activeTool) {
      case "brush":
        optionLabel = 'Brush'
        iconName = 'brush'
        break;
      case "sculpt":
        optionLabel = 'Sculpt'
        iconName = "circle-o"
        break;
      case "erase":
        optionLabel = 'Erase'
        iconName = "eraser"
        break;
      case "crossHairs":
        
        break;
      case "adjust":
        optionLabel = 'W W/L'
        iconName = "level"
        break;
      case "zoom":
        
        break;
      case "label":
      
        break;
    }
    
    if (optionLabel) {
      optionMenu = 
        <table>
          <tr onClick={() => this.toggleSettingsWindow()}>
            <td><Icon className="medIcon" name={iconName}/></td><td>{optionLabel + ' options'}</td>
          </tr>
        </table>
    }
    
    return (
      <div class="ROITools">
        <div class="drawTools">
          <div className="space20px"></div>
          <Icon className="medIcon" name={this.props.layout==2? "layoutTwo" : "layoutThree"} onClick={() => this.toggleLayoutWindow()}/>
          <div className="space20px"></div>
          <Icon className="medIcon" name="reset" onClick={() => this.props.api.undo()}/>
          <Icon className="medIcon"name="rotate" onClick={() => this.props.api.redo()}/>
          <div className="space20px"></div>
          <Icon className="medIcon"name="times" onClick={() => this.props.api.endAll()}/>
          <div className="space20px"></div>
          <table>
            <tr className={"roiTool"+brushState} onClick={() => this.props.api.startBrush()}>
              <td><Icon className="smallIcon" name="brush"/></td><td>Brush</td> 
            </tr>
            <tr className={"roiTool"+sculptState} onClick={() => this.props.api.startSculpt()}>
              <td><Icon className="smallIcon" name="circle-o"/></td><td>Sculpt</td>
            </tr>
          </table>
          <table>
            <tr className={"roiTool"+eraseState} onClick={() => this.props.api.startErase()}>
              <td><Icon className="smallIcon" name="eraser"/></td><td>Eraser</td> 
            </tr>
            <tr className="roiTool disabled">
              <td><Icon className="smallIcon" name="dot-circle"/></td><td>Smart brush</td>
            </tr>
          </table>
          <div className="space20px"></div>
          <table>
            <tr className={this.props.activeTool=="crossHairs"? "roiTool"+" active":"roiTool"} onClick={() => this.props.api.startCrossHairs()}>
              <td><Icon className="smallIcon" name="crosshairs"/></td><td>Crosshairs</td> 
            </tr>
            <tr className={this.props.activeTool=="adjust"? "roiTool"+" active":"roiTool"} onClick={() => this.props.api.startAdjust()}>
              <td><Icon className="smallIcon" name="level"/></td><td>Adjust</td>
            </tr>
          </table>
          <div className="space20px"></div>
          <table>
            <tr className={this.props.activeTool=="zoom"? "roiTool"+" active":"roiTool"} onClick={() => this.props.api.startZoom()}>
              <td><Icon className="smallIcon" name="magnifyingGlass"/></td><td>Zoom</td> 
            </tr>
            <tr className={this.props.activeTool=="pan"? "roiTool"+" active":"roiTool"} onClick={() => this.props.api.startPan()}>
              <td><Icon className="smallIcon" name="arrows"/></td><td>Pan</td> 
            </tr>
          </table>
          <div className="space20px"></div>
          <div className="space20px"></div>
          {optionMenu}
          <div className="space20px"></div>
          <div className="space20px"></div>
          {this.props.modifying ? (
            <table>
              <tr>
                <td>Modification: </td><td>{modificationInput}</td><td>{modificationCancel}</td>
              </tr>
            </table>) : (null)}          
        </div>
		    {this.state.settingWindow}
		    {this.state.layoutWindow}
      </div>
    );
  }
}

