
import React, { Component } from "react";

import "./customClickMenuStyle.css";

export default class CustomClickMenu extends React.Component{
  constructor(props) {
    super(props);
 
    this.targetId = this.props.targetId;
    this.onItemClick = this.props.onItemClick;
    this.itemContent = this.props.itemContent;
    
    this.listener = (event) => {this.handleClick(event)};
    
    this.state = {
      visible: false,
      x: 0,
      y: 0
    };
  }
  
  handleClick(event) {
    if (!this.props.isEnabled())
    {
      this.componentDidMount(); // To set listener once again
      return;
    }
    
    if (!this.state.visible)
      event.preventDefault();
    
    const clickX = event.clientX;
    const clickY = event.clientY;
    
    this.setState({visible: !this.state.visible, x: clickX, y: clickY});
  }
  
  handleSelection(itemNb) {
    this.onItemClick[itemNb]();
    
    this.setState({visible: false});
    
    this.componentDidMount();
  }
  
  componentDidMount() {
    const options = {
      once: true,
    };
    
    document.getElementById(this.targetId).addEventListener('click', this.listener, options);
  }
  
  componentWillUnmount() {
    document.getElementById(this.targetId).removeEventListener('click', this.listener);
  }
  
  render() {
    if(!this.state.visible)
      return null;
    
    const myStyle = {
      top: ""+this.state.y+"px",
      left: ""+this.state.x+"px",
    };
    
    return (
       <div className="custom-context" style={myStyle}>
        {
          this.itemContent.map((item, index) => {
            return (<div onClick={()=>{this.handleSelection(index)}} className="custom-context-item">{item}</div>);
            })
        }
      </div>
    );
  }
}

