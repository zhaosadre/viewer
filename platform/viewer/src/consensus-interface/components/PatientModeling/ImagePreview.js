
import React, { Component } from "react";
import {api} from "dicomweb-client";
import cornerstone from 'cornerstone-core';

import DataSet from './../../data/DataSet.js';
import './styles.css'

// https://stackoverflow.com/a/10638617
function parseDate(str) {
  let y, m, d;
  
  if (str.substr(4,2)=="19" || str.substr(4,2)=="20") {
    d = str.substr(0,2);
    m = str.substr(2,2) - 1;
    y = str.substr(4,4);
  } else {
    y = str.substr(0,4);
    m = str.substr(4,2) - 1;
    d = str.substr(6,2);
  }
  
  const D = new Date(y,m,d);
  return (D.getFullYear() == y && D.getMonth() == m && D.getDate() == d) ? D : 'invalid date';
}

class ImageThumbnail extends React.Component {
  constructor(props) {
    super(props);
    
    this.id = "thumbnail_"+this.props.SeriesInstanceUID;
  }
  
  componentDidMount() {
    if (this.props.image) {
      const metaData = this.props.image.getMetaData();
      
      const element = document.getElementById(this.id);
      const data = this.props.image.getSlice(Math.round(metaData.length/2));
      const getPixelData = () => {return data;};
      
      const image = {
        getPixelData,
        sizeInBytes : metaData[0].Rows*metaData[0].Columns*Math.round(metaData[0].BitsAllocated/8),
        minPixelValue : 0,
        maxPixelValue : 65535/2,
        //render : cornerstone.renderPseudoColorImage,
        intercept : 0,
        windowCenter : 45,
        windowWidth : 350,
        slope: 1,
        
        imageId: metaData[0].SOPInstanceUID,
        rows: metaData[0].Rows,
        columns: metaData[0].Columns,
        height: metaData[0].Rows,
        width: metaData[0].Columns,
        color: false,
        columnPixelSpacing: 1,
        rowPixelSpacing: 1,
      }

      cornerstone.enable(element);
      cornerstone.displayImage(element, image);
    }
  }
  
  render() {
    return(
      <div id={this.id} background-color="var(--ui-gray-darker)">
      </div>
    );
  }
}

export default class ImagePreview extends React.Component {
  constructor(props) {
    super(props);
    
    this.dataSet = DataSet.getInstance();
    this.dataListener = (event) => this.dataSetListener(event);
    this.dataSet.addListener(this.dataListener);
    
    const imageDataSet = this.dataSet.getImageDataSet();
    let image = null;
    if (imageDataSet && imageDataSet.getImage(this.props.SeriesInstanceUID))
      image = imageDataSet.getImage(this.props.SeriesInstanceUID);
    
    this.state = {
      image,
      progress: null,
      SeriesInstanceUID: this.props.SeriesInstanceUID,
    };
  }
  
  dataSetListener(event) {
    if (event.target==this.props.SeriesInstanceUID) {
      switch(event.type) {
        case "created":
          this.setState({image: this.dataSet.getImageDataSet().getImage(this.props.SeriesInstanceUID)});
          break;
        case "progress":
          this.setState({progress: event.value});
          break;
      }
    }
  }
  
  componentWillUnmount() {
    this.dataSet.removeListener(this.dataListener);
  }
  
  render() {    
    let progressBar = null;
    
    if (this.state.progress && this.state.progress<1) {      
      const style = {
        height: "20px",
        width: this.state.progress*100 + "%",
      }
      
      progressBar = 
        <div className="ProgressBar" style={style}>
          <div className="Progress">{Math.round(this.state.progress*100)+"%"}</div>
        </div>;
    }
    
    const series = this.dataSet.getStudy().filter(elem => elem.SeriesInstanceUID==this.props.SeriesInstanceUID);
    const metaData = series[0];
    
    let thumbnail = null;
    if (this.state.image)
      thumbnail = <ImageThumbnail image={this.state.image} SeriesInstanceUID={this.state.SeriesInstanceUID}/>;
    else
      thumbnail = <div>Loading preview...</div>
    
    const header = progressBar ? progressBar : metaData.SeriesDescription;
      
    return(
      <div className='seriesTableList'>
        {header}
        <div overflow="hidden">
          <div className="scrollItem" width="50%">
            {thumbnail}
          </div>
          <div className="scrollItem" width="50%">
            <table className='seriesTable'>
              <tr> <td className="label">Modality</td> <td>{metaData.modalities ? metaData.modalities : ""}</td> </tr>
              <tr> <td className="label">Description</td> <td>{metaData.SeriesDescription ? metaData.SeriesDescription : ""}</td> </tr>
              <tr> <td className="label">Protocol</td> <td></td> </tr>
              <tr> <td className="label">Patient position</td> <td>{metaData.PatientPosition ? metaData.PatientPosition : ""}</td> </tr>
              <tr> <td className="label">Imaging system</td> <td>{metaData.Manufacturer ? metaData.Manufacturer : ""}</td> </tr>
              <tr> <td className="label">Creation date</td> <td>{metaData.InstanceCreationDate ? parseDate(metaData.InstanceCreationDate).toDateString() : ""}</td> </tr>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

