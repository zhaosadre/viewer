
import React, { Component } from "react";
import ImagePreviewsWithoutActivation from '../PatientModeling/ImagePreviewsWithoutActivation.js';
import uniqBy from 'lodash/uniqBy';
import DataSet from './../../data/DataSet.js';

import './styles.css';


class PatientInformation extends React.Component {
  render() {
    const study = this.props.study[0];
    
    return (
      <div class='tableList'>
        <p> Patient Information </p>
        <table class='patientTable'>
          <tr> <td>PatientName</td> <td>{study.PatientName ? study.PatientName : ""}</td> </tr>
          <tr> <td>PatientID</td> <td>{study.PatientID ? study.PatientID : ""}</td> </tr>
          <tr> <td>PatientBirthDate</td> <td>{study.PatientBirthDate ? study.PatientBirthDate : ""}</td> </tr>
          <tr> <td>PatientSex</td> <td>{study.PatientSex ? study.PatientSex : ""}</td> </tr>
        </table>
      </div>
    );
  }
}

export default class PatientDataManagement extends React.Component {
  constructor(props) {
    super(props);
    
    this.dataSet = DataSet.getInstance();
    this.dataSetListener = null;
    
    this.state = {
      StudyInstanceUID:this.props.StudyInstanceUID,
      study: this.dataSet.getStudy(),
    };
  }
  
  componentDidMount() {
    if (!this.state.study) {
      this.dataSetListener = (event) => {
        if (event.target==this.props.StudyInstanceUID && event.type=="created")
          this.setState({study: this.dataSet.getStudy()});
      }
      
      this.dataSet.addListener(this.dataSetListener);
    }
  }
  
  componentWillUnmount() {
    this.dataSet.removeListener(this.dataSetListener);
  }
  
  getSeriesInstanceUIDs() {
    if (!this.state.study)
      return null;
      
    return uniqBy(this.state.study, 'SeriesInstanceUID').filter(study => (study.modalities=="CT" || study.modalities=="MR" || study.modalities=="PT")).map(study => study.SeriesInstanceUID);
  }

  render() {
    const study = this.state.study;
    
    if (!this.props.StudyInstanceUID)
      return (<div>No patient selected</div>);
      
    if (!study)
      return (<div>Loading...</div>);
    
    return (
      <div>
        <div width="100%" height="70%">
          <PatientInformation study={study}/>
        </div>
        <div height="30%">
          <ImagePreviewsWithoutActivation
              clickEnabled={false}
              SeriesInstanceUIDs={this.getSeriesInstanceUIDs()}
          />
        </div>
      </div>
    );
  }
}

