
import React, { Component } from "react";

import PatientSelection from "./PatientSelection/PatientSelection.js";
import PatientDataManagement from "./PatientDataManagement/PatientDataManagement.js";
import PatientModeling from "./PatientModeling/PatientModelingVTK.js";
import DICOMSettings from "./Settings/DICOMSettings.js";
import DataSet from './../data/DataSet.js';

import './styles.css';

class TabLinks extends React.Component {
  render() {
    return (<button className={this.props.active ? "tablinks" + " active" : "tablinks"} id={this.props.id} onClick={() => {this.props.onClick()}}> {this.props.value} </button>);
  }
}

export default class HomePage extends React.Component {
  constructor(props) {
    super(props);
    
    let activeTabID = "PatientSelection";
    let StudyInstanceUID = null;
    
    if (this.props.StudyInstanceUID) {
      console.log("Starting viewer with images");
      
      //const dataSet = DataSet.getInstance();
      //dataSet.setDataSet(this.props.dataSet);
      
      activeTabID = "PatientDataManagement";
      StudyInstanceUID = this.props.StudyInstanceUID;
    }
      
    this.state = {
      activeTabID,
      StudyInstanceUID: StudyInstanceUID,
    };
  }
  
  handleTabLinksClick(activeTabID) {
    this.setState({activeTabID});
  }
  
  handlePatientSelection(StudyInstanceUID) {
    this.setState({StudyInstanceUID});
  }
  
  activeTabContent() {    
    switch (this.state.activeTabID) {
      case "PatientSelection":
        return (<PatientSelection activeStudyInstanceUID={this.state.StudyInstanceUID} setStudy={(studyInstanceUID) => {this.handlePatientSelection(studyInstanceUID)}}/>); break;
      case "PatientDataManagement":
        return (<PatientDataManagement StudyInstanceUID={this.state.StudyInstanceUID}/>); break;
      case "PatientModeling":
        return (<PatientModeling StudyInstanceUID={this.state.StudyInstanceUID}/>); break;
      case "DICOMSettings":
        return (<DICOMSettings/>); break;
      default:
        return (<div> Error </div>);
    }
  }
  
  render() {
    const activeTabID = this.state.activeTabID;
    const tabContent = this.activeTabContent();
    
    return (
      <div className='mainBlock'>
        <div className="tab">
          {!this.props.StudyInstanceUID && <TabLinks id={"PatientSelection"} active={activeTabID==="PatientSelection"} value={"Patient Selection"} onClick={() => {this.handleTabLinksClick("PatientSelection")}}/>}
          <TabLinks id={"PatientDataManagement"} active={activeTabID==="PatientDataManagement"} value={"Patient Data Management"} onClick={() => {this.handleTabLinksClick("PatientDataManagement")}}/>
          <TabLinks id={"PatientModeling"} active={activeTabID==="PatientModeling"} value={"Patient Modeling"} onClick={() => {this.handleTabLinksClick("PatientModeling")}}/>
          <TabLinks id={"PlanEvaluation"} active={activeTabID==="PlanEvaluation"} value={"Plan Evaluation"} onClick={() => {this.handleTabLinksClick("PlanEvaluation")}}/>
          {!this.props.StudyInstanceUID && <TabLinks id={"DICOMSettings"} active={activeTabID==="DICOMSettings"} value={"Settings"} onClick={() => {this.handleTabLinksClick("DICOMSettings")}}/>}
        </div>
        <div className="tabcontent">
          {tabContent}
        </div>
      </div>
    );
  }
}

