
import React, { Component } from "react";
import {SETTING_NAMES, MainSettings} from '../../utils/MainSettings.js';
import DataSet from './../../data/DataSet.js';

import './styles.css'

export default class PatientSelection extends React.Component {
  constructor(props) {
    super(props);
    
    const settings = new MainSettings();
    const pacsURL = settings.get(SETTING_NAMES.PACS_URL);
    
    this.state = {
      studies: null,
      activeStudyInstanceUID: this.props.activeStudyInstanceUID
    };
    
    const dataSet = DataSet.getInstance();
    
    const dicomDataLoader = dataSet.getDataLoader();
    dicomDataLoader.setPACSURL(pacsURL);
    dicomDataLoader.enable();
    
    dicomDataLoader.getStudies().then((studies) => this.setState({studies:studies.slice()}));
  }
  
  handleClickStudy(StudyInstanceUID) {
    const dataSet = DataSet.getInstance();
    dataSet.loadStudyAsync(StudyInstanceUID).then();
    
    this.props.setStudy(StudyInstanceUID);
    this.setState({activeStudyInstanceUID: StudyInstanceUID});
  }
  
  render() {
    const studies = this.state.studies;
    
    let studiessBtn = null;
    
    
    if (studies) {      
      studiessBtn = studies.map((value, index) => {
        return (
          <tr className={this.state.activeStudyInstanceUID===value.StudyInstanceUID ? "tableRow" + " active" : "tableRow"} onClick={()=>{this.handleClickStudy(value.StudyInstanceUID)}}>
            <td> {value.PatientName ? value.PatientName : ""} </td>
            <td> {value.PatientID ? value.PatientID : ""} </td>
            <td> {value.StudyDate ? value.StudyDate : ""} </td>
            <td> {value.StudyDescription ? value.StudyDescription : ""} </td>
          </tr>
        );
      });
    }
    else
      return (<p> No patient found </p>);
    
    return (
      <div className='tableList'>
        <p> Patient Selection </p>
        <div className='patientTableDiv'>
          <table className='patientTable'>
            <tr className='patientTableHeader'> <td> PatientName </td> <td> PatientID </td> <td> StudyDate </td> <td> StudyDescription </td> </tr>
            {studiessBtn}
          </table>
        </div>
      </div>
    );
  }
}

