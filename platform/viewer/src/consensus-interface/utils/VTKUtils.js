
import vtkColorTransferFunction from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction';
import vtkPiecewiseFunction from 'vtk.js/Sources/Common/DataModel/PiecewiseFunction';
import vtkImageData from 'vtk.js/Sources/Common/DataModel/ImageData';
import vtkDataArray from 'vtk.js/Sources/Common/Core/DataArray';
import vtkColorMaps from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction/ColorMaps';
import vtkMath from 'vtk.js/Sources/Common/Core/Math';
import vtkVolumeMapper from 'vtk.js/Sources/Rendering/Core/VolumeMapper';
import vtkVolume from 'vtk.js/Sources/Rendering/Core/Volume';
import dcmjs from 'dcmjs';

function getShiftRange(colorTransferArray) {
  // Credit to paraview-glance
  // https://github.com/Kitware/paraview-glance/blob/3fec8eeff31e9c19ad5b6bff8e7159bd745e2ba9/src/components/controls/ColorBy/script.js#L133

  // shift range is original rgb/opacity range centered around 0
  let min = Infinity;
  let max = -Infinity;
  for (let i = 0; i < colorTransferArray.length; i += 4) {
    min = Math.min(min, colorTransferArray[i]);
    max = Math.max(max, colorTransferArray[i]);
  }

  const center = (max - min) / 2;

  return {
    shiftRange: [-center, center],
    min,
    max,
  };
}

function applyPointsToPiecewiseFunction(points, range, pwf) {
  const width = range[1] - range[0];
  const rescaled = points.map(([x, y]) => [x * width + range[0], y]);

  pwf.removeAllPoints();
  rescaled.forEach(([x, y]) => pwf.addPoint(x, y));

  return rescaled;
}

function applyPointsToRGBFunction(points, range, cfun) {
  const width = range[1] - range[0];
  const rescaled = points.map(([x, r, g, b]) => [
    x * width + range[0],
    r,
    g,
    b,
  ]);

  cfun.removeAllPoints();
  rescaled.forEach(([x, r, g, b]) => cfun.addRGBPoint(x, r, g, b));

  return rescaled;
}

export function applyPreset(actor, preset) {
  // Create color transfer function
  const colorTransferArray = preset.colorTransfer
    .split(' ')
    .splice(1)
    .map(parseFloat);

  const { shiftRange } = getShiftRange(colorTransferArray);
  let min = shiftRange[0];
  const width = shiftRange[1] - shiftRange[0];
  const cfun = vtkColorTransferFunction.newInstance();
  const normColorTransferValuePoints = [];
  for (let i = 0; i < colorTransferArray.length; i += 4) {
    let value = colorTransferArray[i];
    const r = colorTransferArray[i + 1];
    const g = colorTransferArray[i + 2];
    const b = colorTransferArray[i + 3];

    value = (value - min) / width;
    normColorTransferValuePoints.push([value, r, g, b]);
  }

  applyPointsToRGBFunction(normColorTransferValuePoints, shiftRange, cfun);

  actor.getProperty().setRGBTransferFunction(0, cfun);

  // Create scalar opacity function
  const scalarOpacityArray = preset.scalarOpacity
    .split(' ')
    .splice(1)
    .map(parseFloat);

  const ofun = vtkPiecewiseFunction.newInstance();
  const normPoints = [];
  for (let i = 0; i < scalarOpacityArray.length; i += 2) {
    let value = scalarOpacityArray[i];
    const opacity = scalarOpacityArray[i + 1];

    value = (value - min) / width;

    normPoints.push([value, opacity]);
  }

  applyPointsToPiecewiseFunction(normPoints, shiftRange, ofun);

  actor.getProperty().setScalarOpacity(0, ofun);

  const [
    gradientMinValue,
    gradientMinOpacity,
    gradientMaxValue,
    gradientMaxOpacity,
  ] = preset.gradientOpacity
    .split(' ')
    .splice(1)
    .map(parseFloat);

  actor.getProperty().setUseGradientOpacity(0, true);
  actor.getProperty().setGradientOpacityMinimumValue(0, gradientMinValue);
  actor.getProperty().setGradientOpacityMinimumOpacity(0, gradientMinOpacity);
  actor.getProperty().setGradientOpacityMaximumValue(0, gradientMaxValue);
  actor.getProperty().setGradientOpacityMaximumOpacity(0, gradientMaxOpacity);

  if (preset.interpolation === '1') {
    actor.getProperty().setInterpolationTypeToFastLinear();
    //actor.getProperty().setInterpolationTypeToLinear()
  }

  const ambient = parseFloat(preset.ambient);
  //const shade = preset.shade === '1'
  const diffuse = parseFloat(preset.diffuse);
  const specular = parseFloat(preset.specular);
  const specularPower = parseFloat(preset.specularPower);

  //actor.getProperty().setShade(shade)
  actor.getProperty().setAmbient(ambient);
  actor.getProperty().setDiffuse(diffuse);
  actor.getProperty().setSpecular(specular);
  actor.getProperty().setSpecularPower(specularPower);
}


export async function getCTActor(pacsURL, StudyInstanceUID, SeriesInstanceUID, preset, progressFunc) {
  const ctImageData = await getVTKImage(pacsURL, StudyInstanceUID, SeriesInstanceUID, progressFunc) ;
      const range = ctImageData
        .getPointData()
        .getScalars()
        .getRange();
      const mapper = vtkVolumeMapper.newInstance();
      const ctVol = vtkVolume.newInstance();
      
      const rgbTransferFunction = ctVol.getProperty().getRGBTransferFunction(0);

      mapper.setInputData(ctImageData);
      mapper.setMaximumSamplesPerRay(2000);
      rgbTransferFunction.setRange(range[0], range[1]);
      ctVol.setMapper(mapper);
      
      ctVol.getProperty().setUseGradientOpacity(0, true);
      
      ctVol
        .getProperty()
        .getRGBTransferFunction(0)
        .setRange(range[0], range[1]);
        
      applyPreset(ctVol, preset);
        
      ctVol.getProperty().setScalarOpacityUnitDistance(0, 2.5);

      return( {actor:ctVol, actor3d:ctVol} );
}


export function getActorFromSegs(segDataSet) {
  const data = segDataSet.getVTKData();
  const segs = segDataSet.getSegs();
  
  if (!data)
    return null;
  
  const image = {
    imageMapper: vtkVolumeMapper.newInstance(),
    actor: vtkVolume.newInstance(),
    cfun: vtkColorTransferFunction.newInstance(),
    ofun: vtkPiecewiseFunction.newInstance(),
  };
  
  image.data = data;
      
 
  
        image.ofun.addPoint(0, 0); // All 0's are fully transparent
        image.ofun.addPoint(0.5, 1);
        image.ofun.addPoint(1, 1);
        image.ofun.addPoint(segs.length, 1);
        
        image.cfun.addRGBPoint(0, 0, 0 , 0);
        for (let i=0; i<segs.length; i++) {
          const color = segs[i].getColor();
          image.cfun.addRGBPoint(i+1, color[0]/255.0, color[1]/255.0 , color[2]/255.0);
        }
        
        
        image.actor.getProperty().setRGBTransferFunction(0, image.cfun);
        image.actor.getProperty().getRGBTransferFunction(0).setRange(0, segs.length);
        
        image.actor.getProperty().setScalarOpacity(0, image.ofun);
        image.actor.getProperty().setInterpolationTypeToNearest();
        
        image.actor.getProperty().setScalarOpacityUnitDistance(0, 3);
        
        
        image.actor.getProperty().setGradientOpacityMinimumValue(0, 2);
        image.actor.getProperty().setGradientOpacityMinimumOpacity(0, 0.0);
        image.actor.getProperty().setGradientOpacityMaximumValue(0, 20);
        image.actor.getProperty().setGradientOpacityMaximumOpacity(0, 1.0);
        image.actor.getProperty().setShade(true);
        image.actor.getProperty().setAmbient(0.2);
        image.actor.getProperty().setDiffuse(0.2);
        image.actor.getProperty().setSpecular(0.3);
        image.actor.getProperty().setSpecularPower(8.0);
      
  image.actor.setMapper(image.imageMapper);
  image.imageMapper.setSampleDistance(1.1);
  image.imageMapper.setMaximumSamplesPerRay(2000);
  image.imageMapper.setInputData(data);
      
  return (image.actor);
}

export function getActorFromSeg(seg) {
  const data = seg.getVTKData();
  const image = {
    imageMapper: vtkVolumeMapper.newInstance(),
    actor: vtkVolume.newInstance(),
    cfun: vtkColorTransferFunction.newInstance(),
    ofun: vtkPiecewiseFunction.newInstance(),
  };
  
  image.data = data;
      
  const color = seg.getColor();
  
        image.ofun.addPoint(0, 0); // All 0's are fully transparent
        image.ofun.addPoint(0.5, 1);
        image.ofun.addPoint(1, 1);
        image.ofun.addPoint(255, 1);
        
        image.cfun.addRGBPoint(0, 0, 0 , 0);
        image.cfun.addRGBPoint(1, color[0]/255.0, color[1]/255.0 , color[2]/255.0);
        image.cfun.addRGBPoint(255, color[0]/255.0, color[1]/255.0 , color[2]/255.0);
        
        
        image.actor.getProperty().setRGBTransferFunction(0, image.cfun);
        image.actor.getProperty().getRGBTransferFunction(0).setRange(0, 1);
        
        image.actor.getProperty().setScalarOpacity(0, image.ofun);
        //image.actor.getProperty().setInterpolationTypeToNearest();
        
        image.actor.getProperty().setScalarOpacityUnitDistance(0, 3);
        
        
        image.actor.getProperty().setGradientOpacityMinimumValue(0, 2);
        image.actor.getProperty().setGradientOpacityMinimumOpacity(0, 0.0);
        image.actor.getProperty().setGradientOpacityMaximumValue(0, 20);
        image.actor.getProperty().setGradientOpacityMaximumOpacity(0, 1.0);
        image.actor.getProperty().setShade(true);
        image.actor.getProperty().setAmbient(0.2);
        image.actor.getProperty().setDiffuse(0.7);
        image.actor.getProperty().setSpecular(0.3);
        image.actor.getProperty().setSpecularPower(8.0);
      
  image.actor.setMapper(image.imageMapper);
  image.imageMapper.setSampleDistance(1.1);
  image.imageMapper.setMaximumSamplesPerRay(2000);
  image.imageMapper.setInputData(data);
      
  return (image.actor);
}

export async function getSEGActor(pacsURL, StudyInstanceUID, SEGSeriesUID, SEGSOPUID, segPreset) {
  const {imageDataArray, RecommendedDisplayCIELabValue} = await getVTKSEG(pacsURL, StudyInstanceUID, SEGSeriesUID, SEGSOPUID);
  
  let actors =[];
  let actors3d = [];
  
      for (let i=0; i<imageDataArray.length; i++) {
        const segImageData = imageDataArray[i];
        
        const mapper = vtkVolumeMapper.newInstance();
        const actor = vtkVolume.newInstance();
        const actor3d = vtkVolume.newInstance();
        
        const rgbTransferFunction = actor.getProperty().getRGBTransferFunction(0);
        const rgbTransferFunction3d = actor3d.getProperty().getRGBTransferFunction(0);
  
        mapper.setInputData(segImageData);
        mapper.setMaximumSamplesPerRay(2000);

        const range = [0, 255];
        
        actor.setMapper(mapper);
        actor3d.setMapper(mapper);
        
        applyPreset(actor, segPreset);
        applyPreset(actor3d, segPreset);
        
        const rgbtf = actor.getProperty().getRGBTransferFunction(0);
        const rgbtf3d = actor3d.getProperty().getRGBTransferFunction(0);
        
        rgbtf.setRange(range[0], range[1]);
        rgbtf.removeAllPoints();
        rgbtf3d.setRange(range[0], range[1]);
        rgbtf3d.removeAllPoints();
        
        
        
        const cielab = RecommendedDisplayCIELabValue[i];
        const rgba = dcmjs.data.Colors.dicomlab2RGB(cielab).map(x => Math.round(x * 255));
        let vtkColor = rgba.map(e => e / 255.);
        
        rgbtf.addRGBPoint(range[0], 0,0,0);
        rgbtf.addRGBPoint(range[1], vtkColor[0], vtkColor[1] , vtkColor[2]);
        
        rgbtf3d.addRGBPoint(range[0], 0,0,0);
        rgbtf3d.addRGBPoint(range[1], vtkColor[0], vtkColor[1] , vtkColor[2]);
        
        actor.getProperty().setUseLabelOutline(true);
        actor.setVisibility(true);
        actor.getProperty().setUseGradientOpacity(0, false);
        
        actor3d.setVisibility(true);
        actor3d.getProperty().setUseGradientOpacity(0, false);
        
        const cfun = vtkColorTransferFunction.newInstance();
        const ofun = vtkPiecewiseFunction.newInstance();
        
        ofun.addPointLong(0, 0, 0.5, 1.0);
        ofun.addPointLong(1, 1.0, 0.5, 1.0);

        cfun.addRGBPoint(1, vtkColor[0], vtkColor[1] , vtkColor[2]);
        ofun.addPointLong(1, 0.5, 0.5, 1.0);
        
        actor.getProperty().setRGBTransferFunction(0, cfun);
        actor.getProperty().setScalarOpacity(0, ofun);
        actor.getProperty().setInterpolationTypeToNearest();
        actor.getProperty().setScalarOpacityUnitDistance(0, 0.1);
        actor.getProperty().setUseGradientOpacity(0, false);
        
        actor3d.getProperty().setRGBTransferFunction(0, cfun);
        actor3d.getProperty().setScalarOpacity(0, ofun);
        actor3d.getProperty().setInterpolationTypeToNearest();
        actor3d.getProperty().setScalarOpacityUnitDistance(0, 0.1);
        actor3d.getProperty().setUseGradientOpacity(0, false);

        actors.push(actor);
        actors3d.push(actor3d);
      }
   
   console.log(actors);
   return( {SEGActors:actors, SEGActors3d:actors3d} );
}



