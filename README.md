<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<div align="center">
  <h1>Medical Imaging Viewer</h1>
  <p>This is a zero-footprint medical image viewer</p>
</div>

<hr />

## Developing

### Requirements

- [Yarn 1.17.3+](https://yarnpkg.com/en/docs/install)
- [Node 10+](https://nodejs.org/en/)
- Yarn Workspaces should be enabled on your machine:
  - `yarn config set workspaces-experimental true`

### Getting Started

1. Fork this repository
2. Clone your forked repository
3. Navigate to the cloned project's directory
4. Add this repo as a `remote` named `upstream`
5. `yarn install` to restore dependencies and link projects
6. `yarn run dev`


## Acknowledgments

[OHIF](https://github.com/OHIF)

## License

MIT © Sylvain Deffet

