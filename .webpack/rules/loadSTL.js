
//const PUBLIC_URL = process.env.PUBLIC_URL || '';

const loadSTL = {
    rules: [
      {
        test:  /\.(stl)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 100000,
            },
            /*options: {
              name: '[name].[ext]',
              outputPath: 'stl',
              publicPath: `${PUBLIC_URL}/stl`,
            },*/
          },
        ],
      },
    ],
};

module.exports = loadSTL;

